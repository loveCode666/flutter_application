import 'package:flutter/material.dart';

// 1. TapboxA 管理自身状态.
//------------------------- TapboxA ----------------------------------
class TapboxA extends StatefulWidget {
  TapboxA({Key? key}) : super(key: key);

  @override
  State<TapboxA> createState() => _TapboxAState();
}

class _TapboxAState extends State<TapboxA> {
  bool _active = false;

  void _handleTap() {
    setState(() {
      _active = !_active;
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: _handleTap,
      child: Container(
        width: 200.0,
        height: 200.0,
        decoration: BoxDecoration(
          color: _active ? Colors.lightGreen[700] : Colors.grey[600],
        ),
        child: Center(
          child: Text(
            _active ? 'Active' : 'Inactive',
            style: TextStyle(fontSize: 32.0, color: Colors.white),
          ),
        ),
      ),
    );
  }
}

// 2. ParentWidget 为 TapboxB 管理状态.
// 用于状态需要被多个子组件共享吧？
//------------------------ ParentWidget --------------------------------

class ParentWidgetB extends StatefulWidget {
  @override
  State createState() => _ParentWidgetState();
}

class _ParentWidgetState extends State<ParentWidgetB> {
  bool _active = false;

  void _handleTapboxChanged(bool newValue) {
    setState(() {
      _active = newValue;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        TapboxB(
          active: _active,
          onChanged: _handleTapboxChanged,
        ),
        TapboxB1(
          active: _active,
        )
      ],
    );
  }
}

//------------------------- TapboxB ----------------------------------

class TapboxB extends StatelessWidget {
  TapboxB({Key? key, this.active = false, required this.onChanged})
      : super(key: key);

  final bool active;
  final ValueChanged<bool> onChanged;

  void _handleTap() {
    onChanged(!active);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: _handleTap,
      child: Container(
        width: 200.0,
        height: 200.0,
        decoration: BoxDecoration(
          color: active ? Colors.lightGreen[700] : Colors.grey[600],
        ),
        child: Center(
          child: Text(
            active ? 'Active' : 'Inactive',
            style: TextStyle(fontSize: 32.0, color: Colors.white),
          ),
        ),
      ),
    );
  }
}

class TapboxB1 extends StatelessWidget {
  final bool active;

  TapboxB1({required this.active});

  @override
  Widget build(BuildContext context) {
    return Text(
      active ? 'Active' : 'Inactive',
      style: TextStyle(color: active ? Colors.green : Colors.blue),
    );
  }
}

// 3. 混合状态管理
// 比part2多了一个highlight状态: 点击时边框高亮。因为TapBox共享给其他人使用时，
// 别人并不关心内部的hightlight是如何实现的，所以内部管理就好了
//---------------------------- ParentWidget ----------------------------

class ParentWidgetC extends StatefulWidget {
  @override
  State createState() => _ParentWidgetCState();
}

class _ParentWidgetCState extends State<ParentWidgetC> {
  bool _active = false;

  void _handleTapboxChanged(bool newValue) {
    setState(() {
      _active = newValue;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        TapboxC(
          active: _active,
          onChanged: _handleTapboxChanged,
        ),
        TapboxB1(
          active: _active,
        ),
      ],
    );
  }
}

//----------------------------- TapboxC ------------------------------

class TapboxC extends StatefulWidget {
  TapboxC({Key? key, this.active = false, required this.onChanged})
      : super(key: key);

  final bool active;
  final ValueChanged<bool> onChanged;

  @override
  State createState() => _TapboxCState();
}

class _TapboxCState extends State<TapboxC> {
  bool _highlight = false;

  void _handleTapDown(TapDownDetails details) {
    setState(() {
      _highlight = true;
    });
  }

  void _handleTapUp(TapUpDetails details) {
    setState(() {
      _highlight = false;
    });
  }

  void _handleTapCancel() {
    setState(() {
      _highlight = false;
    });
  }

  void _handleTap() {
    widget.onChanged(!widget.active);
  }

  @override
  Widget build(BuildContext context) {
    // 在按下时添加绿色边框，当抬起时，取消高亮
    return GestureDetector(
      onTapDown: _handleTapDown, // 处理按下事件
      onTapUp: _handleTapUp, // 处理抬起事件
      onTap: _handleTap,
      onTapCancel: _handleTapCancel,
      child: Container(
        width: 200.0,
        height: 200.0,
        decoration: BoxDecoration(
          color: widget.active ? Colors.lightGreen[700] : Colors.grey[600],
          border: _highlight
              ? Border.all(
                  color: Colors.teal,
                  width: 10.0,
                )
              : null,
        ),
        child: Center(
          child: Text(
            widget.active ? 'Active' : 'Inactive',
            style: TextStyle(fontSize: 32.0, color: Colors.white),
          ),
        ),
      ),
    );
  }
}

void main(List<String> args) {
  runApp(MaterialApp(
    home: Column(
      children: [
        Row(
          children: [
            Text(
              "1. 自己管理状态",
              style: TextStyle(fontSize: 18),
            ),
            TapboxA(),
          ],
        ),
        Row(
          children: [
            Text(
              "2. 父管理子",
              style: TextStyle(fontSize: 18),
            ),
            ParentWidgetB(),
          ],
        ),
        Row(
          children: [
            Text(
              "3. 混合状态管理",
              style: TextStyle(fontSize: 18),
            ),
            ParentWidgetC(),
          ],
        ),
      ],
    ),
  ));
}
