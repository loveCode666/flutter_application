import 'package:flutter/material.dart';

class Demo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(getPosition("text1", context)),
        SizedBox(height: 10),
        Text(getPosition("xxx", context)),
      ],
    );
  }

  String getPosition(String xxx, BuildContext context) {
    RenderObject? renderObject = context.findRenderObject();
    // 获取text1, text2的位置，宽高等布局信息

    return xxx;
  }
}

void main(List<String> args) {
  runApp(MaterialApp(
    home: Scaffold(body: Demo()),
  ));
}
