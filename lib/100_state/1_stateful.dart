import 'package:flutter/material.dart';

/// createElement & createState
class Echo extends StatefulWidget {
  final String text;
  Echo({required this.text});

  @override
  State<StatefulWidget> createState() => _MyEcho();
}

class _MyEcho extends State<Echo> {
  int _count = 0;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text("${widget.text} $_count"),
        TextButton(
          onPressed: () {
            setState(() {
              _count++;
            });
          },
          child: Text("change"),
        )
      ],
    );
  }
}

void main(List<String> args) {
  runApp(MaterialApp(
    home: Scaffold(
      appBar: AppBar(
        title: Text("test stateful"),
      ),
      body: Center(
        child: Echo(text: "是谁在敲打我窗"),
      ),
    ),
  ));
}
