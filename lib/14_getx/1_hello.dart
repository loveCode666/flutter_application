import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Demo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("1. 原始写法Dialog"),
        ElevatedButton(
          onPressed: () {
            showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Text("确认操作"),
                  content: Text("你确定要执行此操作吗?"),
                  actions: <Widget>[
                    TextButton(
                        onPressed: Navigator.of(context).pop,
                        child: Text("取消")),
                    // 通常是一个强调的按钮
                    TextButton(
                        onPressed: Navigator.of(context).pop,
                        child: Text("确定")),
                  ],
                );
              },
            );
          },
          child: const Text("原始confirm dialog：AlertDialog"),
        ),
        SizedBox(height: 10),
        Text("2. getx Dialog: 把showDialog继承进来了"),
        ElevatedButton(
          onPressed: () {
            Get.defaultDialog(
                title: "默认弹窗",
                middleText: '弹出内容，你确定要卸载吗？',
                confirm:
                    TextButton(onPressed: Get.back, child: const Text("确定")),
                cancel:
                    TextButton(onPressed: Get.back, child: const Text("取消")));
          },
          child: const Text("getx 默认dialog"),
        ),
        SizedBox(height: 10),
        Text("3. getx snackbar"),
        ElevatedButton(
          onPressed: () {
            Get.snackbar("提示", "删除成功");
          },
          child: const Text("snack-bar"),
        ),
        SizedBox(height: 20),
        Text("4. BottomSheet"),
        ElevatedButton(
          onPressed: () {
            showBottomSheet(
              context: context,
              builder: (BuildContext context) {
                return Container(
                  padding: EdgeInsets.all(20.0),
                  child: Text('这是一个 BottomSheet'),
                );
              },
            );
          },
          child: const Text("原生showBottomSheet "),
        ),
        ElevatedButton(
          onPressed: () {
            showBottomSheet(
              context: context,
              builder: (BuildContext context) {
                return Stack(
                  children: [
                    Padding(
                      padding: EdgeInsets.all(25),
                      child: Text('这是一个 BottomSheet'),
                    ),
                    Positioned(
                        top: 5,
                        right: 5,
                        child: GestureDetector(
                          onTap: Navigator.of(context).pop,
                          child: Icon(Icons.close),
                        )),
                  ],
                );
              },
            );
          },
          child: const Text("原生showBottomSheet + 关闭按钮 "),
        ),
        ElevatedButton(
          onPressed: () {
            showBottomSheet(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Text("确认操作"),
                  content: Text("你确定要执行此操作吗, AlertDialog自带居中吗 把整个页面都撑开了?"),
                  actions: <Widget>[
                    TextButton(
                        onPressed: Navigator.of(context).pop,
                        child: Text("取消")),
                    // 通常是一个强调的按钮
                    TextButton(
                        onPressed: Navigator.of(context).pop,
                        child: Text("确定")),
                  ],
                );
              },
            );
          },
          child: const Text("原生showBottomSheet + alertDialog "),
        ),
        ElevatedButton(
          onPressed: () {
            showModalBottomSheet(
              context: context,
              builder: (BuildContext context) {
                return Container(
                  padding: EdgeInsets.all(20.0),
                  child: Text('这是一个 ModalBottomSheet, 增加了模态背景'),
                );
              },
            );
          },
          child: const Text("原生ModalBottomSheet"),
        ),
        SizedBox(height: 10),
        ElevatedButton(
          onPressed: () {
            // showBottomSheet(
            //   context: context,
            //   builder: (BuildContext context) {
            //     return Container(
            //       padding: EdgeInsets.all(20.0),
            //       child: Text('这是一个 BottomSheet'),
            //     );
            //   },
            // );
            Get.bottomSheet(
              Container(
                padding: EdgeInsets.all(20.0),
                // 1.  getx很多api都把这个context和builder给去掉了,
                // 2. 而且这个Get.bottomSheet自带蒙版，相当于ModalBottomSheet
                // 3. 背景是透明的
                child: Text('这是一个 getx BottomSheet;'),
              ),
            );
          },
          child: const Text("getx showBottomSheet "),
        ),
        ElevatedButton(
          onPressed: () {
            Get.bottomSheet(
              Container(
                color: Colors.white,
                height: 100,
                padding: EdgeInsets.all(20.0),
                child: Text('反而是少了原生的好看样式，需要自定义样式了，圆角，高度之类的'),
              ),
            );
          },
          child: const Text("getx showBottomSheet 2, 自定义显示内容 "),
        ),
        ElevatedButton(
          onPressed: () {
            Get.bottomSheet(Container(
              color: Colors.white,
              height: 130,
              child: Column(
                children: [
                  ListTile(
                    leading: const Icon(Icons.wb_sunny_outlined),
                    title: const Text("白天模式"),
                    onTap: () {
                      Get.changeTheme(ThemeData.light());
                      Get.back();
                    },
                  ),
                  const Divider(),
                  ListTile(
                    leading: const Icon(Icons.wb_sunny),
                    title: const Text("夜间模式"),
                    onTap: () {
                      Get.changeTheme(ThemeData.dark());
                      Get.back();
                    },
                  ),
                ],
              ),
            ));
          },
          child: const Text("Get.bottomSheet change theme"),
        ),
        SizedBox(height: 30),
        Text("总结：Getx语法中好像是省区了Context的申明"),
      ],
    );
  }
}

void main(List<String> args) {
  runApp(GetMaterialApp(
    title: "GetX",
    theme: ThemeData(
      colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
    ),
    // theme: ThemeData.dark(),
    home: Scaffold(body: Demo()),
  ));
}
