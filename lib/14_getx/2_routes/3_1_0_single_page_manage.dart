import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SinglePageState extends StatefulWidget {
  const SinglePageState({super.key, required this.title});
  final String title;
  @override
  State<SinglePageState> createState() => _SinglePageStateState();
}

class _SinglePageStateState extends State<SinglePageState> {
  // 定义一个响应式的整数
  RxInt count = 0.obs;
  // 或者
  RxInt a = RxInt(0);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title),
      ),
      // const Color(0x00cab6ec)
      body: Column(
        children: [
          ElevatedButton(
              onPressed: () {
                count.value += 1;
              },
              child: const Text("加1")),
          //   监听值的变化
          Obx(() => Text("当前值：${count.value}")),
        ],
      ),
    );
  }
}
