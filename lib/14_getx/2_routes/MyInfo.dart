import 'package:flutter/material.dart';

class MyInfo extends StatelessWidget {
  const MyInfo({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("My Info Page"),
        backgroundColor: Colors.pink,
      ),
      body: const Center(
        child: Text("MyInfo Page"),
      ),
    );
  }
}
