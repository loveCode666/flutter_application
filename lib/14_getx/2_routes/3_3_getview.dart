import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '3_2_count_controller.dart';

class GetViewPage extends GetView<CountController> {
  const GetViewPage({super.key});

  @override
  Widget build(BuildContext context) {
    // 如果第一次使用还需要put（没有结合Binding时）
    CountController countController = Get.put(CountController());
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text("Getview page"),
      ),
      // const Color(0x00cab6ec)
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("无需statefulWidget 和 StatelessWidget了"),
          ElevatedButton(
              onPressed: () {
                countController.count.value += 1;
              },
              child: const Text("加1")),
          //   监听值的变化
          Obx(() => Text("当前值：${countController.count.value}")),
        ],
      ),
    );
  }
}
