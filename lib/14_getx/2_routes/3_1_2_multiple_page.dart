import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '3_2_count_controller.dart';

class MultiPageState2 extends StatefulWidget {
  const MultiPageState2({super.key});
  @override
  State<MultiPageState2> createState() => _MultiPageState2State();
}

class _MultiPageState2State extends State<MultiPageState2> {
  // 创建控制器示例
  CountController countController = Get.find(tag: 'countController');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text("多页面共享状态"),
      ),
      // const Color(0x00cab6ec)
      body: Column(
        children: [
          ElevatedButton(
              onPressed: () {
                // countController.del();
                countController.count.value--;
              },
              child: const Text("减去1")),
          //   监听值的变化
          Obx(() => Text("当前值：${countController.count.value}")),
        ],
      ),
    );
  }
}
