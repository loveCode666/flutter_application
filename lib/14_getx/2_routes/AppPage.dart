import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:namer_app/14_getx/2_routes/3_1_1_multiple_page.dart';
import 'package:namer_app/14_getx/2_routes/LoginPage.dart';
import 'package:namer_app/14_getx/2_routes/MyHomePage.dart';
import 'package:namer_app/14_getx/2_routes/MyInfo.dart';

import '3_1_0_single_page_manage.dart';
import '3_1_2_multiple_page.dart';
import '3_3_getview.dart';

class AppPage {
  static final List<GetPage<dynamic>> routes = [
    //路由地址
    GetPage(name: '/', page: () => const MyHomePage(title: 'getx')),
    GetPage(
        name: '/my',
        page: () => const MyInfo(),
        transition: Transition.leftToRight, // 设置路由过度动画
        // 路由中间件（路由守卫）
        middlewares: [MyMiddleWare()]),
    GetPage(name: '/login', page: () => const LoginPage()),
    GetPage(
      name: '/single-page-state',
      page: () => const SinglePageState(title: '单页面状态管理'),
    ),
    GetPage(
      name: '/multiple-page-state',
      page: () => const MultiPageState(title: '多页面状态管理'),
    ),
    GetPage(
      name: '/multiple-page-state2',
      page: () => const MultiPageState2(),
    ),
    GetPage(name: '/get-view', page: () => const GetViewPage()),
  ];
}

// 中间件
class MyMiddleWare extends GetMiddleware {
  // 重写重定向
  @override
  redirect(route) {
    print("route- $route");
    // 获取不到arguments？
    String power = Get.arguments?['power'] ?? '';
    print("route-power $power");
    if (power == 'no') {
      Get.snackbar("提示", "请先进行登录");
      return const RouteSettings(name: 'login');
    }
    // 放行，跳转到目标路由
    return null;
  }
}
