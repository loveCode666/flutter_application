import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'get_controller.dart';

class GetViewPage2 extends GetView<CountController> {
  const GetViewPage2({super.key});

  @override
  Widget build(BuildContext context) {
    // 有bind页面则无需手动创建controller了
    // CountController countController = Get.put(CountController());
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text("Getview page2"),
      ),
      // const Color(0x00cab6ec)
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ElevatedButton(
              onPressed: () {
                controller.count.value -= 1;
              },
              child: const Text("minus 1")),
          //   监听值的变化
          Obx(() => Text("当前值：${controller.count.value}")),
        ],
      ),
    );
  }
}
