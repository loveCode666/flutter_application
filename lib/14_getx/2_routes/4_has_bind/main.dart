import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'AppPage.dart';
import 'get_bind.dart';

void main(List<String> args) {
  runApp(GetMaterialApp(
    title: "GetX",
    // 初始化绑定状态管理类
    //   初始化完成后，就不需要再进行创建。使用时只需要通过Get.find来获取实例即可
    initialBinding: AllControllerBinding(),
    theme: ThemeData(
      colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
    ),
    initialRoute: '/view1',
    defaultTransition: Transition.leftToRight,
    getPages: AppPage.routes,
  ));
}
