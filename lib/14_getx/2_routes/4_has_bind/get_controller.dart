import 'package:get/get.dart';

class CountController extends GetxController {
  // 定义响应式变量
  RxInt count = 0.obs;
//  增加
  void add() {
    count.value++;
    // 调用GetxController内置方法进行更新
    update();
  }

// 减少
  void del() {
    count.value--;
    // 调用GetxController内置方法进行更新
    update();
  }
}
