import 'package:get/get.dart';

import 'get_controller.dart';

class AllControllerBinding implements Bindings {
  @override
  void dependencies() {
    // 初始化
    Get.lazyPut<CountController>(() => CountController());
    // 多个则多执行几次Get.lazyPut
  }
}
