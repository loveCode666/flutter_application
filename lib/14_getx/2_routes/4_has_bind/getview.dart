import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'get_controller.dart';

class GetViewPage extends GetView<CountController> {
  const GetViewPage({super.key});

  @override
  Widget build(BuildContext context) {
    // 有bind页面则无需手动创建controller了
    // CountController countController = Get.put(CountController());
    // Get.find<CountController>();
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text("Getview page"),
      ),
      // const Color(0x00cab6ec)
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ElevatedButton(
              onPressed: () {
                controller.count.value += 1;
              },
              child: const Text("加1")),
          //   监听值的变化
          Obx(() => Text("当前值：${controller.count.value}")),
          TextButton(
              onPressed: () => Get.toNamed("view2"),
              child: Text("got to getview2")),
        ],
      ),
    );
  }
}
