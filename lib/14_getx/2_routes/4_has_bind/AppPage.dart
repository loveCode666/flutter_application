import 'package:get/get.dart';

import 'getview.dart';
import 'getview2.dart';

class AppPage {
  static final List<GetPage<dynamic>> routes = [
    //路由地址
    GetPage(name: '/view1', page: () => const GetViewPage()),
    GetPage(
      name: '/view2',
      page: () => const GetViewPage2(),
      transition: Transition.leftToRight,
    ),
  ];
}
