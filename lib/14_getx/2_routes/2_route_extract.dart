import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:namer_app/14_getx/2_routes/MyHomePage.dart';

import 'AppPage.dart';

void main(List<String> args) {
  runApp(GetMaterialApp(
    title: "GetX",
    theme: ThemeData(
      colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
    ),
    // theme: ThemeData.dark(),
    // home 和 initialRoute同时存在的话initialRoute会优先生效
    home: const MyHomePage(title: 'getx'), // 主页
    initialRoute: '/', // 默认显示的路由页面
    defaultTransition: Transition.leftToRight, // 设置所有页面默认的路由跳转动画
    // 提取路由配置到一个单独的文件中
    getPages: AppPage.routes,
  ));
}
