import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'AppPage.dart';

void main(List<String> args) {
  runApp(GetMaterialApp(
    title: "GetX",
    theme: ThemeData(
      colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
    ),
    initialRoute: '/', // 默认显示的路由页面
    defaultTransition: Transition.leftToRight, // 设置所有页面默认的路由跳转动画
    // 提取路由配置到一个单独的文件中
    getPages: AppPage.routes,
    // 监听路由变化
    routingCallback: (routing) {
      debugPrint("路由变化了：${routing?.current}");
    },
  ));
}
