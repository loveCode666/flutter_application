import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '3_2_count_controller.dart';

class MultiPageState extends StatefulWidget {
  const MultiPageState({super.key, required this.title});
  final String title;
  @override
  State<MultiPageState> createState() => _MultiPageStateState();
}

class _MultiPageStateState extends State<MultiPageState> {
  // 创建控制器示例
  CountController countController =
      Get.put(CountController(), tag: 'countController');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title),
      ),
      // const Color(0x00cab6ec)
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ElevatedButton(
              onPressed: () {
                countController.count.value += 1;
                // countController.add();
              },
              child: const Text("加1")),
          //   监听值的变化
          Obx(() => Text("当前值：${countController.count.value}")),
          SizedBox(height: 10),
          TextButton(
              onPressed: () => Get.toNamed("multiple-page-state2"),
              child: Text("跳转到状态管理页面2")),
          SizedBox(height: 30),

          Text("创建示例的几个方法：", style: TextStyle(fontWeight: FontWeight.bold)),
          Text("Get.put，不使用控制器实例也会被创建"),
          Text("Get.lazyPut，懒加载方式创建实例，只有在使用时才会被创建"),
          Text("Get.putAsync，Get.put的异步版本"),
          Text("Get.create，每次使用都会创建一个新的实例"),
          SizedBox(height: 10),
          Text("Get.find. 查找实例"),
        ],
      ),
    );
  }
}
