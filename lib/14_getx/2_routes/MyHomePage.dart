import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'MyInfo.dart';

class MyHomePage extends StatelessWidget {
  final String title;
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("My home"),
          backgroundColor: Colors.blue,
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("Home page - $title"),
              TextButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => MyInfo()),
                    );
                  },
                  child: Text("1.1 MyInfo Page - original")),
              TextButton(
                  onPressed: () {
                    Get.toNamed('my', arguments: {
                      "message": 'Hello Flutter',
                      "power": 'no',
                    });
                  },
                  child: Text("1.2 MyInfo Page - getx")),
              TextButton(
                  onPressed: () => Get.toNamed('single-page-state'),
                  child: Text("2 单页面状态管理")),
              TextButton(
                  onPressed: () => Get.toNamed('multiple-page-state'),
                  child: Text("2 多页面状态管理 - StatefulWidget")),
              TextButton(
                onPressed: () => Get.toNamed("get-view"),
                child: Text("Getview简化页面的写法"),
              ),
            ],
          ),
        ));
  }
}
