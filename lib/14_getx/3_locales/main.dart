import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'Messages .dart';
import 'controller.dart';

void main() => runApp(
      GetMaterialApp(
        translations: Messages(), // 你的翻译
        locale: Locale('zh', 'CN'), // 将会按照此处指定的语言翻译
        fallbackLocale: Locale('en', 'US'), // 添加一个回调语言选项，以备上面指定的语言翻译不存在
        home: Home(),
      ),
    );

class Home extends StatelessWidget {
  @override
  Widget build(context) {
    // 使用Get.put()实例化你的类，使其对当下的所有子路由可用。
    final Controller c = Get.put(Controller());

    return Scaffold(
        // 使用Obx(()=>每当改变计数时，就更新Text()。
        appBar: AppBar(title: Obx(() => Text("Clicks: ${c.count}"))),

        // 用一个简单的Get.to()即可代替Navigator.push那8行，无需上下文！
        body: Center(
          child: Column(
            children: [
              ElevatedButton(
                child: Text("Go to Other"),
                onPressed: () => Get.to(Other()),
              ),
              ElevatedButton(
                  onPressed: () {
                    Get.changeTheme(
                        Get.isDarkMode ? ThemeData.light() : ThemeData.dark());
                  },
                  child: Text("切换亮暗色主题")),
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton(
            onPressed: c.increment, child: Icon(Icons.add)));
  }
}

class Other extends StatelessWidget {
  // 你可以让Get找到一个正在被其他页面使用的Controller，并将它返回给你。
  final Controller c = Get.find();

  @override
  Widget build(context) {
    // 访问更新后的计数变量
    return Scaffold(
      appBar: AppBar(title: Text("page2")),
      body: Center(child: Text("${c.count}")),
    );
  }
}
