import 'package:flutter/cupertino.dart';

import 'my_flex.dart';

class MyColumn extends MyFlex {
  const MyColumn({
    super.key,
    super.mainAxisAlignment,
    super.mainAxisSize,
    super.crossAxisAlignment,
    super.textDirection,
    super.verticalDirection,
    super.textBaseline,
    super.children,
  }) : super(
          direction: Axis.vertical,
        );
}
