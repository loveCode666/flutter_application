import 'package:flutter/material.dart';

import 'source/_2_my_listview.dart';

class Demo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("1. 静态布局"),
        MyListView(
          shrinkWrap: true,
          padding: const EdgeInsets.all(20.0),
          // 高度
          // itemExtent: 10,
          children: <Widget>[
            Text("构造函数有四种"),
            Text("1. 静态函数，像当前这个一样"),
            Text("2. "),
            Text("3. "),
          ],
        ),
        SizedBox(height: 30),
        Text("2. ListView.builder是不能嵌入在Column中吗？高度出问题，样式错乱..."),
        Text("ListView.builder需要父亲有固定高度"),
        // ListView.builder(
        //   itemCount: 100,
        //   itemExtent: 50.0, //强制高度为50.0
        //   itemBuilder: (BuildContext context, int index) {
        //     return ListTile(title: Text("$index"));
        //   },
        // ),
        Text("2.1 固定父级高度后，可以显示"),
        SizedBox(
          height: 100,
          child: MyListView.builder(
            itemCount: 100, // 这个参数不写就无限了
            itemBuilder: (BuildContext context, int index) {
              return ListTile(title: Text("$index"));
            },
          ),
        ),
        SizedBox(height: 10),
        Text("2.2 换个思路，动态计算高度"),
        // SizedBox(
        //   // 动态减去top高度
        //   height: MediaQuery.of(context).size.height - 400,
        //   child: ListView.builder(
        //     itemCount: 100, // 这个参数不写就无限了
        //     itemBuilder: (BuildContext context, int index) {
        //       return ListTile(title: Text("$index"));
        //     },
        //   ),
        // ),
        SizedBox(height: 10),
        Text("2.3 进一步优雅，flex动态计算高度"),
        Expanded(
          child: MyListView.builder(
            itemCount: 100, // 这个参数不写就无限了
            itemBuilder: (BuildContext context, int index) {
              return ListTile(title: Text("$index"));
            },
          ),
        ),
      ],
    );
  }
}

class ListViewBuilder extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      // 不知道这个参数是干嘛的
      // prototypeItem: ListTile(title: Text("1")),
      itemExtent: 56, //强制高度为50.0

      itemCount: 100,
      itemBuilder: (BuildContext context, int index) {
        return ListTile(title: Text("$index"));
      },
    );
  }
}

class ListViewSeparated extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //下划线widget预定义以供复用。
    Widget divider1 = Divider(color: Colors.red);
    Widget divider2 = Divider(color: Colors.green);
    return ListView.separated(
      itemCount: 100,
      //列表项构造器
      itemBuilder: (BuildContext context, int index) {
        return ListTile(title: Text("$index"));
      },
      //分割器构造器: 偶红奇绿
      separatorBuilder: (BuildContext context, int index) {
        return index % 2 == 0 ? divider1 : divider2;
      },
    );
  }
}

void main(List<String> args) {
  runApp(MaterialApp(
    home: Scaffold(
      // 1. ListView静态写法
      body: Demo(),

      // 2. builder动态生成
      // body: ListViewBuilder(),

      // 3. ListView.separated可以在生成的列表项之间添加一条分割线
      // body: ListViewSeparated(),

      // 4. 实例：无限加载列表
      // body: InfiniteListView(),
    ),
  ));
}
