import 'package:flutter/material.dart';
import 'package:namer_app/5_scroll/source/my_column.dart';

class SingleChildScrollViewTest extends StatelessWidget {
  Widget longList() {
    String str = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    return Center(
      child: MyColumn(
        //动态创建一个List<Widget>  // 每一个字母都用一个Text显示,字体为原来的两倍
        children: str
            .split("")
            .map((c) => Text(c, textScaler: TextScaler.linear(3.0)))
            .toList(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    // 这样会溢出的
    // return longList();

    // 显示进度条
    return Scrollbar(child: SingleChildScrollView(child: longList()));
  }
}

void main(List<String> args) {
  runApp(MaterialApp(
    home: Scaffold(body: SingleChildScrollViewTest()),
  ));
}
