滚动组件在手机端不可或缺，因为屏幕高度就那么小

Flutter 有两种布局模型：
基于 RenderBox 的盒模型布局。
基于 Sliver ( RenderSliver ) 按需加载列表布局。

1. Sliver 的主要作用是配合：加载子组件并确定每一个子组件的布局和绘制信息，如果 Sliver 可以包含多个子组件时，通常会实现按需加载模型。
只有当 Sliver 出现在视口中时才会去构建它，这种模型也称为“基于Sliver的列表按需加载模型”。
2.不支持该模型的，如SingleChildScrollView。


Flutter 中的可滚动组件主要由三个角色组成：Scrollable、Viewport 和 Sliver：
Scrollable ：用于处理滑动手势，确定滑动偏移，滑动偏移变化时构建 Viewport 。
Viewport：显示的视窗，即列表的可视区域；
Sliver：视窗里显示的元素。





