// user.g.dart 将在我们运行生成命令后自动生成
import 'package:json_annotation/json_annotation.dart';

part '_2_people.g.dart';

///这个标注是告诉生成器，这个类是需要生成Model类的
@JsonSerializable()
class People {
  People(this.name, this.email, this.user);

  String name;
  String email;
  User2 user;
  //不同的类使用不同的mixin即可
  factory People.fromJson(Map<String, dynamic> json) => _$PeopleFromJson(json);
  Map<String, dynamic> toJson() => _$PeopleToJson(this);
}

///这个标注是告诉生成器，这个类是需要生成Model类的
@JsonSerializable()
class User2 {
  User2(this.name, this.email);

  String name;
  String email;
  //不同的类使用不同的mixin即可
  factory User2.fromJson(Map<String, dynamic> json) => _$User2FromJson(json);
  Map<String, dynamic> toJson() => _$User2ToJson(this);
}
