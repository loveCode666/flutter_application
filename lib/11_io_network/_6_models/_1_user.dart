//手写json对象
// 干吗是对象和Map之间的转换呢？直接写成对象和Json字符串之间转换不香吗？
import 'dart:convert';

class User {
  final String name;
  final String email;

  User(this.name, this.email);

  // static fromJson(String jsonStr) {
  factory User.fromJson(String jsonStr) {
    Map<dynamic, dynamic> userMap = jsonDecode(jsonStr);
    var user = User(userMap['name'], userMap['email']);
    return user;
  }

  // 没有toJson就不能使用json.encode(userObj)
  Map<String, dynamic> toJson() {
    return {
      'name': name,
      'email': email,
    };
  }
}
