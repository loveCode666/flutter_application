// GENERATED CODE - DO NOT MODIFY BY HAND

part of '_2_people.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

People _$PeopleFromJson(Map<String, dynamic> json) => People(
      json['name'] as String,
      json['email'] as String,
      User2.fromJson(json['user'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$PeopleToJson(People instance) => <String, dynamic>{
      'name': instance.name,
      'email': instance.email,
      'user': instance.user,
    };

User2 _$User2FromJson(Map<String, dynamic> json) => User2(
      json['name'] as String,
      json['email'] as String,
    );

Map<String, dynamic> _$User2ToJson(User2 instance) => <String, dynamic>{
      'name': instance.name,
      'email': instance.email,
    };
