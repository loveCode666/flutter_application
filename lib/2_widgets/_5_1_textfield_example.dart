import 'package:flutter/material.dart';

class Demo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text("1. 用户名密码登录默认"),
          TextField(
            autofocus: true,
            decoration: InputDecoration(
                labelText: "用户名",
                hintText: "用户名或邮箱",
                prefixIcon: Icon(Icons.person)),
          ),
          TextField(
            decoration: InputDecoration(
                labelText: "密码",
                hintText: "您的登录密码",
                prefixIcon: Icon(Icons.lock)),
            obscureText: true,
          ),
          SizedBox(height: 50),
          Text("2. enabledBorder和focusedBorder: 下划线，未获得焦点在紫色，获得焦点绿色"),
          TextField(
            autofocus: true,
            decoration: InputDecoration(
              labelText: "样式",
              hintText: "decoration中可修改样式",
              prefixIcon: Icon(Icons.person),
              // 未获得焦点下划线设为灰色
              enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.purple),
              ),
              //获得焦点下划线设为蓝色
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.green),
              ),
            ),
          ),
          SizedBox(height: 50),
          Text("3. Theme hintcolor "),
          Theme(
            data: Theme.of(context).copyWith(
              // 文本框的提示的颜色
              hintColor: Colors.red,
              // 能再度覆盖掉上面的hint
              inputDecorationTheme: InputDecorationTheme(
                  labelStyle: TextStyle(color: Colors.blue), //定义label字体样式
                  hintStyle:
                      TextStyle(color: Colors.green, fontSize: 14.0) //定义提示文本样式
                  ),
            ),
            child: TextField(
              autofocus: true,
              decoration: InputDecoration(
                  labelText: "用户名",
                  hintText: "用户名或邮箱",
                  prefixIcon: Icon(Icons.person)),
            ),
          )
        ],
      ),
    );
  }
}

void main(List<String> args) {
  runApp(MaterialApp(
    home: Scaffold(body: Demo()),
  ));
}
