import 'package:flutter/material.dart';

class FormDemo extends StatefulWidget {
  @override
  State createState() => _FormDemo();
}

class _FormDemo extends State<FormDemo> {
  TextEditingController _unameController = TextEditingController();
  TextEditingController _pwdController = TextEditingController();
  GlobalKey _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey, //设置globalKey，用于后面获取FormState
      autovalidateMode: AutovalidateMode.onUserInteraction,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          TextFormField(
            autofocus: true,
            controller: _unameController,
            decoration: InputDecoration(
              labelText: "用户名",
              hintText: "用户名或邮箱",
              icon: Icon(Icons.person),
            ),
            // 校验用户名
            validator: (v) {
              return v!.trim().isNotEmpty ? null : "用户名不能为空";
            },
          ),
          TextFormField(
            controller: _pwdController,
            decoration: InputDecoration(
              labelText: "密码",
              hintText: "您的登录密码",
              icon: Icon(Icons.lock),
            ),
            obscureText: true,
            //校验密码
            validator: (v) {
              return v!.trim().length > 5 ? null : "密码不能少于6位";
            },
          ),
          Text("不用通过 _unameController,  _unameController来获取值，然后验证，太慢了..."),
          Text("FormState.validate即可，所有表单内容一次验证 ~"),
          Text("FormState为Form的State类，可以通过Form.of()或GlobalKey获得"),

          SizedBox(height: 10),
          Text("globalKey有点类似于React.useRef(), 创建一个对象，指向form"),
          // 登录按钮
          Padding(
            padding: const EdgeInsets.only(top: 28.0),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: ElevatedButton(
                    child: Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Text("登录"),
                    ),
                    onPressed: () {
                      // 通过_formKey.currentState 获取FormState后，
                      // 调用validate()方法校验用户名密码是否合法，校验
                      // 通过后再提交数据。
                      if ((_formKey.currentState as FormState).validate()) {
                        //验证通过提交数据
                      }
                    },
                  ),
                ),
              ],
            ),
          ),

          SizedBox(height: 10),
          Text("Form.of(context)获取FormState"),
          ElevatedButton(
            child: Text("登录1, Form.of(context)获取失败"),
            onPressed: () {
              if (Form.of(context).validate()) {
                // 此处的context是FormDemo的context, 在form之外,
                // Form.of(context)可能是找context的父节点处的form，没有哦。。。
              }
            },
          ),

          SizedBox(height: 10),
          Builder(
            builder: (context) => ElevatedButton(
              child: Text("登录2, Form.of(context)可以校验"),
              onPressed: () {
                if (Form.of(context).validate()) {
                  //
                }
              },
            ),
          ),

          SizedBox(height: 50),
          Text("总结：3个重要概念： Form, FormField extends TextField, FormState")
        ],
      ),
    );
  }
}

void main(List<String> args) {
  runApp(MaterialApp(
    home: Scaffold(body: FormDemo()),
  ));
}
