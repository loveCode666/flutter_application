import 'package:flutter/material.dart';

class TestText extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("1. 一些普通属性："),
        Text("Hello world! " * 8, textAlign: TextAlign.right),
        Text("Hello world! I'm Jack. " * 4,
            maxLines: 1, overflow: TextOverflow.ellipsis),
        // 相对于当前字体的比例大小
        Text("Hello world", textScaleFactor: 1.5),

        SizedBox(height: 30),
        Text("2. textStyle:"),
        Text(
          "Hello world",
          style: TextStyle(
            color: Colors.blue,
            fontSize: 18.0, // 绝对大小，textScaleFactor为相对比例大小
            height: 1.2, // 真正行高 = fontSize * height
            fontFamily: "Courier",
            background: Paint()
              ..color = Colors.yellow, // 什么意思？ 和Colors.yellow有何不同呢？
            decoration: TextDecoration.underline,
            decorationStyle: TextDecorationStyle.dashed,
          ),
        ),

        SizedBox(height: 30),
        Text("3. TextSpan: text一次只能显示一种样式, 而span可以多种组合:"),
        Text("3.1: 用row拼接多个字符呗"),
        Row(
          children: [
            Text("Home: "),
            Text(
              "https://flutterchina.club",
              style: TextStyle(color: Colors.blue),
            )
          ],
        ),
        Text("3.2: 官方还搞了个这么复杂的api: ...."),
        Text.rich(TextSpan(children: [
          TextSpan(text: "Home: "),
          TextSpan(
            text: "https://flutterchina.club",
            style: TextStyle(color: Colors.blue),
            //  recognizer: _tapRecognizer
          ),
        ])),

        // 不是说万物皆widget吗？这里是怎么限制不让直接放TextSpan的呢？ 因为没有从Widget类继承而来？
        // TextSpan(text: "Home: "),

        SizedBox(height: 30),
        Text("4. DefaultTextStyle: 样式继承："),
        DefaultTextStyle(
          //1.设置文本默认样式
          style: TextStyle(
            color: Colors.red,
            fontSize: 20.0,
          ),
          textAlign: TextAlign.center,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text("hello world! " * 2),
              Text("I am Jack"),
              Text(
                "I am Jack",
                style: TextStyle(
                    inherit: false, //2.不继承默认样式
                    color: Colors.grey),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

void main(List<String> args) {
  runApp(
    MaterialApp(
      home: Scaffold(body: TestText()),
    ),
  );
}
