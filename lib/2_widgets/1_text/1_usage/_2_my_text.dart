import 'package:flutter/material.dart';
import '../2_source/my_text.dart';

class TestText extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        MyText("1. 一些普通属性："),
        MyText("Hello world! " * 8, textAlign: TextAlign.right),
        MyText("Hello world! I'm Jack. " * 4,
            maxLines: 1, overflow: TextOverflow.ellipsis),
        // 相对于当前字体的比例大小
        MyText("Hello world", textScaleFactor: 1.5),

        SizedBox(height: 30),
        MyText("2. textStyle:"),
        MyText(
          "Hello world",
          style: TextStyle(
            color: Colors.blue,
            fontSize: 18.0, // 绝对大小，textScaleFactor为相对比例大小
            height: 1.2, // 真正行高 = fontSize * height
            fontFamily: "Courier",
            background: Paint()
              ..color = Colors.yellow, // 什么意思？ 和Colors.yellow有何不同呢？
            decoration: TextDecoration.underline,
            decorationStyle: TextDecorationStyle.dashed,
          ),
        ),

        SizedBox(height: 30),
        MyText("3. TextSpan: text一次只能显示一种样式, 而span可以多种组合:"),

        MyText.rich(
          TextSpan(children: [
            TextSpan(text: "Home: "),
            TextSpan(
              text: "https://flutterchina.club",
              style: TextStyle(color: Colors.blue),
              //  recognizer: _tapRecognizer
            ),
          ]),
        ),

        SizedBox(height: 30),
        MyText("4. DefaultTextStyle: 样式继承："),
        DefaultTextStyle(
          //1.设置文本默认样式
          style: TextStyle(
            color: Colors.red,
            fontSize: 20.0,
          ),
          textAlign: TextAlign.center,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              MyText("hello world! " * 2),
              MyText("I am Jack"),
              MyText(
                "I am Jack",
                style: TextStyle(
                    inherit: false, //2.不继承默认样式
                    color: Colors.grey),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

void main(List<String> args) {
  runApp(
    MaterialApp(
      home: Scaffold(body: TestText()),
    ),
  );
}
