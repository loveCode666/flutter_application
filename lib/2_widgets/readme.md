# 1. 概念
Flutter中几乎所有的对象都是一个widget 
- UI元素，
- 功能性的组件如：用于手势检测的 GestureDetector 、用于APP主题数据传递的 Theme等等

# 2. widget

```dart
@immutable // 不可变的
abstract class Widget extends DiagnosticableTree {
  const Widget({ this.key });

  final Key? key;
  ...
```
- 不允许 Widget 中定义的属性变化呢？

Flutter 中如果属性发生变化则会重新构建Widget树，即重新创建新的 Widget 实例来替换旧的 Widget 实例，所以允许 Widget 的属性变化是没有意义的，所以属性也需要设置为final? 
- 继承关系

Widget(abstract class) extends DiagnosticableTree(abstract class) with Diagnosticable(mixin)

class A extends [StatefulWidget(abstract class) || StatelessWidget(abstract class)] extends Widget

# 3. flutter四棵树
Widget 树 -> Element树 -> Render树 -> Layer树
![flutter树](./trees.jpg)
# 4. StatelessWidget
不维护状态
- build
- context
# 5. StatefulWidget

# 6. State
![flutter生命周期](./life_cycle.jpg)







