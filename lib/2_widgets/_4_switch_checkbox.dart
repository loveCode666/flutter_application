import 'package:flutter/material.dart';

class SwitchAndCheckBoxTestRoute extends StatefulWidget {
  @override
  State createState() => _SwitchAndCheckBoxTestRouteState();
}

enum RadioValue { android, iOS, flutter }

class _SwitchAndCheckBoxTestRouteState
    extends State<SwitchAndCheckBoxTestRoute> {
  bool _switchSelected = true; //维护单选开关状态

  bool _checkboxSelected = true; //维护复选框状态
  bool _checkboxSelected2 = false;
  bool _checkboxSelected3 = true;
  RadioValue _radioValue = RadioValue.flutter;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text("1. Ridio"),
        Row(
          children: [
            Radio(
              // value 和 groupValue匹配上了，才会被选择上
              value: RadioValue.android,
              groupValue: _radioValue,
              onChanged: (value) => setState(() => _radioValue = value!),
            ),
            const Text(
              'android',
              style: TextStyle(fontSize: 18.0),
            ),
            Radio(
                value: RadioValue.iOS,
                groupValue: _radioValue,
                onChanged: (value) {
                  // 更新_radioValue
                  setState(() {
                    _radioValue = value!;
                  });
                }),
            const Text(
              'iOS',
              style: TextStyle(fontSize: 18.0),
            ),
            Radio(
                value: RadioValue.flutter,
                groupValue: _radioValue,
                onChanged: (value) {
                  // 更新_radioValue
                  setState(() {
                    _radioValue = value!;
                  });
                }),
            const Text(
              'Flutter',
              style: TextStyle(fontSize: 18.0),
            ),
          ],
        ),
        SizedBox(height: 30),
        Text("2. Switch"),
        Switch(
          value: _switchSelected, //当前状态
          onChanged: (value) {
            //重新构建页面
            setState(() {
              _switchSelected = value;
            });
          },
        ),
        Switch(value: false, onChanged: (bool value) {}),
        SizedBox(height: 30),
        Text("3. Checkbox: 怎么提交值到form呢? 大家的value都是bool"),
        Row(
          children: [
            Checkbox(
              value: _checkboxSelected,
              activeColor: Colors.red, //选中时的颜色
              onChanged: (value) {
                setState(() {
                  _checkboxSelected = value!;
                });
              },
            ),
            Text("唐三"),
            Checkbox(
              value: _checkboxSelected2,
              onChanged: (value) {
                setState(() {
                  _checkboxSelected2 = value!;
                });
              },
            ),
            Text("比比东"),
            Checkbox(
              value: _checkboxSelected3,
              activeColor: Colors.green, //选中时的颜色
              onChanged: (value) {
                setState(() {
                  _checkboxSelected3 = value!;
                });
              },
            ),
            Text("辛弃疾"),
          ],
        ),
        SizedBox(height: 30),
        Text("4. state是组件本身维护，还是父组件来管理呢？"),
      ],
    );
  }
}

void main(List<String> args) {
  runApp(MaterialApp(
    home: Scaffold(
      body: SwitchAndCheckBoxTestRoute(),
    ),
  ));
}
