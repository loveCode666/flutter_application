import 'package:flutter/material.dart';

class ImageTest extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    print(Icons.alarm.codePoint.toRadixString(16));
    String icons = "";
// accessible: 0xe03e
    icons += "\uE03e";
// error:  0xe237
    icons += " \uE237";
// fingerprint: 0xe287
    icons += " \uE287";
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
              "Image(image: AssetImage|NetworkImage), 而且都有静态函数Image.asset| Image.network"),
          Text("1. 本地图片"),
          Image(image: AssetImage("images/flutter.jpg")),
          Image.asset("images/flutter.jpg"),
          SizedBox(height: 30),
          Text("2. 网络图片:"),
          Image(
            image: NetworkImage(
                "https://nimg.ws.126.net/?url=http%3A%2F%2Fdingyue.ws.126.net%2F2022%2F0130%2F366f46ddj00r6he2n000rd000hs00alp.jpg&thumbnail=650x2147483647&quality=80&type=jpg"),
          ),
          Image.network(
              "https://nimg.ws.126.net/?url=http%3A%2F%2Fdingyue.ws.126.net%2F2022%2F0130%2F366f46ddj00r6he2n000rd000hs00alp.jpg&thumbnail=650x2147483647&quality=80&type=jpg"),
          SizedBox(height: 30),
          Text("2. 参数设置："),
          Text("2.1 fit适应模式"),
          Wrap(
            children: [
              Container(
                  width: 300,
                  height: 300,
                  decoration: BoxDecoration(
                    border: Border.all(width: 1, color: Colors.red),
                  ),
                  child: Image.asset('images/xiaoyan.jpg', width: 300)),
              SizedBox(width: 20),
              Text(
                  "只设置width/height中的一个，另外一个属性会默认等比例缩放。widht=200, 高度自适应。如果宽高都设置，需要配合fit来显示"),
            ],
          ),
          ImageFit(BoxFit.fill, "BoxFit.fill: 按照宽高拉伸充满整个空间，图片变形"),
          ImageFit(
              BoxFit.contain, "BoxFit.contain: 图片的默认适应规则，本身比例不变的情况下适应显示空间"),
          ImageFit(BoxFit.cover, "BoxFit.cover： 按照图片等比例缩放 & 撑满空间，取小边，超出部分裁剪"),
          ImageFit(BoxFit.fitWidth, "BoxFit.fitWidth: 适应宽度，高度缩放后裁剪"),
          ImageFit(BoxFit.fitHeight, "BoxFit.fitHeight：适应高度，宽度缩放后裁剪"),
          ImageFit(BoxFit.scaleDown,
              "BoxFit.scaleDown：没啥用吧，是contain & none的组合体。This is the same as `contain` if that would shrink the image, otherwise it is the same as `none`."),
          ImageFit(BoxFit.none, "BoxFit.none: 在空间内展示部分原图，一般是中间部分"),
          SizedBox(height: 30),
          Text("3. repeat重复，这种一般适合背景设置"),
          Container(
            decoration: BoxDecoration(
              border: Border.all(width: 1, color: Colors.red),
            ),
            child: Image.asset(
              "images/grass.png",
              height: 400,
              repeat: ImageRepeat.repeat,
            ),
          ),
          SizedBox(height: 30),
          Text("4. Icons类似于文本。每个16进制编码对应一个矢量图？"),
          Text("好处：1. 不用额外引入，flutter中集成了。2. 可以和文本一样改变大小和颜色。3. 矢量图，放大不失真"),
          Text("4.1 直接将编码写到text中。可以和text， textspan等混用，但是可读性不强哦 ~"),
          Text(
            icons,
            style: TextStyle(
              fontFamily: "MaterialIcons",
              fontSize: 24.0,
              color: Colors.green,
            ),
          ),
          Text("4.2 Icon & IconData。flutter帮我们把icon十六进制封装成了IconData, Icon"),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(Icons.accessible, color: Colors.green),
              Icon(Icons.error, color: Colors.green),
              Icon(Icons.fingerprint, color: Colors.green),
            ],
          )
        ],
      ),
    );
  }
}

class ImageFit extends StatelessWidget {
  const ImageFit(this.fit, this.text, {super.key});
  final BoxFit fit;
  final String text;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          margin: EdgeInsets.only(top: 20),
          decoration: BoxDecoration(
            border: Border.all(width: 1, color: Colors.red),
          ),
          child: Image.asset('images/xiaoyan.jpg',
              width: 300, height: 300, fit: fit),
        ),
        SizedBox(width: 20),
        Expanded(child: Text(text))
      ],
    );
  }
}

void main(List<String> args) {
  runApp(MaterialApp(
    home: Scaffold(body: ImageTest()),
  ));
}
