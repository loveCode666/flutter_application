import 'package:flutter/material.dart';

class TestButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text("这些button都是extends ButtonStyleButton"),
        // onPress为空就是禁用？
        ElevatedButton(onPressed: () {}, child: Text("ElevatedButton")),
        SizedBox(height: 30),
        TextButton(onPressed: () {}, child: Text("TextButton")),
        SizedBox(height: 30),
        OutlinedButton(onPressed: () {}, child: Text("OutlinedButton")),
        SizedBox(height: 30),
        Text('最原始的按钮？ 和ButtonStyleButton关系是?'),
        RawMaterialButton(onPressed: () {}, child: Text("RawMaterialButton")),

        SizedBox(height: 30),
        Text("来几个炫酷按钮:"),
        IconButton(icon: Icon(Icons.thumb_up), onPressed: () {}),

        SizedBox(height: 50),
        Text(
            "ElevatedButton、TextButton、OutlinedButton都有一个icon 构造函数, 他们比icon多了文字"),
        // RawMaterialButton.icon

        ElevatedButton.icon(
          onPressed: () {},
          icon: Icon(Icons.alarm),
          label: Text("ElevatedButton"),
        ),
        TextButton.icon(
          onPressed: () {},
          icon: Icon(Icons.add),
          label: Text("TextButton"),
        ),
        OutlinedButton.icon(
          onPressed: () {},
          icon: Icon(Icons.info),
          label: Text("OutlinedButton"),
        ),
      ],
    );
  }
}

void main(List<String> args) {
  runApp(MaterialApp(
    home: Scaffold(body: TestButton()),
  ));
}
