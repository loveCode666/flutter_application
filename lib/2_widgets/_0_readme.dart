import 'package:flutter/material.dart';

class Demo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("1. 四棵树"),
        Container(
          // 一个容器 widget
          color: Colors.blue, // 设置容器背景色
          child: Row(
            // 可以将子widget沿水平方向排列
            children: [
              Image.asset(
                "images/mbappe.jpeg",
                width: 200,
              ), // 显示图片的 widget
              const Text('Mbappe'),
            ],
          ),
        ),
        SizedBox(height: 10),
        Text("2. "),
      ],
    );
  }
}

void main(List<String> args) {
  runApp(MaterialApp(
    home: Scaffold(body: Demo()),
  ));
}
