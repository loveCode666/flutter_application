将依赖分为包和插件
1. 包：偏UI类的
2. 插件：系统类调用，借助他们和原生平台进行通信。访问相册，摄像头。

Flutter web
1. 支持所有平台：macOS, Windows, Linux, Android, iOS, Web
   write once, run anywhere.
2. web上的限制：
   a. 同源策略
   b. 没有访问文件系统的权限，只能上传，不可读取
   c. 对系统硬件权限访问限制：wifi, 蓝牙, 摄像头, 麦克风, 位置信息等.
3. web渲染器：支持JS和Wasm(CanvasKit )
4. flutter web的应用场景：
   a. 渐进式web应用(Progressive web apps, PWA)：
   b. 单页应用(Single-page application, SPA)
   c. 同一份代码拓展到web
