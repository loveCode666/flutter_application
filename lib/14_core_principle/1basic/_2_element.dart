import 'package:flutter/material.dart';

class HomeView extends ComponentElement {
  HomeView(Widget widget) : super(widget);
  String text = "123456789";

  @override
  Widget build() {
    // this就是当前Element
    Color primary = Theme.of(this).primaryColor; //1
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text("class直接extends Element, context的地方可以用this"),
          Text("setState内部调用的是markNeedsBuild方法"),
          TextButton(
            child: Text(
              text,
              style: TextStyle(color: primary),
            ),
            onPressed: () {
              var t = text.split("")..shuffle();
              text = t.join();
              //点击后将该Element标记为dirty，Element将会rebuild
              markNeedsBuild();
            },
          )
        ],
      ),
    );
  }
}

class CustomHome extends Widget {
  @override
  Element createElement() {
    return HomeView(this);
  }
}

void main(List<String> args) {
  runApp(MaterialApp(
    theme: ThemeData(primaryColor: Colors.red),
    home: Scaffold(
      appBar: AppBar(title: Text("Custom Home")),
      body: CustomHome(),
    ),
  ));
}
