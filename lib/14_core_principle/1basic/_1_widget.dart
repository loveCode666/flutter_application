import 'package:flutter/material.dart';

class Demo extends StatefulWidget {
  const Demo({super.key});

  @override
  State<Demo> createState() => _DemoState();
}

class _DemoState extends State<Demo> {
  String text = "123456789";
  @override
  Widget build(BuildContext context) {
    Color primary = Theme.of(context).primaryColor;
    return Center(
      child: TextButton(
        child: Text(
          text,
          style: TextStyle(color: primary),
        ),
        onPressed: () {
          var t = text.split("")..shuffle();
          text = t.join();
          // markNeedsBuild(); //点击后将该Element标记为dirty，Element将会rebuild
          setState(() {
            text = text;
          });
        },
      ),
    );
  }
}

void main(List<String> args) {
  runApp(MaterialApp(
    theme: ThemeData(primaryColor: Colors.red),
    home: Scaffold(
      appBar: AppBar(title: Text("Custom Home")),
      body: Demo(),
    ),
  ));
}
