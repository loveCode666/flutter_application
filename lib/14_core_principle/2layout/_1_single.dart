import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class CustomCenter extends SingleChildRenderObjectWidget {
  const CustomCenter({Key? key, required Widget child})
      : super(key: key, child: child);

  @override
  RenderObject createRenderObject(BuildContext context) {
    return RenderCustomCenter();
  }
}

class RenderCustomCenter extends RenderShiftedBox {
  RenderCustomCenter({RenderBox? child}) : super(child);

  @override
  void performLayout() {
    //1. 先对子组件进行layout，随后获取它的size
    child!.layout(
      // constraints是CustomCenter的父组件传递过来的约束条件
      constraints.loosen(), // 将约束传递给子节点,约束子节点最大宽高不超过自身的最大宽高
      parentUsesSize: true, // 因为我们接下来要使用child的size,所以不能为false
    );
    //2.根据子组件的大小确定自身的大小
    size = constraints.constrain(Size(
      constraints.maxWidth == double.infinity
          ? child!.size.width
          : double.infinity,
      constraints.maxHeight == double.infinity
          ? child!.size.height
          : double.infinity,
    ));

    // 3. 根据父节点子节点的大小，算出子节点在父节点中居中之后的偏移，然后将这个偏移保存在
    // 子节点的parentData中，在后续的绘制阶段，会用到。
    BoxParentData parentData = child!.parentData as BoxParentData;
    parentData.offset = ((size - child!.size) as Offset) / 2;
  }
}

class Demo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // Widget columnWidget = Column(
    //   crossAxisAlignment: CrossAxisAlignment.start,
    //   children: [
    //     Text("1. 自定义类似于Center的组件"),
    //     SizedBox(height: 10),
    //     Text("2. "),
    //   ],
    // );

    // return Center(child: columnWidget);
    // return CustomCenter(child: columnWidget);

    return CustomCenter(
      child: Container(
        width: 100,
        height: 100,
        decoration: BoxDecoration(color: Colors.red),
      ),
    );
  }
}

void main(List<String> args) {
  runApp(MaterialApp(
    home: Scaffold(body: Demo()),
  ));
}
