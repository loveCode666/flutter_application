Flutter 从上到下分为框架层、引擎层和嵌入层三层：

- 框架层：Flutter Framework，负责UI的渲染、手势处理、动画等
- 引擎层：Skia引擎，负责Flutter的绘制、布局、渲染等
- 嵌入层：Android和iOS的SDK，负责Flutter和原生平台的通信

Flutter的渲染流程：
根据Widget生成Element，然后创建相应的RenderObject并关联到Element.renderObject属性上，最后再通过RenderObject来完成布局排列和绘制
![flutter UI 渲染树](./ui-tree.png)

# 1、有三个基础组件：Element, BuildContext, RenderObject
Element：Element是Flutter Framework和Engine之间的桥梁，它将Widget和RenderObject关联起来，并且Element还负责Element的创建、更新和销毁等操作。
BuildContext：BuildContext是Flutter Framework提供的一个上下文对象，它用于在Widget树中传递和获取信息。BuildContext就是widget对应的Element
RenderObject：RenderObject是Flutter Framework提供的一个抽象类，它用于描述一个Widget的绘制和布局信息。

# 2、Flutter启动流程和渲染流程
## 启动流程
在runApp中启动, 对象WidgetsFlutterBinding初始化
WidgetsFlutterBinding.ensureInitialized() // 负责初始化一个WidgetsBinding的全局单例
..attachRootWidget(app)  // 将根Widget添加到RenderView上
..scheduleWarmUpFrame(); // 被调用后会立即进行一次绘制，在此次绘制结束前，该方法会锁定事件分发，也就是说在本次绘制结束完成之前 Flutter 将不会响应各种事件，这可以保证在绘制过程中不会再触发新的重绘。

## 渲染流程
Frame:
一次绘制过程，我们称其为一帧（frame), ??
Flutter 调度过程 SchedulerPhase

当有新的 frame 到来时，具体处理过程就是依次执行四个任务队列：transientCallbacks、midFrameMicrotasks、persistentCallbacks、postFrameCallbacks，
Flutter 将整个生命周期分为五种状态，通过 SchedulerPhase 枚举类来表示它们
- idle: 空闲状态，表示没有需要执行的任务
- transientCallbacks: 表示临时任务
- midFrameMicrotasks: 表示在绘制过程中产生的微任务
- persistentCallbacks: 表示持久任务
- postFrameCallbacks: 表示绘制完成后的任务

## setState执行
1、调用当前 element 的 markNeedsBuild 方法，将当前 element标记为 dirty 。
2、调用 scheduleBuildFor，将当前 element 添加到pipelineOwner的 dirtyElements 列表。
3、请求一个新的 frame，随后会绘制新的 frame：onBuildScheduled->ensureVisualUpdate->scheduleFrame() 。

# 布局更新
## 1. 布局边界（relayoutBoundary）
![边界渲染树](./boundary_tree.png)
Column2就是Text3的relayoutBoundary， 因为其满足一个原则四个场景：？
原则是“组件自身的大小变化不会影响父组件”

## markNeedsLayout


