import 'package:flutter/material.dart';
import 'package:namer_app/6_function/_6_0_showCustomDialog.dart';

class Demo extends StatelessWidget {
  Widget getAlertDialog(BuildContext context) {
    return AlertDialog(
      title: Text("提示"),
      content: Text("您确定要删除当前文件吗?"),
      actions: <Widget>[
        TextButton(
          child: Text("取消"),
          onPressed: () => Navigator.of(context).pop(), // 关闭对话框
        ),
        TextButton(
          child: Text("删除"),
          onPressed: () {
            //关闭对话框并返回true
            Navigator.of(context).pop(true);
          },
        ),
      ],
    );
  }

  Future<bool?> showAlertDialog(BuildContext context) {
    return showDialog<bool>(
      context: context,
      builder: getAlertDialog,
    );
  }

  Future<void> showSimpleDialog(context) async {
    int? i = await showDialog<int>(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            title: const Text('请选择语言'),
            children: <Widget>[
              SimpleDialogOption(
                onPressed: () {
                  // 返回1
                  Navigator.pop(context, 1);
                },
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 6),
                  child: const Text('中文简体'),
                ),
              ),
              SimpleDialogOption(
                onPressed: () {
                  // 返回2
                  Navigator.pop(context, 2);
                },
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 6),
                  child: const Text('美国英语'),
                ),
              ),
            ],
          );
        });

    if (i != null) {
      print("选择了：${i == 1 ? "中文简体" : "美国英语"}");
    }
  }

  Future<void> showListDialog(context) async {
    int? index = await showDialog<int>(
      context: context,
      builder: (BuildContext context) {
        var child = Column(
          children: <Widget>[
            ListTile(title: Text("请选择")),
            Expanded(
                child: ListView.builder(
              itemCount: 30,
              itemBuilder: (BuildContext context, int index) {
                return ListTile(
                  title: Text("$index"),
                  onTap: () => Navigator.of(context).pop(index),
                );
              },
            )),
          ],
        );
        //使用AlertDialog,SimpleDialog都会报错
        // return AlertDialog(content: child);
        // return SimpleDialog(children: [child]);
        // return Dialog(child: child);
        // 也不一定非要用AlertDialog, SimpleDialog, Dialog, 才能弹出对话框，也能自定义
        // 全屏的对话框了
        Widget fullScreen = ConstrainedBox(
          constraints: BoxConstraints(maxWidth: 280),
          child: Material(
            type: MaterialType.card,
            child: child,
          ),
        );
        // return fullScreen;
        // 不明觉厉，这样就是对话框了？
        return UnconstrainedBox(
          constrainedAxis: Axis.vertical,
          child: fullScreen,
        );
      },
    );
    if (index != null) {
      print("点击了：$index");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("1. AlertDialog弹出对话框, 需要点击取消或者确定后再消失"),
        //点击该按钮后弹出对话框
        ElevatedButton(
          child: Text("AlertDialog"),
          onPressed: () async {
            //弹出对话框并等待其关闭
            bool? delete = await showAlertDialog(context);
            if (delete == null) {
              print("取消删除");
            } else {
              print("已确认删除");
              //... 删除文件
            }
          },
        ),
        SizedBox(height: 10),
        Text("2. SimpleDialog用于弹出对话框后列表选择场景"),
        ElevatedButton(
          child: Text("SimpleDialog"),
          onPressed: () => showSimpleDialog(context),
        ),
        SizedBox(height: 10),
        Text(
            "3. AlertDialog和SimpleDialog中使用了IntrinsicWidth来尝试通过子组件的实际尺寸来调整自身尺寸，这就导致他们的子组件不能是延迟加载模型的组件（如ListView、GridView 、 CustomScrollView等）"),
        Text("但是可以用Dialog"),
        ElevatedButton(
          child: Text("AlertDialog, SimpleDialog会报错，得是Dialog"),
          onPressed: () => showListDialog(context),
        ),
        SizedBox(height: 10),
        Text("4. 普通风格的Dialog，showGeneralDialog, 点击外层遮罩还消不掉弹框"),
        ElevatedButton(
          child: Text("showGeneralDialog"),
          onPressed: () => showGeneralDialog(
            context: context,
            pageBuilder: (BuildContext buildContext,
                Animation<double> animation,
                Animation<double> secondaryAnimation) {
              return getAlertDialog(context);
            },
            // barrierColor: Colors.red.withAlpha(100),
            barrierColor: Colors.red.withOpacity(0.3),
          ),
        ),
        SizedBox(height: 10),
        Text("5. 自定义风格弹框, showCustomDialog"),
        ElevatedButton(
          child: Text("showCustomDialog"),
          onPressed: () => showCustomDialog(
            context: context,
            builder: getAlertDialog,
          ),
        ),
      ],
    );
  }
}

void main(List<String> args) {
  runApp(MaterialApp(
    home: Scaffold(body: Demo()),
  ));
}
