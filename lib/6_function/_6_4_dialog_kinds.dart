import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:namer_app/6_function/_6_0_showCustomDialog.dart';

class Demo extends StatelessWidget {
  Widget getAlertDialog(BuildContext context) {
    return AlertDialog(
      title: Text("提示"),
      content: Text("您确定要删除当前文件吗?"),
      actions: <Widget>[
        TextButton(
          child: Text("取消"),
          onPressed: () => Navigator.of(context).pop(), // 关闭对话框
        ),
        TextButton(
          child: Text("删除"),
          onPressed: () {
            //关闭对话框并返回true
            Navigator.of(context).pop(true);
          },
        ),
      ],
    );
  }

  Widget sizedLoading() {
    return SizedBox(
      width: 280,
      child: AlertDialog(
        content: Column(
          mainAxisSize: MainAxisSize.min,
          // 么有横向size？
          children: <Widget>[
            CircularProgressIndicator(),
            Padding(
              padding: const EdgeInsets.only(top: 26.0),
              child: Text("正在加载，请稍后..."),
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("展示各种各样的对话框"),
        //点击该按钮后弹出对话框
        ElevatedButton(
          child: Text("1. showDialog"),
          onPressed: () async {
            await showDialog<bool>(
              context: context,
              builder: getAlertDialog,
              // builder: (BuildContext context) => Text("haha"),
            );
          },
        ),
        SizedBox(height: 10),
        ElevatedButton(
          child: Text("2. 底部菜单列表"),
          onPressed: () async {
            await showModalBottomSheet<bool>(
              context: context,
              // builder: getAlertDialog,
              builder: (BuildContext context) => ListView.builder(
                itemCount: 30,
                itemBuilder: (BuildContext context, int index) {
                  return ListTile(
                    title: Text("$index"),
                    onTap: () => Navigator.of(context).pop(index),
                  );
                },
              ),
            );
          },
        ),
        SizedBox(height: 10),
        ElevatedButton(
          child: Text("3. Loading框: 还是showDialog+AlertDialog"),
          onPressed: () async {
            await showDialog<bool>(
              context: context,
              builder: (context) {
                return AlertDialog(
                  content: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      CircularProgressIndicator(),
                      Padding(
                        padding: const EdgeInsets.only(top: 26.0),
                        child: Text("正在加载，请稍后..."),
                      )
                    ],
                  ),
                );
              },
            );
          },
        ),

        SizedBox(height: 10),
        ElevatedButton(
          child: Text("4. Loading框减小: 只设置Sizebox没效果，还得靠"),
          onPressed: () async {
            await showDialog<bool>(
              context: context,
              builder: (context) {
                // 没啥用
                // return sizedLoading();
                return UnconstrainedBox(
                  // 这就奇怪了，为啥改的是垂直方向呢？
                  constrainedAxis: Axis.vertical,
                  child: sizedLoading(),
                );
              },
            );
          },
        ),

        SizedBox(height: 10),
        ElevatedButton(
          child: Text("5. DatePicker"),
          onPressed: () async {
            var date = DateTime.now();
            await showDatePicker(
              context: context,
              initialDate: date,
              firstDate: date,
              lastDate: date.add(
                //未来30天可选
                Duration(days: 30),
              ),
            );
          },
        ),

        SizedBox(height: 10),
        ElevatedButton(
          child: Text("6. IOS风格DatePicker"),
          onPressed: () async {
            var date = DateTime.now();
            await showCupertinoModalPopup(
              context: context,
              builder: (ctx) {
                return SizedBox(
                  height: 200,
                  child: CupertinoDatePicker(
                    mode: CupertinoDatePickerMode.dateAndTime,
                    minimumDate: date,
                    maximumDate: date.add(Duration(days: 30)),
                    maximumYear: date.year + 1,
                    onDateTimeChanged: (DateTime value) {
                      print(value);
                    },
                  ),
                );
              },
            );
          },
        ),
      ],
    );
  }
}

void main(List<String> args) {
  runApp(MaterialApp(
    home: Scaffold(body: Demo()),
  ));
}
