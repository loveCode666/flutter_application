import 'package:flutter/material.dart';

class NavBar extends StatelessWidget {
  final String title;
  final Color color; //背景颜色

  NavBar({
    Key? key,
    required this.color,
    required this.title,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 52,
      color: color,
      alignment: Alignment.center,
      child: Text(
        title,
        style: TextStyle(
          fontWeight: FontWeight.bold,
          //根据背景色亮度来确定Title颜色
          color: color.computeLuminance() < 0.5 ? Colors.white : Colors.black,
        ),
      ),
    );
  }
}

class Demo extends StatelessWidget {
  const Demo({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        //背景为蓝色，则title自动为白色
        NavBar(color: Colors.blue, title: "标题"),
        //背景为白色，则title自动为黑色
        NavBar(color: Colors.white, title: "标题"),
        NavBar(color: Colors.red, title: "标题"),
        SizedBox(height: 50),
        Text("MaterialColor定义了10种级别，由浅到深"),
        Container(width: 200, height: 50, color: Colors.blue),
        SizedBox(height: 5),
        Container(width: 200, height: 50, color: Colors.blue[100]),
        SizedBox(height: 5),
        Container(width: 200, height: 50, color: Colors.blue.shade100),
        SizedBox(height: 5),
        Container(width: 200, height: 50, color: Colors.blue.shade900),
        SizedBox(height: 5),
      ],
    );
  }
}

void main(List<String> args) {
  runApp(MaterialApp(
    home: Scaffold(body: Demo()),
  ));
}
