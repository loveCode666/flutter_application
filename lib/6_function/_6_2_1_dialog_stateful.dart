import 'package:flutter/material.dart';

class Demo extends StatelessWidget {
  bool withTree = false; // 复选框选中状态
  Widget getAlertDialog(BuildContext context) {
    return AlertDialog(
      title: Text("提示"),
      content: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Text("您确定要删除当前文件吗?"),
          Text("同时删除子目录？ "),
          Row(
            children: [
              // Checkbox(
              //   value: withTree,
              //   onChanged: (bool? value) => setState(() {
              //     withTree = !withTree;
              //   }),
              // )

              // 这样写虽然不影响checkebox选中与不选中，但是还差点意思：状态没办法往上抛出来
              Text("不往上抛出状态"),
              CheckBoxState(checked: withTree),
            ],
          ),
          Row(
            children: [
              Text("往上抛出状态"),
              CheckBoxState(
                  checked: withTree,
                  changeState: (bool value) => withTree = value),
            ],
          )
        ],
      ),
      actions: <Widget>[
        TextButton(
          child: Text("取消"),
          onPressed: () => Navigator.of(context).pop(), // 关闭对话框
        ),
        TextButton(
          child: Text("删除"),
          onPressed: () {
            //关闭对话框并返回true
            Navigator.of(context).pop(withTree);
          },
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
            "既然是context不对，那么直接的思路就是将复选框的选中逻辑单独封装成一个StatefulWidget，然后在其内部管理复选状态"),
        //点击该按钮后弹出对话框
        ElevatedButton(
          child: Text("AlertDialog"),
          onPressed: () async {
            //弹出对话框并等待其关闭
            bool? delete = await showDialog<bool>(
              context: context,
              builder: getAlertDialog,
            );
            if (delete == null) {
              print("取消删除");
            } else {
              print("已确认删除。 删除子文件: $delete");
              //... 删除文件
            }
          },
        ),
      ],
    );
  }
}

class CheckBoxState extends StatefulWidget {
  CheckBoxState({super.key, required this.checked, this.changeState});
  final bool checked;
  final ValueChanged<bool>? changeState;

  @override
  State<CheckBoxState> createState() => _CheckBoxStateState();
}

class _CheckBoxStateState extends State<CheckBoxState> {
  bool? checkedState;

  @override
  void initState() {
    checkedState = widget.checked;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Checkbox(
        value: checkedState,
        onChanged: (bool? value) {
          setState(() => checkedState = value);
          widget.changeState?.call(value!);
          // if (widget.changeState != null) {
          //   widget.changeState!(value!);
          // }
        });
  }
}

void main(List<String> args) {
  runApp(MaterialApp(
    home: Scaffold(body: Demo()),
  ));
}
