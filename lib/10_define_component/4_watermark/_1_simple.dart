import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class Watermark extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    DateTime now = DateTime.now();
    String formattedDate = DateFormat('yyyy-MM-dd HH:mm').format(now);

    return Transform.rotate(
      angle: -30 * 3.14159265359 / 180,
      child: Opacity(
        opacity: 0.3,
        child: Text(
          '天健熊忠波 $formattedDate',
          // style: TextStyle(
          //   fontSize: 24.0,
          //   color: Colors.red,
          // ),
        ),
      ),
    );
  }
}

class Demo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      // fit: StackFit.expand,
      children: [
        // 1. 正常的应用内容
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("这里是程序的正常内容"),
            SizedBox(height: 30),
            Text("原理："),
            Text("1. 使用Stack，正常的应用内容放在底层，而水印则放在上层"),
            Text(
                "2. Watermark类是一个带有水印的简单组件。它使用Transform.rotate将水印旋转45度，然后使用Opacity调整水印的透明度"),
            Text(
                "3. 单个水印可以通过Positioned控制位置，全屏水印动态渲染多个就好，List, ListView, GridView应该都可以的")
          ],
        ),

        // 2. 屏幕水印
        // 单个水印
        // Positioned(
        //   top: 0,
        //   left: 0,
        //   child: Watermark(),
        // ),

        // 全屏都是水印
        Wrap(
          runSpacing: 150.0, // 设置垂直方向上的距离
          children: List.generate(
            // 固定个数, 只要足够多，也能覆盖全屏
            100,
            (index) => Watermark(),
          ),
        ),
      ],
    );
  }
}

void main(List<String> args) {
  runApp(MaterialApp(
    home: Scaffold(
      appBar: AppBar(title: Text("简单展示水印")),
      body: Demo(),
    ),
  ));
}
