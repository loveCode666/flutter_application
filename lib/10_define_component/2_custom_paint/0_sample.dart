import 'dart:math';

import 'package:flutter/material.dart';

class Demo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("1. 自定义painter"),
        Text("class CustomPaint extends SingleChildRenderObjectWidget"),
        Text("abstract class CustomPainter extends Listenable"),
        Text("CustomPaint(painter: CustomPainter()"),
        Text(
            "核心还是CustomPainter中的void paint(Canvas canvas, Size size)回调，canvas.drawCircle、drawRect、drawLine等"),
        CustomPaint(
          painter: CirclePainter(),
          size: Size(200, 200), // 指定绘制区域的大小
        ),
        CustomPaint(
          painter: ArcPainter(Colors.blue),
          size: Size(200, 200), // 设置绘制区域的大小
        ),
        CustomPaint(
          painter: RectPainter(), // 应用 RectPainter
          size: Size(200, 200), // 设置 CustomPaint 的大小
        ),
        Text(
            "2. flutter中也有不少组件是SingleChildRenderObjectWidget的子类：Padding, Transform, Opacity, 一般是容器组件，装一个Child"),
        Padding(padding: EdgeInsets.all(10), child: Text("四周padding")),
        Opacity(opacity: 0.2, child: Text("透明度")),
        SizedBox(height: 10),
      ],
    );
  }
}

class CirclePainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint()
      ..color = Colors.red
      ..style = PaintingStyle.stroke;
    var center = Offset(size.width / 2, size.height / 2);
    var radius = size.width / 3;
    canvas.drawCircle(center, radius, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return oldDelegate != this;
  }
}

class ArcPainter extends CustomPainter {
  final Color color;

  ArcPainter(this.color);

  @override
  void paint(Canvas canvas, Size size) {
    final Paint paint = Paint()
      ..color = color
      ..style = PaintingStyle.fill
      ..isAntiAlias = true;

    // 定义弧形的起始角度和扫过角度
    const double startAngle = -pi / 2; // 从正上方开始
    const double sweepAngle = pi; // 扫过180度

    // 定义弧形的矩形边界
    final Rect rect = Rect.fromCenter(
      center: Offset(size.width / 2, size.height / 2),
      width: size.width * 0.8,
      height: size.height * 0.8,
    );

    // 绘制弧形
    canvas.drawArc(rect, startAngle, sweepAngle, true, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return oldDelegate != this;
  }
}

class RectPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    // 创建一个 Paint 对象
    final Paint paint = Paint()
      ..color = Colors.green // 设置颜色
      ..style = PaintingStyle.fill; // 设置样式为填充

    // 定义矩形的左上角和右下角的坐标
    final Rect rect = Rect.fromLTWH(50.0, 50.0, 100.0, 100.0);

    // 使用 drawRect 方法绘制矩形
    canvas.drawRect(rect, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    // 如果绘制逻辑没有变化，这里返回 false
    return oldDelegate != this;
  }
}

void main(List<String> args) {
  runApp(MaterialApp(
    home: Scaffold(body: Demo()),
  ));
}
