import 'dart:math';

import 'package:flutter/material.dart';

class CustomPaintRoute extends StatelessWidget {
  const CustomPaintRoute({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center, // 垂直居中
      children: [
        Row(),
        CustomPaint(
          size: Size(300, 300), //指定画布大小
          // 背景画笔，会显示在子节点后面;
          painter: MyPainter(),
          // 有子节点，为了避免子节点不必要的重绘并提高性能，通常情况下都会将子节点包裹在
          // RepaintBoundary组件中,在绘制时就会创建一个新的绘制层
          // child: RepaintBoundary(child: Text("666")),
        ),
        //添加一个刷新button, 它有个水波动画，此动画执行过程中画布会不停的刷新，所以就导致了CustomPaint 不停的重绘。
        ElevatedButton(onPressed: () {}, child: Text("刷新")),
        // 解决办法，给button或者CustomPaint添加一个RepaintBoundary组件，让其成为一个独立的画布
        RepaintBoundary(
            child: ElevatedButton(onPressed: () {}, child: Text("不会引起棋盘刷新"))),
      ],
    );
  }
}

class MyPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    print('paint');
    var rect = Offset.zero & size;
    //画棋盘
    drawChessboard(canvas, rect);
    //画棋子
    drawPieces(canvas, rect);
    // canvas可以画画
    // canvas.drawArc(rect, startAngle, sweepAngle, useCenter, paint)
  }

  // 返回false, 后面介绍
  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;

  // 棋盘绘制
  void drawChessboard(Canvas canvas, Rect rect) {
    //棋盘背景
    var paint = Paint()
      ..isAntiAlias = true // 是否抗锯齿
      ..style = PaintingStyle.fill //填充
      ..color = Color(0xFFDCC48C);
    canvas.drawRect(rect, paint);

    // 画棋盘网格
    paint
      ..style = PaintingStyle.stroke //线
      ..color = Colors.black38
      ..strokeWidth = 1.0;

    // 画横线
    for (int i = 0; i <= 15; ++i) {
      double dy = rect.top + rect.height / 15 * i;
      canvas.drawLine(Offset(rect.left, dy), Offset(rect.right, dy), paint);
    }

    for (int i = 0; i <= 15; ++i) {
      double dx = rect.left + rect.width / 15 * i;
      canvas.drawLine(Offset(dx, rect.top), Offset(dx, rect.bottom), paint);
    }
  }

  // 棋子绘制
  void drawPieces(Canvas canvas, Rect rect) {
    double eWidth = rect.width / 15;
    double eHeight = rect.height / 15;
    //画一个黑子
    var paint = Paint()
      ..style = PaintingStyle.fill
      ..color = Colors.black;
    //画一个黑子
    canvas.drawCircle(
      Offset(rect.center.dx - eWidth / 2, rect.center.dy - eHeight / 2),
      // min(eWidth / 2, eHeight / 2) - 2,
      eWidth / 2 - 2,
      paint,
    );
    //画一个白子
    paint.color = Colors.white;
    canvas.drawCircle(
      Offset(rect.center.dx + eWidth / 2, rect.center.dy - eHeight / 2),
      min(eWidth / 2, eHeight / 2) - 2,
      paint,
    );
  }
}

void main(List<String> args) {
  runApp(MaterialApp(
    home: Scaffold(body: CustomPaintRoute()),
  ));
}
