import 'package:flutter/material.dart';

class Demo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("1. 之前在4_container一章介绍过RotatedBox。"),
        DecoratedBox(
          decoration: BoxDecoration(color: Colors.red),
          child: RotatedBox(
            quarterTurns: 1,
            child: Text("Hello world"),
          ),
        ),
        Text("a. 只能以90度的倍数进行旋转。b. 旋转角度发生变化时，更新过程没有动画"),
        TestRotatedBox(),
        SizedBox(height: 10),
        Text("2. 自定义旋转组件"),
        TurnBoxRoute(),
        SizedBox(height: 30),
        Text("3. 我们要封装一个富文本展示组件MyRichText ，它可以自动处理url链接 (功能暂未实现)"),
        Text("封装的是StatefulWidget，一定要注意在组件更新时是否需要同步状态"),
        MyRichText(
            text: "月落乌啼霜满天 www.baidu.com",
            linkStyle: TextStyle(color: Colors.green)),
      ],
    );
  }
}

class TestRotatedBox extends StatefulWidget {
  const TestRotatedBox({super.key});

  @override
  State<TestRotatedBox> createState() => _TestRotatedBoxState();
}

class _TestRotatedBoxState extends State<TestRotatedBox> {
  int quarterTurns = 1;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          decoration: BoxDecoration(color: Colors.red),
          height: 80,
          width: 100,
          child: RotatedBox(
            quarterTurns: quarterTurns,
            child: Text("Hello world"),
          ),
        ),
        ElevatedButton(
            onPressed: () {
              setState(() {
                quarterTurns++;
              });
            },
            child: Text("顺时针旋转")),
        ElevatedButton(
            onPressed: () => setState(() {
                  quarterTurns--;
                }),
            child: Text("逆时针旋转")),
      ],
    );
  }
}

class TurnBox extends StatefulWidget {
  const TurnBox({
    this.turns = .0, //旋转的“圈”数,一圈为360度，如0.25圈即90度
    this.speed = 200, //过渡动画执行的总时长
    required this.child,
  });

  final double turns;
  final int speed;
  final Widget child;

  @override
  State createState() => _TurnBoxState();
}

class _TurnBoxState extends State<TurnBox> with SingleTickerProviderStateMixin {
  late AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
        vsync: this, lowerBound: -double.infinity, upperBound: double.infinity);
    _controller.value = widget.turns;
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return RotationTransition(
      turns: _controller,
      child: widget.child,
    );
  }

  @override
  void didUpdateWidget(TurnBox oldWidget) {
    super.didUpdateWidget(oldWidget);
    //旋转角度发生变化时执行过渡动画
    if (oldWidget.turns != widget.turns) {
      _controller.animateTo(
        widget.turns,
        duration: Duration(milliseconds: widget.speed ?? 200),
        curve: Curves.easeOut,
      );
    }
  }
}

class TurnBoxRoute extends StatefulWidget {
  const TurnBoxRoute({Key? key}) : super(key: key);

  @override
  State createState() => _TurnBoxRouteState();
}

class _TurnBoxRouteState extends State<TurnBoxRoute> {
  double _turns = .0;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          TurnBox(
            turns: _turns,
            speed: 500,
            child: const Icon(
              Icons.refresh,
              size: 50,
            ),
          ),
          TurnBox(
            turns: _turns,
            speed: 1000,
            child: const Icon(
              Icons.refresh,
              size: 150.0,
            ),
          ),
          ElevatedButton(
            child: const Text("顺时针旋转1/5圈"),
            onPressed: () {
              setState(() {
                _turns += .2;
              });
            },
          ),
          ElevatedButton(
            child: const Text("逆时针旋转1/5圈"),
            onPressed: () {
              setState(() {
                _turns -= .2;
              });
            },
          )
        ],
      ),
    );
  }
}

class MyRichText extends StatefulWidget {
  MyRichText({
    Key? key,
    required this.text, // 文本字符串
    required this.linkStyle, // url链接样式
  }) : super(key: key);

  final String text;
  final TextStyle linkStyle;

  @override
  State createState() => _MyRichTextState();
}

class _MyRichTextState extends State<MyRichText> {
  late TextSpan _textSpan;

  @override
  Widget build(BuildContext context) {
    return RichText(text: _textSpan);
  }

  TextSpan parseText(String text) {
    // 耗时操作：解析文本字符串，构建出TextSpan。
    // 省略具体实现。
    print("假设parseText非常耗时...");
    return TextSpan(
        text: text, style: TextStyle(color: Colors.red, fontSize: 30));
  }

  @override
  void initState() {
    _textSpan = parseText(widget.text);
    super.initState();
  }

  @override
  void didUpdateWidget(MyRichText oldWidget) {
    if (widget.text != oldWidget.text) {
      _textSpan = parseText(widget.text);
    }
    super.didUpdateWidget(oldWidget);
  }
}

void main(List<String> args) {
  runApp(MaterialApp(
    home: Scaffold(body: Demo()),
  ));
}
