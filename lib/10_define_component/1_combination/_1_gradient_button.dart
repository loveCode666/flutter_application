import 'package:flukit/flukit.dart';
import 'package:flutter/material.dart';

class Demo extends StatelessWidget {
  List<Color> _colors = [Colors.blue, Colors.green];
  BorderRadius borderRadius = BorderRadius.all(Radius.circular(20));

  @override
  Widget build(BuildContext context) {
    return Column(
      // spacing: 16.0,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("a. 背景支持渐变色; b. 手指按下时有涟漪效果; c. 可以支持圆角"),
        SizedBox(height: 30),
        Text("1. 原生按钮效果"),
        ElevatedButton(onPressed: () {}, child: Text("原生ElevatedButton")),
        SizedBox(height: 10),
        ElevatedButton(
          onPressed: () {},
          child: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: _colors,
              ),
            ),
            child: Text("ElevatedButton外部，内部contain颜色渐变"),
          ),
        ),
        SizedBox(height: 10),
        Text("缺乏涟漪效果"),
        Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: _colors,
              ),
              borderRadius: borderRadius,
            ),
            child: TextButton(
              onPressed: () {},
              child: Text("Container外部，child为TextButton"),
            )),
        SizedBox(height: 30),
        Text("2. 自定义按钮效果：DecoratedBox支持背景色渐变和圆角，InkWell在手指按下有涟漪效果"),
        DecoratedBox(
          decoration: BoxDecoration(
            gradient: LinearGradient(colors: [Colors.blue, Colors.green]),
            borderRadius: borderRadius,
          ),
          // InkWell作的是真差劲呀，为啥外层还要套一个Material
          child: Material(
            // 透明，即采用了父级DecoratedBox的decoration
            type: MaterialType.transparency,
            child: InkWell(
              splashColor: _colors.last,
              highlightColor: Colors.transparent,
              borderRadius: borderRadius,
              onTap: () {},
              child: ConstrainedBox(
                constraints: BoxConstraints.tightFor(height: 40, width: 300),
                child: Center(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: DefaultTextStyle(
                      style: const TextStyle(fontWeight: FontWeight.bold),
                      child: Text("Container + InkWell 组合"),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
        SizedBox(height: 10),
        Text("《Flutter实战》作者封装了上面的组件, 需要添加依赖包flukit"),
        GradientButton(
          borderRadius: borderRadius,
          colors: const [Colors.orange, Colors.red],
          child: const Text("Submit"),
          onPressed: () {},
        ),
        GradientButton(
          borderRadius: borderRadius,
          colors: [Colors.lightGreen, Colors.green.shade700],
          child: const Text("Submit"),
          onPressed: () {},
        ),
        GradientButton(
          borderRadius: const BorderRadius.all(Radius.circular(15)),
          colors: [Colors.lightBlue.shade300, Colors.blueAccent],
          child: const Text("Submit"),
          onPressed: () {},
        ),
      ],
    );
  }
}

void main(List<String> args) {
  runApp(MaterialApp(
    home: Scaffold(body: Demo()),
  ));
}
