import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main(List<String> args) {
  runApp(MaterialApp(
    home: LocaleTest(),
  ));
}

class LocaleTest extends StatefulWidget {
  const LocaleTest({super.key});

  @override
  State<LocaleTest> createState() => _LocaleTestState();
}

class _LocaleTestState extends State<LocaleTest> {
  // 最顶层父组件存储locale, jsonMap, changeFunc
  String _locale = 'zh'; // 初始Locale
  Map<String, dynamic> jsonMap = {};

  void _changeLocale() {
    String nextLocale = _locale == 'zh' ? 'en' : 'zh';
    // 异步加载本地化资源
    final String fileName = './jsons/$nextLocale.json';
    rootBundle.loadString(fileName).then((string) {
      final Map<String, dynamic> nextJsonMap = json.decode(string);
      setState(() {
        _locale = nextLocale;
        jsonMap = nextJsonMap;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: HomeDemo(jsonMap, _changeLocale),
    );
  }
}

class HomeDemo extends StatelessWidget {
  HomeDemo(this.jsonMap, this.changeLocale);

  final Map<String, dynamic> jsonMap;
  final void Function() changeLocale;

  // 获取本地化字符串的方法
  String translate(String key) {
    return jsonMap[key] ?? '*** $key not found ***';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(translate('app_title')),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(translate('hello').replaceFirst("%s", "sbjun")),
            SizedBox(height: 30),
            Text("手写版语言切换："),
            Text("1. 国际化处使用变量；"),
            Text("2. locale, jsonMap, changeLocale置于最顶层"),
            Text("3. 最麻烦的是：每个子组件都需要传递参数jsonMap: 解决办法, 使用redux等全局状态管理"),
          ],
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.miniEndTop,
      floatingActionButton: ElevatedButton.icon(
        icon: Icon(Icons.switch_access_shortcut),
        onPressed: changeLocale,
        label: Text(translate('button_label')), // 你可以更换为你需要的图标
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
