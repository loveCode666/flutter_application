import 'package:flutter/material.dart';

class Demo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("1. 数组中，同一个组件，key是必要要的，否则在热更新或者动态操作中，会出现错位 "),
        SizedBox(height: 10),
        Column(
          children: [
            Box(Colors.red),
            Box(Colors.blue),
          ],
        ),
        Text("2. "),
      ],
    );
  }
}

class Box extends StatelessWidget {
  final Color color;
  const Box(this.color, {super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 100,
      height: 100,
      color: color,
      child: Center(child: Text("hi")),
    );
  }
}

void main(List<String> args) {
  runApp(MaterialApp(
    home: Scaffold(body: Demo()),
  ));
}
