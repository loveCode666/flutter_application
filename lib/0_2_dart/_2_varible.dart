// 类似于java & js的结合体
// 1. 变量类型有：int, double, String, bool
// 2. 变量类型可以显示声明，也可以隐式声明

// 2023-10

// 1. 显示申明 & 隐式声明
void test1() {
  String name = 'Mbappe';
  print(name);
  // name = 18;
  List list1 = ['Jupiter', 'Saturn', 'Uranus', 'Neptune'];
  print(list1);

  var age = 18;
  print(age);
  // age = 'Mbappe'; // 不同于js，推断出类型后，是不可以再赋值为不同类型的变量
  var list2 = ['Jupiter', 'Saturn', 'Uranus', 'Neptune'];
  print(list2);
}

// 2. 空安全检查
void test2(String name) {
  print(name.length);
}

void test3(String? name) {
  // 报错 The non-nullable local variable 'name' must be assigned before it can be used
  // print(name.length);
  print("test3: ${name?.length}");
}

void main() {
  // test1();

  // test2("Neymar");
  test3(null);
}
