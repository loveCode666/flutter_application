// []这种可选参数不常见吧
void getInfo(String name, [String country = "china", String? align]) {
  print("name: $name");
  print("country: $country");
  print("align: $align");
}

// 命名参数{}，也可以可选
void getInfo2(String name, {required int age, String? align}) {
  print("$name, $age, $align");
}

void main() {
  getInfo("shujun", "china");
  getInfo2("sj", age: 23, align: "sbjun");
}
