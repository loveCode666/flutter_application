Future login(String userName, String password) {
  return Future.delayed(Duration(seconds: 3), () => "101");
}

Future<String> getUserInfo(String id) {
  return Future(() => "sbjun");
  // return Future.delayed(Duration(seconds: 2), () => "sbjun");
}

saveUserInfo(String useInfo) {
  return Future.delayed(Duration(seconds: 5), () => true);
}

task() async {
  try {
    String id = await login("alice", "******");
    print("id: $id");
    String userInfo = await getUserInfo(id);
    print("userInfo: $userInfo");
    await saveUserInfo(userInfo);
    print("success");
    //执行接下来的操作
  } catch (e) {
    //错误处理
    print(e);
  }
}

void main(List<String> args) {
  task();
}
