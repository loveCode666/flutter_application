import 'dart:math';

/// Future太类似promise了, 也是then式调用链
void futureThen() {
  Future.delayed(Duration(seconds: 2), () {
    return "hi world!";
    // throw AssertionError("Error");
  }).then((data) {
    //执行成功会走到这里
    print("success");
  }).catchError((e) {
    //执行失败会走到这里
    print(e);
  });
}

futureCatch() {
  Future.delayed(Duration(seconds: 2), () {
    //return "hi world!";
    throw AssertionError("Error");
  }).then((data) {
    //执行成功会走到这里
    print("success");
  }).catchError((e) {
    //执行失败会走到这里
    print("error: $e");
  });
}

// 类似finally
futureComplete() {
  Future.delayed(Duration(seconds: 2), () {
    //return "hi world!";
    throw AssertionError("Error");
  }).then((data) {
    //执行成功会走到这里
    print(data);
  }).catchError((e) {
    //执行失败会走到这里
    print(e);
  }).whenComplete(() {
    //无论成功或失败都会走到这里
    print("complete");
  });
}

// 类似Promise.all
futureWait() {
  Future.wait([
    // 2秒后返回结果
    Future.delayed(Duration(seconds: 2), () {
      return "hello";
    }),
    // 4秒后返回结果
    Future.delayed(Duration(seconds: 4), () {
      return " world";
    })
  ]).then((results) {
    print(results[0] + results[1]);
  }).catchError((e) {
    print(e);
  });
}

void main(List<String> args) {
  // futureThen();
  // futureCatch();
  // futureComplete();
  futureWait();
}
