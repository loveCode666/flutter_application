void main(List<String> args) {
  // 1. identical判断内存地址
  var a = 1;
  var b = 1;
  print(identical(a, b)); // true
  b = 2;
  print(identical(a, b)); // false
  var d = 'hello';
  var e = 'hello';
  print(identical(d, e)); // true
  var f = 'hello world';
  var g = 'hello world';
  print(identical(f, g)); // true

  var o1 = Object();
  var o2 = Object();
  print(identical(o1, o2)); // false

  // const还能修饰对象
  var o3 = const Object();
  var o4 = const Object();
  print(identical(o3, o4)); // true

  var o5 = const [1];
  var o6 = const [2];
  print(identical(o5, o6)); // false, 需要const一样的对象，才能地址一样

  Person p1 = Person('张三', 20);
  Person p2 = Person('张三', 20);
  print("p1==p2: ${identical(p1, p2)}"); // false

  Person1 p3 = const Person1('张三', 20);
  Person1 p4 = const Person1('张三', 20);
  print("p3==p4: ${identical(p3, p4)}"); // true
}

class Person {
  String name;
  int age;
  Person(this.name, this.age);
}

class Person1 {
  final String name;
  final int age;
  const Person1(this.name, this.age);
}
