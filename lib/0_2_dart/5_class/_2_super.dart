class Person {
  String name;
  int age;
  Person(this.name, this.age);
  void printInfo() {
    print('Name: $name, Age: $age');
  }
}

class Student extends Person {
  String school;
  Student(String name, int age, this.school) : super(name, age);
  void printStudentInfo() {
    print('Name: $name, Age: $age, School: $school');
  }
}

void main() {
  Student student = Student('Alice', 18, 'ABC School');
  student.printInfo(); // 输出：Name: Alice, Age: 18
  student.printStudentInfo(); // 输出：Name: Alice, Age: 18, School: ABC School
}
