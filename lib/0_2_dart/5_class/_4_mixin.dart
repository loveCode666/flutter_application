/*
在dart中使用mixins实现类似多继承的功能

mixins是一种特殊的类，可以被混入到其他类中，以扩展其他类的功能。
使用mixins时，需要满足以下条件：
a. 混入的类不能有构造函数。
b. 混入的类不能继承其他类。
*/

class A0 {
  String name = "A0";
  printA() {
    print("A0 $name");
  }
}

class B0 {
  String name = "B0";
  printB() {
    print("B0 $name");
  }
}

// 不可extends多继承
// class C extends A0, B0 {
//   // 实现多个接口
// }

mixin C0 {
  String name = "C0";
  printC() {
    print("C0 $name");
  }
}

class M extends A0 with C0 {
  printM() {
    print("M $name");
  }
}

void main() {
  // 1. minxin基本用法: 如果字段方法重复，采用最后那个
  M m = M();
  m.printA();
  m.printC();
  m.printM();
}
