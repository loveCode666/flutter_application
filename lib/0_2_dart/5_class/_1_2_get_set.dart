class Rect {
  num width;
  num height;

  // 构造函数
  Rect(this.width, this.height);

  // 计算面积的方法
  num area() => width * height;

  num get area2 => width * height; // 自动生成getter方法

  set area2(num value) {
    // 自动生成setter方法
    print('设置面积为 $value');
    // 执行其他操作
    width = value / height;
  }
}

void main(List<String> args) {
  Rect rect = Rect(10, 20);
  print(rect.area()); // 输出200
  print(rect.area2); // 输出200
}
