// https://dart.cn/language/constructors
// 2023-10-15

// 常规写法
class Point0 {
  // Non-nullable instance field 'x' must be initialized.
  // double x;

  late double x;
  late double y;

  Point0(double x, double y) {
    this.x = x;
    this.y = y;
  }
}

class Point1 {
  double x;
  double y;

  Point1(this.x, this.y);
}

class Point2 {
  final double x;
  final double y;

  Point2(this.x, this.y);

  // Named constructor  冒号赋值
  Point2.origin(double xOrigin, double yOrigin)
      : x = xOrigin,
        y = yOrigin;
}

class Point3 {
  late final double x;
  late final double y;
  // 不写的画，默认有个构造函数，无参而已，后面再赋值呗
}

void main(List<String> args) {
  // 1. 朴素写法
  // Point0 p0 = Point0(1.0, 2.0);
  // print('p0: (${p0.x}, ${p0.y})');

  // 2. dart推荐构造函数写法
  // Point1 p1 = Point1(1.0, 2.0);
  // print('p1: (${p1.x}, ${p1.y})');

  // 3. 命名式赋值
  // Point2 p2 = Point2(1.0, 2.0);
  // print('p2: (${p2.x}, ${p2.y})');
  // Point2 p2_2 = Point2.origin(3, 4);
  // print('p2_2: (${p2_2.x}, ${p2_2.y})');

  // Point3 p3 = Point3();
  // print('p3: $p3');
  // p3.x = 1.0;
  // print('p3: ${p3.x}');
}
