/*
  dart中的对象操作符
  ？ 条件运算符
  as 类型转换
  is 类型判断
  .. 级联操作符

*/

class Person {
  String name;
  int age;
  Person(this.name, this.age);
  void printInfo() {
    print('$name----$age');
  }
}

main() {
  var p1 = Person('张三', 20)
    ..name = '李四'
    ..age = 30;
  p1.printInfo();
}
