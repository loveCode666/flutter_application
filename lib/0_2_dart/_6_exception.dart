void main() {
  try {
    print("try");
    // throw Exception('Error');
    throw FormatException('Error');
  } on FormatException {
    print('Format Error');
  } on Exception {
    print('Exception');
  } catch (e) {
    print(e);
  } finally {
    print('Finally');
  }
}
