// 集合
// 2024-7-12

void list() {
  List players = ['Mbappe', 'CR7', 'Neymar', 'Salah'];
  print(players.first);
  String name = players.firstWhere((element) => element.startsWith('N'));
  print("find name: $name");
  // 找不到就抛出异常了：Bad state: No element
  // String name1 = players.firstWhere((element) => element.startsWith('A'));
  // print("find name: $name1");

  String name1 = players.firstWhere((element) => element.startsWith('A'),
      orElse: () => "Alvin");
  print("find name: $name1");
}

void map() {
  var person = {
    'name': 'Alvin',
    'age': 20,
  };
  // map还能这么方便的迭代？ 牛
  person.forEach((key, value) {
    print("key: $key, value: $value");
  });
}

void set() {
  Set<int> numbers = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
  print(numbers.first);
  print(numbers.last);
}

void main(List<String> args) {
  // 1. List
  // list();

  // 2. map
  // map();

  // 3. Set
  set();
}
