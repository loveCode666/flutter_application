import 'package:flutter/material.dart';
import 'package:namer_app/8_event_and_notify/_4_event_bus_main.dart';

import '_4_event_bus.dart';

class DetailPage extends StatelessWidget {
  const DetailPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("New route"),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text("This is new route"),
          ElevatedButton(
            onPressed: () {
              eventBusInstance.emit("login", "sbjun");
            },
            child: Text("add counter"),
          ),
          Row(),
        ],
      ),
    );
  }
}
