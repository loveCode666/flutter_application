import 'package:flutter/material.dart';

import '_4_detail.dart';
import '_4_event_bus.dart';

class Demo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("这是A页面, 点击按钮进入B页面"),
        SizedBox(height: 10),
        Counter(),
        ElevatedButton(
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) {
                return DetailPage();
              }),
            );
          },
          child: Text("Go to new page"),
        ),
        SizedBox(height: 30),
        Text("2. 所有的监听其实都可以理解为回调注册，发送或者生产消息过程就是一个主动回调。"),
      ],
    );
  }
}

class Counter extends StatefulWidget {
  const Counter({super.key});

  @override
  State<Counter> createState() => _CounterState();
}

class _CounterState extends State<Counter> {
  int _counter = 0;

  @override
  void initState() {
    var bus = EventBus();

    bus.on("login", (arg) {
      print("arg $arg");
      setState(() => _counter++);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Text("counter: $_counter");
  }
}

void main(List<String> args) {
  runApp(MaterialApp(
    home: Scaffold(body: Demo()),
  ));
}
