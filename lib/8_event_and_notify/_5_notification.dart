import 'package:flutter/material.dart';

class Demo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // return NotificationListener(
    //指定监听通知的类型为滚动结束通知(ScrollEndNotification)
    return NotificationListener<ScrollEndNotification>(
      onNotification: (notification) {
        switch (notification.runtimeType) {
          case ScrollStartNotification:
            print("开始滚动");
            break;
          case ScrollUpdateNotification:
            print("正在滚动");
            break;
          case ScrollEndNotification:
            print("滚动停止");
            break;
          case OverscrollNotification:
            print("滚动到边界");
            break;
        }
        //true: 阻止冒泡，其父级Widget将再也收不到该通知；
        //false: 继续向上冒泡通知。
        return true;
      },
      child: ListView.builder(
          itemCount: 100,
          itemBuilder: (context, index) {
            return ListTile(
              title: Text("$index"),
            );
          }),
    );
  }
}

class NotificationRoute extends StatefulWidget {
  @override
  NotificationRouteState createState() {
    return NotificationRouteState();
  }
}

class NotificationRouteState extends State<NotificationRoute> {
  String _msg = "";

  Widget sendButton() {
    // 这么写木有用，这个context是根Context，而NotificationListener是监听的子树
//         return  ElevatedButton(
//           onPressed: () => MyNotification("Hi").dispatch(context),
//           child: Text("Send Notification"),
//          ),

    return Builder(
      builder: (context) {
        return ElevatedButton(
          // 2.2 按钮点击时分发通知, dispatch来自Notification
          onPressed: () => MyNotification("Hi").dispatch(context),
          child: Text("Send Notification"),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    //监听通知
    return NotificationListener<MyNotification>(
      onNotification: (notification) {
        setState(() {
          _msg += "${notification.msg}  ";
        });
        return true;
      },
      child: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text("1. 自定义通知，得Builder包裹的context才是子树的，才可以，否则context是root的"),
            sendButton(),
            Text("2. 能够阻止冒泡"),
            // 阻止冒泡原理
            NotificationListener<MyNotification>(
              onNotification: (notification) {
                print("监听到了: ${notification.msg}");
                return false;
                // true则阻止冒泡
                // return true;
              },
              child: sendButton(),
            ),
            SizedBox(height: 30),
            Text(_msg)
          ],
        ),
      ),
    );
  }
}

// 2.1 定义一个通知类，要继承自Notification类；
class MyNotification extends Notification {
  MyNotification(this.msg);
  final String msg;
}

void main(List<String> args) {
  runApp(MaterialApp(
    home: Scaffold(
      // 1. 事件监听
      // body: Demo(),
      // 2. 自定义通知
      body: NotificationRoute(),
    ),
  ));
}
