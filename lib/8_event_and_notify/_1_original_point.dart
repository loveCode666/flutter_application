import 'package:flutter/material.dart';
import 'package:namer_app/8_event_and_notify/source/_1_listener.dart';

class PointerMoveIndicator extends StatefulWidget {
  const PointerMoveIndicator({super.key});

  @override
  State<PointerMoveIndicator> createState() => _PointerMoveIndicatorState();
}

class _PointerMoveIndicatorState extends State<PointerMoveIndicator> {
  PointerEvent? _event;

  @override
  Widget build(BuildContext context) {
    return MyListener(
      child: Container(
        alignment: Alignment.center,
        color: Colors.blue,
        width: 300.0,
        height: 150.0,
        child: Text(
          '${_event?.localPosition ?? ''}',
          style: TextStyle(color: Colors.white),
        ),
      ),
      onPointerDown: (PointerDownEvent event) => setState(() => _event = event),
      onPointerMove: (PointerMoveEvent event) => setState(() => _event = event),
      onPointerUp: (PointerUpEvent event) => setState(() => _event = event),
    );
  }
}

class Demo extends StatelessWidget {
  Widget listenPointDown(String title) {
    return MyListener(
      child: Container(
        color: Colors.red,
        width: 200.0,
        height: 100.0,
        child: Text(title),
      ),
      onPointerDown: (event) => print("in"),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("1. 原始指针事件处理"),
        PointerMoveIndicator(),
        SizedBox(height: 10),
        Text("2. IgnorePointer和AbsorbPointer都能阻止子树接收指针事件"),
        Text(
            "不同之处: AbsorbPointer参与命中测试(没有in, 但是有up)，而IgnorePointer不会(in, out都没有)"),
        Listener(
          child: AbsorbPointer(
            child: listenPointDown("AbsorbPointer"),
          ),
          onPointerDown: (event) => print("up"),
        ),
        SizedBox(height: 10),
        Listener(
          child: IgnorePointer(
            child: listenPointDown("IgnorePointer"),
          ),
          onPointerDown: (event) => print("up"),
        ),
        SizedBox(height: 50),
        Text("总结: Listener包裹，继承的是RenderProxyBoxWithHitTestBehavior"),
      ],
    );
  }
}

void main(List<String> args) {
  runApp(MaterialApp(
    home: Scaffold(body: Demo()),
  ));
}
