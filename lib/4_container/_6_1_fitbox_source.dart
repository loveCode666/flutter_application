import 'package:flutter/material.dart';

import 'source/_6_my_fitbox.dart';

/// 2023-4-8
class Demo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text('1. BoxFit.contain: 子组件缩小，适配父组件.'),
          wContainer(BoxFit.contain),
          SizedBox(height: 10),
          Text("2. 示例：FittedBox来做自适应："),
          SizedBox(height: 10),
          Text("长度太长了会溢出的"),
          wRow(' 9000000000000000000000000 '),
          Text("FitttdBox默认缩小 fit = BoxFit.contain"),
          MyFittedBox(child: wRow(' 9000000000000000000000000 ')),
          SizedBox(height: 50),
          Text("总结: 源码看不懂... 怎么实现的自适应呢?"),
          Text(
              " MyFittedBox extends SingleChildRenderObjectWidget, MyRenderFittedBox中有个paint方法画图..."),
        ],
      ),
    );
  }
}

Widget wContainer(BoxFit boxFit) {
  return Container(
    width: 50,
    height: 50,
    color: Colors.red,
    child: MyFittedBox(
      fit: boxFit,
      // 子容器超过父容器大小
      child: Container(width: 60, height: 70, color: Colors.blue),
    ),
  );
}

Widget wRow(String text) {
  Widget child = Text(text);
  child = Row(
    // 一定需要父组件传递限制下来，默认传下来的父组件宽度是屏幕宽度
    // 如果用FitedBox包裹，传递下来的maxWidth就是double.infinity, 此时就无法计算啦，so spaceEvenly会失效
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: [child, child, child],
  );
  return child;
}

void main(List<String> args) {
  runApp(MaterialApp(
    home: Scaffold(body: Demo()),
  ));
}
