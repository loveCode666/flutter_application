import 'package:flutter/material.dart';

import 'source/_5_my_clip.dart';

class Demo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget avatar = Image.asset("images/hanli.jpeg", width: 150);

    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("原图: "),
          avatar,
          SizedBox(height: 10),
          Text("Clip能裁剪出各种形状:"),
          Text("1. ClipOval：子组件为正方形时裁剪成圆形, 子组件原图为矩形时, 取最大边，裁剪成椭圆"),
          MyClipOval(child: avatar),
          SizedBox(height: 10),
          Text("2. ClipRRect圆角矩形"),
          MyClipRRect(
            borderRadius: BorderRadius.circular(50.0),
            child: avatar,
          ),
          SizedBox(height: 50),
          Text("4. 自定义裁剪"),
          Text("自定义CustomClipper<Rect>, 适用于ClipRect, MyClipOval "),
          DecoratedBox(
            decoration: BoxDecoration(color: Colors.red),
            child: MyClipOval(
              clipper: MyClipperRect(), //使用自定义的clipper
              child: avatar,
            ),
          ),
          Text("自定义CustomClipper<RRect>, 适用于ClipRRect "),
          DecoratedBox(
            decoration: BoxDecoration(color: Colors.red),
            child: MyClipRRect(
              clipper: MyClipperRRect(), //使用自定义的clipper
              child: avatar,
            ),
          ),
          SizedBox(height: 50),
          Text("总结：MyRenderClipRRect extends "),
          Text(
              "MyClipOval extends SingleChildRenderObjectWidget, 核心是：MyRenderClipRRect createRenderObject() "),
          Text("MyRenderClipRRectd的核心是void paint() {context.paintChild();}"),
        ],
      ),
    );
  }
}

//
class MyClipperRect extends CustomClipper<Rect> {
  @override
  Rect getClip(Size size) => Rect.fromLTWH(10.0, 15.0, 100.0, 100.0);

  @override
  bool shouldReclip(CustomClipper<Rect> oldClipper) => false;
}

class MyClipperRRect extends CustomClipper<RRect> {
  @override
  RRect getClip(Size size) =>
      RRect.fromLTRBR(10.0, 15.0, 100.0, 100.0, Radius.circular(20));

  @override
  bool shouldReclip(CustomClipper<RRect> oldClipper) => false;
}

void main(List<String> args) {
  runApp(MaterialApp(
    home: Scaffold(body: Demo()),
  ));
}
