import 'package:flutter/material.dart';

import 'source/_1_my_padding.dart';

class Demo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("1. Padding初体验"),
        Text("EdgeInsets extends EdgeInsetsGeometry"),
        Padding(
          //左边添加8像素补白
          padding: EdgeInsets.only(left: 8),
          child: Text("Hello world"),
        ),
        Padding(
          //上下各添加8像素补白
          padding: EdgeInsets.symmetric(vertical: 8),
          child: Text("I am Jack"),
        ),
        Padding(
          // 分别指定四个方向的补白
          padding: EdgeInsets.fromLTRB(20, 0, 20, 20),
          child: Text("Your friend"),
        ),
        SizedBox(height: 10),
        Text("2. Container也可以"),
        Container(
          //左边添加8像素补白
          padding: EdgeInsets.only(left: 8),
          child: Text("Hello world"),
        ),
        SizedBox(height: 50),
        Text("3. 手写源码调用"),
        Text(
            "extends SingleChildRenderObjectWidget, 核心方法是MyRenderPadding createRenderObject, 里面performLayout可能是绘图"),
        MyPadding(
          padding: EdgeInsets.all(8),
          child: Text("四周都是8"),
        ),
      ],
    );
  }
}

void main(List<String> args) {
  runApp(MaterialApp(
    home: Scaffold(body: Demo()),
  ));
}
