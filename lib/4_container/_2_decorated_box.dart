import 'package:flutter/material.dart';

import 'source/_2_my_decorated_box.dart';

class Demo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("1. DecoratedBox基本就一个属性能用：decoration"),
        DecoratedBox(
          decoration: BoxDecoration(color: Colors.red),
          // position: DecorationPosition.foreground,
          child: Text("Login"),
        ),
        SizedBox(height: 10),
        Text("2. 重要属性: BoxDecoration"),
        DecoratedBox(
          decoration: BoxDecoration(
            gradient: LinearGradient(
                colors: [Colors.red, Colors.orange.shade700]), //背景渐变
            borderRadius: BorderRadius.circular(8.0),
            // shape: BoxShape.circle,
            boxShadow: [
              //阴影
              BoxShadow(
                  color: Colors.black54,
                  offset: Offset(2.0, 2.0),
                  blurRadius: 4.0)
            ],
          ),
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 80.0, vertical: 18.0),
            child: Text(
              "Login",
              style: TextStyle(color: Colors.white),
            ),
          ),
        ),
        SizedBox(height: 10),
        Text("神奇：能画出一个🌙"),
        DecoratedBox(
          decoration: BoxDecoration(
            gradient: RadialGradient(
              center: Alignment(-0.5, -0.6),
              radius: 0.15,
              colors: <Color>[
                Color(0xFFEEEEEE),
                Color(0xFF111133),
              ],
              stops: <double>[0.9, 1.0],
            ),
          ),
          child: SizedBox(width: 200, height: 200),
        ),
        SizedBox(height: 50),
        Text("3. 源码"),
        Text(
            " MyDecoratedBox extends SingleChildRenderObjectWidget, 核心MyRenderDecoratedBox: _painter!.paint(context.canvas, offset, filledConfiguration);"),
        MyDecoratedBox(
          decoration: BoxDecoration(
            color: Colors.red,
          ),
          child: Padding(padding: EdgeInsets.all(30), child: Text("Login")),
        )
      ],
    );
  }
}

void main(List<String> args) {
  runApp(MaterialApp(
    home: Scaffold(body: Demo()),
  ));
}
