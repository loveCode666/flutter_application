import 'package:flutter/material.dart';

class Demo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget avatar = Image.asset("images/hanli.jpeg", width: 150);
    Widget containerCycle = Container(
      width: 100,
      height: 100,
      alignment: Alignment.center,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: Colors.red,
      ),
      child: avatar,
    );

    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("原图: "),
          avatar,
          SizedBox(height: 10),
          Text("Clip能裁剪出各种形状:"),
          Text("1. ClipOval：子组件为正方形时裁剪成圆形, 子组件原图为矩形时, 取最大边，裁剪成椭圆"),
          ClipOval(child: avatar),
          Text('1.1 Container也能圆角, 但是它只弄了背景，没有改变图片本身'),
          containerCycle,
          SizedBox(height: 10),
          Text("2. ClipRRect圆角矩形"),
          ClipRRect(
            borderRadius: BorderRadius.circular(50.0),
            child: avatar,
          ),
          Text("3. ClipRec"),
          Text(
              "3.1 Align将宽度设为原来宽度一半，另一半会溢出 & 还是可见，但是不挤压占用空间，奇葩... 所以文字就显示在上面了"),
          Row(
            children: <Widget>[
              SizedBox(width: 50),
              Align(
                // Align将宽度设为原来宽度一半, 以中间为竖线，两侧各 1/4 其实是没有了的
                widthFactor: 0.5,
                child: avatar,
              ),
              Text("你好世界", style: TextStyle(color: Colors.green))
            ],
          ),
          Text("3.2 ClipRect有点难理解呀：剪裁掉子组件布局空间之外的绘制内容（溢出部分剪裁）"),
          Row(
            children: <Widget>[
              SizedBox(width: 50),
              // 补上ClipRect, 相当于去掉了左右两侧的裁剪部分
              ClipRect(
                child: Align(
                  widthFactor: 0.5,
                  child: avatar,
                ),
              ),
              Text("你好世界", style: TextStyle(color: Colors.green))
            ],
          ),
          Text("3.3 应用ClipRect对上面的container进行修饰；"),
          Text("没成功, 因为containerCycle就没有溢出部分"),
          ClipRect(child: containerCycle),
          SizedBox(height: 10),
          Text("左右两侧有溢出部分，就能成功了"),
          ClipRect(child: Align(widthFactor: 0.5, child: containerCycle)),
          SizedBox(height: 10),
          Text("FittedBox也能模拟溢出部分, 虽然暂时不知道FittedBox的作用"),
          Row(
            children: [
              ClipRect(
                // 将超出子组件布局范围的绘制内容剪裁掉
                child: Container(
                  width: 50,
                  height: 50,
                  color: Colors.red,
                  child: FittedBox(
                    fit: BoxFit.contain,
                    child: avatar,
                  ),
                ),
              ),
              Text("你好世界", style: TextStyle(color: Colors.green)),
            ],
          ),
          SizedBox(height: 50),
          Text("4. 自定义裁剪"),
          Text("自定义CustomClipper<Rect>, 适用于ClipRect, ClipOval "),
          DecoratedBox(
            decoration: BoxDecoration(color: Colors.red),
            child: ClipRect(
              clipper: MyClipperRect(), //使用自定义的clipper
              child: avatar,
            ),
          ),
          SizedBox(height: 10),
          DecoratedBox(
            decoration: BoxDecoration(color: Colors.red),
            child: ClipOval(
              clipper: MyClipperRect(), //使用自定义的clipper
              child: avatar,
            ),
          ),
          SizedBox(height: 10),
          Text("自定义CustomClipper<RRect>, 适用于ClipRRect "),
          DecoratedBox(
            decoration: BoxDecoration(color: Colors.red),
            child: ClipRRect(
              clipper: MyClipperRRect(), //使用自定义的clipper
              child: avatar,
            ),
          ),
          SizedBox(height: 10),
          Text("ClipPath", style: TextStyle(color: Colors.red)),
          DecoratedBox(
            decoration: BoxDecoration(color: Colors.red),
            child: ClipPath(
              clipper: MyClipperPath(), //使用自定义的clipper
              child: avatar,
            ),
          ),
          SizedBox(height: 50),
          Text("总结：最主要的还是ClipOval来裁剪圆形，ClipRRect来裁剪圆角")
        ],
      ),
    );
  }
}

//
class MyClipperRect extends CustomClipper<Rect> {
  @override
  Rect getClip(Size size) => Rect.fromLTWH(10.0, 15.0, 100.0, 100.0);

  @override
  bool shouldReclip(CustomClipper<Rect> oldClipper) => false;
}

class MyClipperRRect extends CustomClipper<RRect> {
  @override
  RRect getClip(Size size) =>
      RRect.fromLTRBR(10.0, 15.0, 100.0, 100.0, Radius.circular(20));

  @override
  bool shouldReclip(CustomClipper<RRect> oldClipper) => false;
}

class MyClipperPath extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();
    path.moveTo(0, size.height * 0.2);
    path.lineTo(size.width, size.height * 0.2);
    path.lineTo(size.width * 0.75, size.height * 0.5);
    path.lineTo(size.width * 0.25, size.height * 0.5);
    path.lineTo(size.width * 0.5, size.height * 0.8);
    path.lineTo(size.width * 0.5, size.height);
    path.lineTo(0, size.height);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}

void main(List<String> args) {
  runApp(MaterialApp(
    home: Scaffold(body: Demo()),
  ));
}
