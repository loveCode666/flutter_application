容器类Widget和布局类Widget：

- 布局类Widget需要接收一个widget数组（children），他们直接或间接继承自（或包含）MultiChildRenderObjectWidget; 按照一定的排列方式来对其子Widget进行排列；

- 容器类Widget只需要接收一个子Widget（child），他们直接或间接继承自（或包含）SingleChildRenderObjectWidget。只是包装其子Widget，对其添加一些修饰（补白或背景色等）、变换(旋转或剪裁等)、或限制(大小等)。

这只是《flutter实战》作者的自己分类而已。。。


