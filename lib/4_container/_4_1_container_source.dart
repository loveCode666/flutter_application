import 'package:flutter/material.dart';

import 'source/_4_my_container.dart';

class Demo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("调用源码, Container其实就是个嵌套组合", style: TextStyle(color: Colors.red)),
          Text("1. Padding"),
          DecoratedBox(
            decoration: BoxDecoration(color: Colors.yellow.shade100),
            child: MyContainer(
              padding: EdgeInsets.all(10),
              child: Text("Container组件"),
            ),
          ),
          SizedBox(height: 30),
          Text("2. DecoratedBox"),
          MyContainer(
            // color 和 decoration互斥
            // color: Colors.red.shade300,
            decoration: BoxDecoration(color: Colors.green.shade300),
            child: Text("Container同样也能装饰样式"),
          ),
          SizedBox(height: 30),
          Text("3. Transform：右移20像素，向上平移10像素"),
          SizedBox(height: 10),
          DecoratedBox(
            decoration: BoxDecoration(color: Colors.red),
            child: MyContainer(
              transform: Matrix4.translationValues(20, -10, 0.0),
              child: Text("Containe也能"),
            ),
          ),
          SizedBox(height: 30),
          Text("4. Align: 文本默认在左上角，现在偏移到右下角"),
          MyContainer(
            height: 50.0,
            width: 120.0,
            color: Colors.blue.shade50,
            child: MyContainer(
              alignment: Alignment.bottomRight,
              child: Text("MyContainer"),
            ),
          ),
          SizedBox(height: 30),
          Text("5. ConstrainedBox"),
          MyContainer(
            // 说好的width、height优先呢？咋是constraints优先了？
            width: 300,
            height: 200,
            // constraints: BoxConstraints(maxWidth: 100, maxHeight: 50),
            constraints: BoxConstraints.tightFor(width: 100, height: 50),
            child: MyContainer(
              color: Colors.yellow.shade400,
              alignment: Alignment.center,
              width: 400,
              height: 200,
              child: Text("MyContainer"),
            ),
          ),
          SizedBox(height: 30),
          Text("总结："),
          Text("0. 没有child & constraints, 就来一个LimitedBox"),
          Text("1. 我的天呐，太强了，9个嵌套: "),
          Text(
              "    Align < Padding < ColoredBox < ClipPath    <     DecoratedBox < DecoratedBox(decoration: foregroundDecoration!) < ConstrainedBox < margin(Padding) < Transform"),
        ],
      ),
    );
  }
}

void main(List<String> args) {
  runApp(MaterialApp(
    home: Scaffold(body: Demo()),
  ));
}
