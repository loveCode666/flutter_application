import 'package:flutter/material.dart';

class Demo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("大杂烩Container, 实际上Padding & ", style: TextStyle(fontSize: 20)),
          Text("1. Padding"),
          DecoratedBox(
            decoration: BoxDecoration(color: Colors.yellow.shade100),
            child: Padding(
              padding: EdgeInsets.all(10),
              child: Text("Padding组件"),
            ),
          ),
          SizedBox(height: 10),
          DecoratedBox(
            decoration: BoxDecoration(color: Colors.yellow.shade100),
            child: Container(
              padding: EdgeInsets.all(10),
              child: Text("Container组件"),
            ),
          ),
          SizedBox(height: 30),
          Text("2. DecoratedBox"),
          DecoratedBox(
            decoration: BoxDecoration(color: Colors.green.shade300),
            child: Text("decaratedbox能装饰样式"),
          ),
          SizedBox(height: 10),
          Container(
            // color 和 decoration互斥
            // color: Colors.red.shade300,
            decoration: BoxDecoration(color: Colors.green.shade300),
            child: Text("Container同样也能装饰样式"),
          ),
          SizedBox(height: 30),
          Text("3. Transform：右移20像素，向上平移10像素"),
          SizedBox(height: 10),
          DecoratedBox(
            decoration: BoxDecoration(color: Colors.red),
            child: Transform(
              // x, y, z
              transform: Matrix4.translationValues(20, -10, 0.0),
              child: Text("Transform"),
            ),
          ),
          SizedBox(height: 10),
          DecoratedBox(
            decoration: BoxDecoration(color: Colors.red),
            child: Container(
              transform: Matrix4.translationValues(20, -10, 0.0),
              child: Text("Containe也能"),
            ),
          ),
          SizedBox(height: 30),
          Text("4. Align: 文本默认在左上角，现在偏移到右下角"),
          Container(
            height: 50.0,
            width: 120.0,
            color: Colors.blue.shade50,
            child: Align(
              alignment: Alignment.bottomRight,
              child: Text("Align"),
            ),
          ),
          SizedBox(height: 10),
          Container(
            height: 50.0,
            width: 120.0,
            color: Colors.blue.shade50,
            child: Container(
              alignment: Alignment.bottomRight,
              child: Text("Container"),
            ),
          ),
          SizedBox(height: 30),
          Text("5. ConstrainedBox"),
          ConstrainedBox(
            constraints: BoxConstraints(maxWidth: 100, maxHeight: 50),
            child: Container(
              color: Colors.yellow.shade400,
              alignment: Alignment.center,
              width: 400,
              height: 200,
              child: Text("ConstrainedBox"),
            ),
          ),
          SizedBox(height: 10),
          // 替换成Container
          Container(
            // 说好的width、height优先呢？咋是constraints优先了？
            width: 300,
            height: 200,
            // constraints: BoxConstraints(maxWidth: 100, maxHeight: 50),
            constraints: BoxConstraints.tightFor(width: 100, height: 50),
            child: Container(
              color: Colors.yellow.shade400,
              alignment: Alignment.center,
              width: 400,
              height: 200,
              child: Text("Container"),
            ),
          ),

          SizedBox(height: 30),
          Text("6. 炫酷实例："),
          Container(
            margin: EdgeInsets.only(top: 50.0, left: 120.0),
            constraints:
                BoxConstraints.tightFor(width: 200.0, height: 150.0), //卡片大小
            decoration: BoxDecoration(
              //背景装饰
              gradient: RadialGradient(
                //背景径向渐变
                colors: [Colors.red, Colors.orange],
                center: Alignment.topLeft,
                radius: .98,
              ),
              boxShadow: [
                //卡片阴影
                BoxShadow(
                  color: Colors.black54,
                  offset: Offset(2.0, 2.0),
                  blurRadius: 4.0,
                )
              ],
            ),
            transform: Matrix4.rotationZ(.2), //卡片倾斜变换
            alignment: Alignment.center, //卡片内文字居中
            child: Text(
              //卡片文字
              "5.20", style: TextStyle(color: Colors.white, fontSize: 40.0),
            ),
          ),
          SizedBox(height: 200),
        ],
      ),
    );
  }
}

void main(List<String> args) {
  runApp(MaterialApp(
    home: Scaffold(body: Demo()),
  ));
}
