import 'package:flutter/material.dart';
import 'dart:math' as math;

import 'source/_3_my_transform.dart';

class Demo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("1. 炫酷的Transform可以实现一些变形，构造函数一定要传参transform"),
          Container(
            margin: EdgeInsets.only(top: 60, left: 10),
            color: Colors.black,
            child: Transform(
              alignment: Alignment.topRight, //相对于坐标系原点的对齐方式
              // Matrix4是一个4D矩阵
              transform: Matrix4.skewY(0.3), //沿Y轴倾斜0.3弧度
              child: Container(
                padding: const EdgeInsets.all(8.0),
                color: Colors.deepOrange,
                child: const Text('Apartment for rent!'),
              ),
            ),
          ),
          SizedBox(height: 50),
          Text("2. transform很难写呀，所以Trasform有很多静态构造函数可以用: 平移，旋转，缩放..."),
          Text("2.1 Transform.translated搭配offset可以实现平移"),
          Text(
              "transform默认Matrix4.translationValues(offset.dx, offset.dy, 0.0)"),
          Container(
            margin: EdgeInsets.only(top: 60, left: 10),
            color: Colors.black,
            child: Transform.translate(
              offset: Offset(20.0, -5.0), //沿Y轴倾斜0.3弧度
              child: Container(
                padding: const EdgeInsets.all(8.0),
                color: Colors.deepOrange,
                child: const Text('Apartment for rent!'),
              ),
            ),
          ),
          SizedBox(height: 10),
          DecoratedBox(
            decoration: BoxDecoration(color: Colors.red),
            //默认原点为左上角，右移20像素，向上平移5像素
            child: Transform.translate(
              offset: Offset(40.0, -5.0),
              child: Text("Hello world"),
            ),
          ),
          SizedBox(height: 30),
          Text("2.2 Transform.rotate可以实现旋转，作用在绘制阶段："),
          DecoratedBox(
            decoration: BoxDecoration(color: Colors.red),
            child: Transform.rotate(
              //旋转90度
              angle: math.pi / 2,
              child: Text("Hello world"),
            ),
          ),
          SizedBox(height: 30),
          Text("2.3 Transform.scale可以对子组件进行缩小或放大:"),
          DecoratedBox(
            decoration: BoxDecoration(color: Colors.red),
            child: Transform.scale(
                scale: 1.5, //放大到1.5倍
                child: Text("Hello world")),
          ),
          Text("2.4  Transform.flip对组件继续翻转:"),
          Transform.flip(
            flipX: true,
            child: const Text('Horizontal Flip'),
          ),
          SizedBox(height: 30),
          Text("3. Transform的变换是应用在绘制阶段，而并不是应用在布局(layout)阶段"),
          Text("文本放大了, 影响空间重叠了: "),
          Row(
            children: <Widget>[
              SizedBox(width: 40),
              DecoratedBox(
                decoration: BoxDecoration(color: Colors.red),
                child: Transform.scale(scale: 1.5, child: Text("Hello world")),
              ),
              Text("你好", style: TextStyle(color: Colors.green, fontSize: 18.0))
            ],
          ),
          SizedBox(height: 10),
          Text("思考题：使用Transform对其子组件先进行平移然后再旋转和先旋转再平移，两者最终的效果一样吗？为什么？"),
          Text("先平移，再旋转"),
          DecoratedBox(
            decoration: BoxDecoration(color: Colors.red),
            child: Transform.rotate(
              angle: math.pi / 2,
              // angle: 0,
              child: Transform.translate(
                offset: Offset(50.0, 0),
                child: Text("Hello world"),
              ),
            ),
          ),
          SizedBox(height: 50),
          Text("先旋转，再平移"),
          DecoratedBox(
            decoration: BoxDecoration(color: Colors.red),
            child: Transform.translate(
              offset: Offset(50.0, 0),
              child: Transform.rotate(
                angle: math.pi / 2,
                child: Text("Hello world"),
              ),
            ),
          ),
          Text("以为是先绘制后布局，所以不过是平移还是旋转，都是根据红色部分的中心点来操作的"),
          SizedBox(height: 60),
          Text("4. RotatedBox作用再布局(layout)阶段, 先绘制了，布局时再生效，所以背景一起旋转了"),
          Text("但是没有TranslatedBox, ScaleBox?"),
          Row(
            children: <Widget>[
              DecoratedBox(
                decoration: BoxDecoration(color: Colors.red),
                child: Transform.rotate(
                  //旋转90度
                  angle: math.pi / 2,
                  child: Text("Hello world"),
                ),
              ),
              SizedBox(width: 50),
              DecoratedBox(
                decoration: BoxDecoration(color: Colors.red),
                child: RotatedBox(
                  quarterTurns: 1,
                  child: Text("Hello world"),
                ),
              ),
              Text(
                "你好",
                style: TextStyle(color: Colors.green, fontSize: 18.0),
              )
            ],
          ),
          SizedBox(height: 60),
          Text("源码解析"),
          Text(
              "MyTransform extends SingleChildRenderObjectWidget, 核心方法：MyRenderTransform createRenderObject: paint"),
          Container(
            margin: EdgeInsets.only(top: 50, left: 10),
            color: Colors.black,
            child: MyTransform(
              alignment: Alignment.topRight, //相对于坐标系原点的对齐方式
              // Matrix4是一个4D矩阵
              transform: Matrix4.skewY(0.3), //沿Y轴倾斜0.3弧度
              child: Container(
                padding: const EdgeInsets.all(8.0),
                color: Colors.deepOrange,
                child: const Text('Apartment for rent!'),
              ),
            ),
          ),
          SizedBox(height: 10),
          Text(
              "静态函数像是在 MyTransform.rotate({}): transform = xx, 这也是一种构造函数？还是自己调用了paint?"),
          MyTransform.rotate(
            angle: -math.pi / 12.0,
            child: Container(
              padding: const EdgeInsets.all(8.0),
              color: const Color(0xFFE8581C),
              child: const Text('Apartment for rent!'),
            ),
          )
        ],
      ),
    );
  }
}

void main(List<String> args) {
  runApp(MaterialApp(
    home: Scaffold(body: Demo()),
  ));
}
