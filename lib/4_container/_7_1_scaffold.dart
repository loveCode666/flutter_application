import 'package:flutter/material.dart';

class ScaffoldRoute extends StatefulWidget {
  @override
  State createState() => _ScaffoldRouteState();
}

class _ScaffoldRouteState extends State<ScaffoldRoute> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        //导航栏
        title: Text("App Name"),
      ),

      // 2. 底部带打洞的导航
      bottomNavigationBar: BottomAppBar(
        color: Colors.white,
        shape: CircularNotchedRectangle(), // 底部导航栏打一个圆形的洞
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            IconButton(
              icon: Icon(Icons.home),
              onPressed: () {},
            ),
            // SizedBox(), //中间位置空出? 没啥用吧
            IconButton(
              icon: Icon(Icons.business),
              onPressed: () {},
            ),
          ], //均分底部导航栏横向空间
        ),
        // onTap: _onItemTapped,
      ),

      // 是Material设计规范中的一种特殊Button，通常悬浮在页面的某一个位置作为某种常用动作的快捷入口
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,

      floatingActionButton: FloatingActionButton(
          //悬浮按钮
          onPressed: _onAdd, //悬浮按钮
          child: Icon(Icons.add)),
    );
  }

  void _onAdd() {
    print("add");
  }
}

void main(List<String> args) {
  runApp(MaterialApp(
    home: ScaffoldRoute(),
  ));
}
