import 'package:flutter/material.dart';

class Demo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
            "1. center和Asign不能说很相同吧，只能说一模一样，只是少了alignment的控制, Center extends Align"),
        Container(
          color: Colors.yellow.shade100,
          width: 100,
          height: 100,
          child: Center(child: Text("唐三")),
        ),
        SizedBox(height: 5),
        Container(
          color: Colors.yellow.shade100,
          width: 100,
          height: 100,
          child: Align(child: Text("唐三")),
        ),
        SizedBox(height: 30),
        Text(
            "2. 当widthFactor或heightFactor为null || width、height为null 时, 组件的宽高将会占用尽可能多的空间"),
        Container(
          color: Colors.green,
          // width: 100,
          // height: 100,
          child: Center(
            // widthFactor: 3,
            // heightFactor: 3,
            child: Text("xxx"),
          ),
        ),
      ],
    );
  }
}

void main(List<String> args) {
  runApp(MaterialApp(
    home: Scaffold(body: Demo()),
  ));
}
