import 'package:flutter/material.dart';

class Demo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("弹性布局允许子组件按照一定比例来分配父容器空间, Row, Column extends Flex"),
        Text("Flex作为父亲，一定需要Expanded作为子元素配合才有意义"),
        SizedBox(height: 10),
        Text("1. 水平方向排列..."),
        //Flex的两个子widget按1：2来占据水平空间
        Flex(
          direction: Axis.horizontal,
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Container(
                height: 30.0,
                color: Colors.red,
              ),
            ),
            Expanded(
              flex: 2,
              child: Container(
                height: 30.0,
                color: Colors.green,
              ),
            ),
          ],
        ),

        SizedBox(height: 10),

        Text("2. 垂直方向排列..."),
        //Flex的两个子widget按1：2来占据垂直空间
        SizedBox(
          width: 50,
          height: 100.0,
          //Flex的三个子widget，在垂直方向按2：1：1来占用100像素的空间
          child: Flex(
            direction: Axis.vertical,
            children: <Widget>[
              Expanded(
                flex: 2,
                child: Container(
                  height: 30.0,
                  color: Colors.red,
                ),
              ),
              Spacer(
                flex: 1,
              ),
              Expanded(
                flex: 1,
                child: Container(
                  height: 30.0,
                  color: Colors.green,
                ),
              ),
            ],
          ),
        ),

        SizedBox(height: 10),
        Text("3. Flex一定要参数direction，所以可以直接简单点使用Row | Column"),
      ],
    );
  }
}

void main(List<String> args) {
  runApp(MaterialApp(
    home: Scaffold(body: Demo()),
  ));
}
