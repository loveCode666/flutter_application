import 'package:flutter/material.dart';

class Demo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("1. Align: 只想简单的调整一个子元素在父元素中的位置"),
        Text("1.1 stack就好，甚至都用不着position"),
        Container(
          height: 120.0,
          width: 120.0,
          color: Colors.blue.shade50,
          child: Stack(
            alignment: Alignment.topRight,
            children: [
              FlutterLogo(size: 60),
            ],
          ),
        ),
        SizedBox(height: 10),
        Text("1.2 当然align也可以"),
        Container(
          height: 120.0,
          width: 120.0,
          color: Colors.blue.shade50,
          child: Align(
            alignment: Alignment.topRight,
            child: FlutterLogo(size: 60),
          ),
        ),
        SizedBox(height: 50),
        Text(
            "2. Align的widthFactor, heightFactor缩放因子也能调整大小， containerWidth = factor * childSize"),
        Container(
          color: Colors.blue.shade50,
          child: Align(
            widthFactor: 2,
            heightFactor: 2,
            // alignment: Alignment.topLeft,
            // alignment: Alignment(-1, -1),
            // alignment: FractionalOffset.topLeft,
            alignment: FractionalOffset(0, 0),
            child: FlutterLogo(
              size: 60,
            ),
          ),
        ),
        Text("总结: "),
        Text("1. Align只能偏移一个元素; Stack可以多个元素，可以层叠, 需要组合Positioned使用"),
        Text(
            "2. Stack/Positioned坐标参考系四条边；Align参考系：a. Alignment中心体系， FractionalOffset 左上角体系")
      ],
    );
  }
}

void main(List<String> args) {
  runApp(MaterialApp(
    home: Scaffold(body: Demo()),
  ));
}
