import 'package:flutter/material.dart';

// 1. stack就相当于设了position: absolute了，层叠定位/绝对定位
// 2. Positioned包裹后可以改变位置
// 3. 属性了解：Stack-alignment：整体位置改变等...
class Demo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      //通过ConstrainedBox来确保Stack占满屏幕
      constraints: BoxConstraints.expand(),
      child: Stack(
        // 指定所有widget的对齐方式，然后有positioned在此基础上修饰
        alignment: Alignment.center,
        // 没有定位(Positioned, 再加left, right等方向)的子组件如何去适应Stack的大小。
        // StackFit.loose表示使用子组件的大小，StackFit.expand表示扩伸到Stack的大小。
        // fit: StackFit.loose,
        children: <Widget>[
          Container(
            color: Colors.amber,
            width: 100,
            height: 100,
          ),
          Container(
            color: Colors.red,
            child: Text("Hello world", style: TextStyle(color: Colors.white)),
          ),
          Text("内马尔"),
          Positioned(
            top: 0,
            left: 10,
            width: 400,
            // left, width, right只需要指定两个就可，另外一个会自动算出，如果都指定则报错
            // right: 200,
            child: Text(
                "1. 一般Stack, Position结合使用，对应csss中(position: absolute; 左右编剧；没有Position, 位置就参考alignment, 所有元素都叠加在一起"),
          ),
          Positioned(
            left: 18.0,
            width: 200,
            child: Text(
                "2. 加上Position, 各自按照自己定位。position中的left，right等都是相对于整个stack的，"),
          ),
          Positioned(
            top: 48.0,
            child: Text("Your friend"),
          )
        ],
      ),
    );
  }
}

class Demo2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      //通过ConstrainedBox来确保Stack占满屏幕
      constraints: BoxConstraints.expand(),
      child: Stack(
        alignment: Alignment.center,
        fit: StackFit.expand, //未定位widget占满Stack整个空间
        children: <Widget>[
          Positioned(
            left: 18.0,
            child: Text("I am Jack"),
          ),
          Container(
            color: Colors.red,
            child: Text("Hello world", style: TextStyle(color: Colors.white)),
          ),
          Positioned(
            top: 18.0,
            child: Text("Your friend"),
          )
        ],
      ),
    );
  }
}

void main(List<String> args) {
  runApp(MaterialApp(
    home: Scaffold(
      body: Demo(),
      // body: Demo2(),
    ),
  ));
}
