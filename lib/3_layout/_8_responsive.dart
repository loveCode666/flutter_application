import 'package:flutter/material.dart';

class Demo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // 此处用Column会溢出，为啥呢？Column里面套了column?
    // 原因未知，解决办法：
    // 1.Column -> ListView / SingleChildScrollView
    return ListView(
      children: [
        Text("需求：自适应布局: 最大宽度小于200，显示单列; 大于200，显示双列。"),
        Text("1. 只用Row, Expanded试图完成？"),
        Text("应该做不到吧，它没法像flex那样小于minWidth时自动换行，大于时自动填充，fluter中两者只能取其一"),
        Text("而且得通过row控制整行，wrap换行"),
        WrapExpanded(),
        Text("2. 自适应布局layoutbuilder: 最大宽度小于200，显示单列; 大于200，显示双列。"),
        Text("2.1 写的有点麻烦，判断宽度，然后是否add Row, 来自flutter教程"),
        LayoutBuilderRoute(),
        Text("2.2 结合GridView 或 ListView的属性, 但是这玩意儿没法嵌套在ListView中吗？"),
        Text("直接改Body中"),
        // LayoutBuilderRoute2(),
        SizedBox(height: 30),
        Text("3. MediaQuery")
      ],
    );
  }
}

class WrapExpanded extends StatelessWidget {
  const WrapExpanded({super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        // getCol2(),
        // getCol2(),
        getCol(),
        getCol(),
      ],
    );
  }
}

Widget getCol() {
  return Expanded(
    child: Container(
      height: 50,
      constraints: BoxConstraints(minWidth: 200),
      decoration: BoxDecoration(border: Border.all(color: Colors.red)),
    ),
  );
}

Widget getCol2() {
  return Container(
    height: 50,
    constraints: BoxConstraints(minWidth: 200),
    decoration: BoxDecoration(border: Border.all(color: Colors.red)),
  );
}

class ResponsiveColumn extends StatelessWidget {
  const ResponsiveColumn({Key? key, required this.children}) : super(key: key);

  final List<Widget> children;

  @override
  Widget build(BuildContext context) {
    // 通过 LayoutBuilder 拿到父组件传递的约束，然后判断 maxWidth 是否小于200
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        print("constraints.maxWidth ${constraints.maxWidth}");
        if (constraints.maxWidth < 200) {
          // 最大宽度小于200，显示单列
          return Column(children: children);
        } else {
          // 大于200，显示双列
          var newChildren = <Widget>[];
          for (var i = 0; i < children.length; i += 2) {
            if (i + 1 < children.length) {
              newChildren.add(Row(
                children: [children[i], children[i + 1]],
              ));
            } else {
              newChildren.add(children[i]);
            }
          }
          print(newChildren);
          return Column(children: newChildren);
        }
      },
    );
  }
}

class LayoutBuilderRoute extends StatelessWidget {
  const LayoutBuilderRoute({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // var children = List.filled(6, Text("A"));
    var children = List.filled(6, getCol2());
    // Column在本示例中在水平方向的最大宽度为屏幕的宽度
    return Column(
      children: [
        // 限制宽度为190，小于 200
        Text("宽度小于200"),
        SizedBox(width: 190, child: ResponsiveColumn(children: children)),
        Text("宽度大于200"),
        ResponsiveColumn(children: children),
      ],
    );
  }
}

class LayoutBuilderRoute2 extends StatelessWidget {
  const LayoutBuilderRoute2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // var children = List.filled(6, Text("A"));
    var children = List.filled(6, getCol2());

    return LayoutBuilder(builder: (context, constraints) {
      // 根据最大宽度来决定显示列数
      int maxColumns = constraints.maxWidth < 200 ? 1 : 2;

      // 1.  使用GridView.builder来创建网格
      return GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: maxColumns, // 列数
            childAspectRatio: 2.0 //宽高比为2
            ),
        itemCount: children.length,
        itemBuilder: (context, index) {
          return children[index];
        },
      );
    });
  }
}

class MediaQueryTest extends StatelessWidget {
  const MediaQueryTest({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // var children = List.filled(6, Text("A"));
    var children = List.filled(6, getCol2());

    final screenSize = MediaQuery.of(context).size;
    int maxColumns = screenSize.width < 600 ? 1 : 2;

    return GridView.builder(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: maxColumns, // 列数
          childAspectRatio: 2.0 //宽高比为2
          ),
      itemCount: children.length,
      itemBuilder: (context, index) {
        return children[index];
      },
    );
  }
}

void main(List<String> args) {
  runApp(MaterialApp(
    home: Scaffold(
      // body: Demo(),

      // GirdView： 宽度 > 200
      // body: LayoutBuilderRoute2(),
      // GirdView： 宽度 < 200
      // body: SizedBox(width: 150, child: LayoutBuilderRoute2()),

      // media Query: 这可能是响应式布局的最简单的实践了吧
      body: MediaQueryTest(),
    ),
  ));
}
