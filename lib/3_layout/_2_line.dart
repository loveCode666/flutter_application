import 'package:flutter/material.dart';

class RowTest extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("线性布局，即指沿水平或垂直方向排列子组件。"),
        Text("1. 原本模样"),
        Text("C罗自律之王"),
        Text("内马尔太懒散了"),
        Container(width: 100, height: 20, color: Colors.red),
        SizedBox(height: 5),
        Container(width: 100, height: 20, color: Colors.green),
        SizedBox(height: 30),
        Text("2. Row包裹"),
        Row(
          children: [
            Text("C罗自律之王"),
            SizedBox(width: 5),
            Text("内马尔太懒散了"),
          ],
        ),
        Row(
          children: <Widget>[
            Container(width: 100, height: 20, color: Colors.red),
            Container(width: 100, height: 20, color: Colors.green),
          ],
        ),
        SizedBox(height: 30),
        Text("3. 一些重要属性"),
        Text("3.1 倒排, 对齐方式"),
        Row(
          // mainAxisAlignment: MainAxisAlignment.end,
          textDirection: TextDirection.rtl,
          children: [
            Text("C罗自律之王"),
            SizedBox(width: 5),
            Text("内马尔太懒散了"),
          ],
        ),
        Text("3.2 副轴的对齐方式，即垂直方向的对齐"),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          // 默认是VerticalDirection.down(从上往下),CrossAxisAlignment.start为顶部对齐；VerticalDirection.up时,CrossAxisAlignment.start则变成了底部对齐了；
          verticalDirection: VerticalDirection.down,
          children: <Widget>[
            Text("C罗自律之王", style: TextStyle(fontSize: 30.0)),
            Text("内马尔太懒散了"),
          ],
        ),
      ],
    );
  }
}

class ColumnTest extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text("C罗自律之王", style: TextStyle(fontSize: 30.0)),
        Text("内马尔太懒散了"),
      ],
    );
  }
}

// 2. Column的一些特性
class ColumnTestCenter1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      // crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text("1. 如何居中对齐呢？有一行横屏了"),
        Text("C罗自律之王", style: TextStyle(fontSize: 30.0)),
        SizedBox(width: double.infinity),
        Text("内马尔太懒散了"),
      ],
    );
  }
}

class ColumnTestCenter2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(border: Border.all(color: Colors.red)),
      // constraints: BoxConstraints(minWidth: double.infinity),
      width: double.infinity,
      child: Column(
        // crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text("2. 这为什么就居中对齐呢？BoxConstraints"),
          Text("C罗自律之王", style: TextStyle(fontSize: 30.0)),
          Text("内马尔太懒散了"),
        ],
      ),
    );
  }
}

class ColumnTestCenter3 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          Text("3. 如何居中对齐呢？center组件"),
          Text("C罗自律之王", style: TextStyle(fontSize: 30.0)),
          Text("内马尔太懒散了"),
        ],
      ),
    );
  }
}

class ColumnTest2 extends StatelessWidget {
  Widget sonColumn() {
    return Container(
      color: Colors.red,
      child: Column(
        mainAxisSize: MainAxisSize.max, //虽然是默认属性，强行指定也无效，内层Colum高度为实际高度
        children: <Widget>[
          Text("C罗自律之王", style: TextStyle(fontSize: 30.0)),
          Text("内马尔太懒散了"),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 300,
      color: Colors.green,
      child: Column(
        // mainAxisSize: MainAxisSize.max, //有效，外层Colum高度为整个屏幕
        children: <Widget>[
          sonColumn(),

          // 2. 想要子元素也盛满，可用Expanded
          // Expanded(child: sonColumn()),
        ],
      ),
    );
  }
}

void main(List<String> args) {
  runApp(MaterialApp(
    home: Scaffold(
      // body: RowTest(),
      // body: ColumnTest(),
      // body: ColumnTestCenter1(),
      // body: ColumnTestCenter2(),
      // body: ColumnTestCenter3(),
      body: ColumnTest2(),
    ),
  ));
}
