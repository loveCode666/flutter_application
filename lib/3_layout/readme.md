# 结构有点乱呀 这一章节
叫做布局类组件？线性，弹性

# 根据子组件个数分类

布局类组件都会包含一个或多个子组件，不同的布局类组件对子组件排列（layout）方式不同，

Widget	说明	用途
LeafRenderObjectWidget	非容器类组件基类	Widget树的叶子节点，用于没有子节点的widget，通常基础组件都属于这一类，如Image。
SingleChildRenderObjectWidget	单子组件基类	包含一个子Widget，如：ConstrainedBox、DecoratedBox等
MultiChildRenderObjectWidget	多子组件基类	包含多个子Widget，一般都有一个children参数，接受一个Widget数组。如Row、Column、Stack等

继承关系 Widget > RenderObjectWidget > (Leaf/SingleChild/MultiChild)RenderObjectWidget 。

# 根据盒模型和按需加载也能分类？
Flutter 中有两种布局模型：
基于 RenderBox 的盒模型布局。
基于 Sliver ( RenderSliver ) 按需加载列表布局。

# 盒模型分类


