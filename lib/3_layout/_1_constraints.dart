import 'package:flutter/material.dart';

class Demo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget redBox = DecoratedBox(decoration: BoxDecoration(color: Colors.red));
    // Widget redBox = Container(color: Colors.red);

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("1. BoxConstraints 是盒模型布局过程中父渲染对象传递给子渲染对象的约束信息，"),
        Container(
          constraints:
              // BoxConstraints(minWidth: 100, maxWidth: 200, minHeight: 50),
              // 固定大小
              // BoxConstraints.tight(Size(200, 100)),
              // 报错？
              // BoxConstraints.expand(),
              // 固定大小？
              BoxConstraints.expand(width: 200, height: 100),
          color: Colors.red,
          child: Text("666"),
        ),
        SizedBox(height: 30),
        Text("2. ConstrainedBox是一个特殊的Container, 但是感觉多此一举，要它有何用？"),
        ConstrainedBox(
          constraints:
              BoxConstraints(minWidth: 100, maxWidth: 200, minHeight: 50),
          child: Container(
            color: Colors.red,
            // 没有效果滴
            height: 5.0,
          ),
        ),
        SizedBox(height: 30),
        Text("3. SizedBox相当于BoxConstraints.tightFor(width: 固定,height: 固定),"),
        SizedBox(
          width: 100.0,
          height: 50.0,
          child: Container(color: Colors.red, height: 5.0),
        ),
        Text("结论：可能能用SizedBox的，会推荐用SizedBox, 但是Container也是可以的呀"),
        SizedBox(height: 30),
        Text("4. 多重限制"),
        Text("如果某一个组件有多个父级ConstrainedBox限制，那么最终会是哪个生效？"),
        Text("4.1 父亲，爷爷都有min, 取max(min父，min爷, double.infinity), 这样两个都满足了"),
        ConstrainedBox(
          constraints: BoxConstraints(minWidth: 30.0, minHeight: 60.0), //父
          child: ConstrainedBox(
            constraints: BoxConstraints(minWidth: 90.0, minHeight: 20.0), //子
            child: redBox,
          ),
        ),
        SizedBox(height: 10),
        ConstrainedBox(
            constraints: BoxConstraints(minWidth: 90.0, minHeight: 10.0),
            child: ConstrainedBox(
              constraints: BoxConstraints(minWidth: 20.0, minHeight: 60.0),
              child: redBox,
            )),
        Text(
            "4.2 父亲，爷爷都有max, 取min(max父，max爷, 0), 会取0，这样两个都满足了； 如果没有minWidth, minHeight设置，直接不可见了"),
        ConstrainedBox(
          constraints: BoxConstraints(
              minWidth: 20, minHeight: 10, maxWidth: 30.0, maxHeight: 60.0), //父
          child: ConstrainedBox(
            constraints: BoxConstraints(maxWidth: 90.0, maxHeight: 20.0), //子
            child: redBox,
          ),
        ),
      ],
    );
  }
}

void main(List<String> args) {
  runApp(MaterialApp(
    home: Scaffold(body: Demo()),
  ));
}
