import 'package:flutter/material.dart';
import 'new_route.dart';

void main(List<String> args) {
  runApp(MaterialApp(
    home: HomeRoute(),
  ));
}

class HomeRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("这是首页")),
      body: TextButton(
        onPressed: () {
          //导航到新路由
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) {
              return NewRoute();
            }),
          );
        },
        child: Text("导航到新页面"),
      ),
    );
  }
}
