# 1. 移动开发技术简介
## 1.1 原生开发
Android原生：使用Java或Kotlin语言直接调用Android SDK；
iOS原生：通过Objective-C或Swift语言直接调用iOS SDK。

优势：
* 可访问平台全部功能（GPS、摄像头）
* 速度快、性能高、可以实现复杂动画及绘制，整体用户体验好；

缺点：
* 维护两套代码
* 不能动态更新？flutter可以吗？

## 1.2 跨平台技术
### 1.2.1 H5 + 原生（Cordova、Ionic、微信小程序）
webview 

混合开发 || HTMLybrid || Hybird

webview中用于js和原生之间通信的工具：webview javascript bridge, JsBridge

优点：
* 社区资源丰富，开发效率高
* 热更新，不用频繁发版

缺点：
* 性能不佳？

### 1.2.2 JavaScript 开发 + 原生（React Native、Weex）
通过桥接技术（如React Native的Bridge）来调用原生模块，以实现接近原生的性能和体验;
dom布局传递给原生，再绘制不同平台的控件

优点：
* react社区，开发效率高
* 最终还是原始渲染
* 也能热更新

缺点：
* js解释性语言，还是不够快？还是因为中间层桥接？
* 渲染依赖不同平台原生控件来渲染，不同平台可能略有不同
* dart在JIT(Just-in-time, 即时编译)下，执行速度比js差不多，但在AOT(Ahead of time, 静态编译)下，执行速度比js慢 

### 1.2.3 自绘UI + 原生 (Qt for mobile、Flutter)
Qt: 采用的是C++

优点
* 性能高，自绘UI性能和原生控件相近，
* 不同平台显示一致
缺点：
* 学习成本大

# 2. Flutter简介
## 2.1 优势
* 跨平台自绘引擎
Flutter 底层使用 Skia 作为其 2D 渲染引擎
Skia 是 Google的一个 2D 图形处理函数库，运用于Google Chrome和Android,

* 高性能
自绘UI，不需要js和原生之间通信；

* dart语言
只能相比C++好用，完全没有js好写
dart支持JIT（Just-in-time） & AOT（Ahead of time）
## 2.1 缺点
学习成本大，语法没js简介

## 2.3 框架结构
![flutter框架图](./flutter_framework.png)
* 框架层


* 引擎层
Flutter的核心。负责将Dart代码编译成机器码并执行，同时处理与底层操作系统的交互。
a. Skia引擎：一个2D向量图形处理函数库，用于图形的渲染。Skia不仅被用于Google Chrome浏览器，也被Flutter用来处理绘图任务。
b. GC: 内存管理和垃圾回收机制
c. 文本渲染：采用libtxt库实现文本的渲染。

* 嵌入层。 
Flutter 最终渲染、交互是要依赖其所在平台的操作系统 API，嵌入层主要是将 Flutter 引擎 ”安装“ 到特定平台上。
嵌入层采用了当前平台的语言编写，例如 Android 使用的是 Java 和 C++，iOS 和 macOS 使用的是 Objective-C 和 Objective-C++，Windows 和 Linux 使用的是 C++。 
