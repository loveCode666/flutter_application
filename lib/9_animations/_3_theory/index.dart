import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

class Demo extends StatefulWidget {
  const Demo({super.key});

  @override
  State<Demo> createState() => _DemoState();
}

class _DemoState extends State<Demo> with SingleTickerProviderStateMixin {
  double _width = 50;

  late AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      vsync: this, // 使用 SingleTickerProviderStateMixin 提供的 vsync
      duration: Duration(seconds: 1),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Text("1. 全自动动画(隐式动画)：Animationed... flutter自动帮我们setState, duration时间内变换"),
      TextButton(
        onPressed: () => setState(() {
          _width = _width == 100 ? 50 : 100;
        }),
        child: Text("change width"),
      ),
      AnimatedContainer(
        duration: Duration(seconds: 5),
        width: _width,
        height: 100,
        color: Colors.red,
      ),
      Text(
          "class AnimatedContainer extends ImplicitlyAnimatedWidget, 内部也是AnimationController"),
      Text("_controller.addStatusListener, 然后controller.forward, reverse等"),
      SizedBox(height: 30),
      Text("2. 显示动画：需要传入controller, 可以是RotationTransition, FadeTransition,"),
      Text("但是假设记不住这么多名字，或者想随心所欲自定义各种属性, 那就得借助AnimatedBuilder"),
      TextButton(
        onPressed: _controller.repeat,
        child: Text("repeat"),
      ),
      TextButton(
        onPressed: _controller.reset,
        child: Text("reset"),
      ),
      AnimatedBuilder(
        animation: _controller,
        builder: (BuildContext context, child) {
          return Container(
            width: Tween(begin: 50.0, end: 100.0).animate(_controller).value,
            height: 100,
            color: Colors.red,
          );
        },
      ),
      Text(
          "ListenableBuilder extends AnimatedWidget, RotationTransition也是extends AnimatedWidget"),
      Text(
          "animation作为listenable传入，widget.listenable.addListener(_handleChange);  _handleChange中只是setState(() {}而已"),
      SizedBox(height: 30),
      Text("3. 自定义Ticker玩玩"),
      Text("Ticker是Flutter中用来驱动动画的计时器，它继承自Timer, 每个刷新频率都会调用一次"),
      Text("自定义动画需要extends SingleTickerProviderStateMixin, 用来控制controller的变化"),
      TickerTest(),
    ]);
  }
}

class TickerTest extends StatefulWidget {
  const TickerTest({super.key});

  @override
  State<TickerTest> createState() => _TickerTestState();
}

class _TickerTestState extends State<TickerTest> {
  double width = 0;
  @override
  void initState() {
    // 每个刷新频率都会调用一次
    Ticker _ticker = Ticker((elapsed) {
      setState(() {
        width++;
        if (width > 200) {
          width = 0;
        }
      });
    });
    _ticker.start();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: 100,
      color: Colors.blue,
    );
  }
}

void main(List<String> args) {
  runApp(MaterialApp(
    home: Scaffold(body: SingleChildScrollView(child: Demo())),
  ));
}
