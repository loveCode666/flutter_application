import 'dart:math';

import 'package:flutter/material.dart';

/// 王叔堆雪人教程：https://www.bilibili.com/video/BV135411W7wE/?spm_id_from=333.788&vd_source=10d355be19883e4a50b66949c50a67aa
class Demo extends StatefulWidget {
  const Demo({super.key});

  @override
  State<Demo> createState() => _DemoState();
}

class _DemoState extends State<Demo> with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  @override
  void initState() {
    _controller =
        AnimationController(vsync: this, duration: Duration(seconds: 2))
          ..repeat();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // 获取屏幕的宽度和高度
    final screenSize = MediaQuery.of(context).size;
    List<Snowflake> snowflakes =
        List.generate(200, (index) => Snowflake(screenSize));

    return Center(
      child: Container(
        // 全屏
        width: double.infinity,
        height: double.infinity,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [Colors.blue, Colors.lightBlue, Colors.white],
            stops: [0.0, 0.7, 0.95],
          ),
        ),
        // 手动控制动画更好，因为需要让其循环播放，自动动画貌似不可以循环播放哦
        child: AnimatedBuilder(
          animation: _controller,
          builder: (BuildContext context, Widget? child) {
            return CustomPaint(
              painter: MyPainter(snowflakes),
            );
          },
        ),
      ),
    );
  }
}

class MyPainter extends CustomPainter {
  List<Snowflake> snowflakes;
  MyPainter(this.snowflakes);

  @override
  void paint(Canvas canvas, Size size) {
    // print("size $size");
    var whitePaint = Paint()..color = Colors.white;
    // 绘制头
    canvas.drawCircle(size.center(Offset(0, 130)), 70, whitePaint);
    // 绘制身体
    canvas.drawOval(
      Rect.fromCenter(
        center: size.center(Offset(0, 350)),
        width: 300,
        height: 350,
      ),
      whitePaint,
    );

    for (var snowflake in snowflakes) {
      // 雪花飘落函数需要一直循环调用
      snowflake.fall();
      canvas.drawCircle(
          Offset(snowflake.x, snowflake.y), snowflake.radius, whitePaint);
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return oldDelegate != this;
  }
}

class Snowflake {
  late Size size;
  late double x;
  late double y;
  double radius = Random().nextDouble() * 3 + 5;
  double velocity = Random().nextDouble() * 3 + 1;

  Snowflake(this.size) {
    x = Random().nextDouble() * size.width;
    y = Random().nextDouble() * size.height;
  }

  fall() {
    y += velocity;
    if (y > size.height) {
      x = Random().nextDouble() * size.width;
      y = 0;
    }
  }
}

void main(List<String> args) {
  runApp(MaterialApp(
    home: Scaffold(body: Demo()),
  ));
}
