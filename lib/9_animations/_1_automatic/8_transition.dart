import 'package:flutter/material.dart';

class Demo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
              "1. Widget属性发生变化时会执行过渡动画的组件统称为动画过渡组件, 他们都它会在内部自管理AnimationController"),
          Text("如果能将AnimationController进行封装，则会大大提高动画组件的易用性。"),
          Text("例子：按钮点击后背景色从蓝色过渡到红色"),
          TransButton1(),
          Text("本质还是AnimatedBuilder，AnimationController"),
          SizedBox(height: 10),
          Text(
              "2. Flutter提供了一个ImplicitlyAnimatedWidget抽象类, 封装上面复杂的代码，AnimationController的管理以及Tween更新部分的代码都是可以抽象出来的"),
          TransButton1(),
          SizedBox(height: 30),
          Text("3. 体验Flutter预置的很多动画过渡组件，实现方式大都和AnimatedDecoratedBox相似"),
          AnimatedWidgetsTest(),
        ],
      ),
    );
  }
}

// 第一版，完全手写
class TransButton1 extends StatefulWidget {
  const TransButton1({super.key});

  @override
  State<TransButton1> createState() => _TransButtonState1();
}

class _TransButtonState1 extends State<TransButton1> {
  Color _decorationColor = Colors.blue;

  @override
  Widget build(BuildContext context) {
    return AnimatedDecoratedBox1(
      duration: Duration(seconds: 1),
      decoration: BoxDecoration(color: _decorationColor),
      child: TextButton(
        onPressed: () {
          setState(() {
            _decorationColor = Colors.red;
          });
        },
        child: const Text("AnimatedDecoratedBox",
            style: TextStyle(color: Colors.white)),
      ),
    );
  }
}

class AnimatedDecoratedBox1 extends StatefulWidget {
  const AnimatedDecoratedBox1({
    Key? key,
    required this.decoration,
    required this.child,
    this.curve = Curves.linear,
    required this.duration,
    this.reverseDuration,
  }) : super(key: key);

  final BoxDecoration decoration;
  final Widget child;
  final Duration duration;
  final Curve curve;
  final Duration? reverseDuration;

  @override
  State createState() => _AnimatedDecoratedBox1State();
}

class _AnimatedDecoratedBox1State extends State<AnimatedDecoratedBox1>
    with SingleTickerProviderStateMixin {
  @protected
  AnimationController get controller => _controller;
  late AnimationController _controller;

  Animation<double> get animation => _animation;
  late Animation<double> _animation;

  late DecorationTween _tween;

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _animation,
      builder: (context, child) {
        return DecoratedBox(
          decoration: _tween.animate(_animation).value,
          child: child,
        );
      },
      child: widget.child,
    );
  }

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      duration: widget.duration,
      reverseDuration: widget.reverseDuration,
      vsync: this,
    );
    _tween = DecorationTween(begin: widget.decoration);
    _updateCurve();
  }

  void _updateCurve() {
    _animation = CurvedAnimation(parent: _controller, curve: widget.curve);
  }

  @override
  void didUpdateWidget(AnimatedDecoratedBox1 oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.curve != oldWidget.curve) _updateCurve();
    _controller.duration = widget.duration;
    _controller.reverseDuration = widget.reverseDuration;
    //正在执行过渡动画
    if (widget.decoration != (_tween.end ?? _tween.begin)) {
      _tween
        ..begin = _tween.evaluate(_animation)
        ..end = widget.decoration;

      _controller
        ..value = 0.0
        ..forward();
    }
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
}

// 2. 使用flutter提供的ImplicitlyAnimatedWidget
class TransButton extends StatefulWidget {
  const TransButton({super.key});

  @override
  State<TransButton> createState() => _TransButtonState();
}

class _TransButtonState extends State<TransButton> {
  Color _decorationColor = Colors.blue;

  @override
  Widget build(BuildContext context) {
    return AnimatedDecoratedBox(
      duration: Duration(seconds: 1),
      decoration: BoxDecoration(color: _decorationColor),
      child: TextButton(
        onPressed: () {
          setState(() {
            _decorationColor = Colors.red;
          });
        },
        child: const Text("AnimatedDecoratedBox",
            style: TextStyle(color: Colors.white)),
      ),
    );
  }
}

class AnimatedDecoratedBox extends ImplicitlyAnimatedWidget {
  const AnimatedDecoratedBox({
    Key? key,
    required this.decoration,
    required this.child,
    Curve curve = Curves.linear,
    required Duration duration,
  }) : super(
          key: key,
          // curve、duration、reverseDuration三个属性在ImplicitlyAnimatedWidget中已定义
          curve: curve,
          duration: duration,
        );
  final BoxDecoration decoration;
  final Widget child;

  @override
  AnimatedWidgetBaseState createState() => _AnimatedDecoratedBoxState();
}

class _AnimatedDecoratedBoxState
    extends AnimatedWidgetBaseState<AnimatedDecoratedBox> {
  //我们自定义的一个DecorationTween类型的对象
  // DecorationTween(begin: _decoration.animate(animation)，end:decoration)。
  late DecorationTween _decoration;

  @override
  // 每一帧都会调用build方法（调用逻辑在ImplicitlyAnimatedWidgetState中
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: _decoration.evaluate(animation),
      child: widget.child,
    );
  }

  @override
  void forEachTween(TweenVisitor<dynamic> visitor) {
    _decoration = visitor(
      _decoration,
      widget.decoration,
      (value) => DecorationTween(begin: value),
    ) as DecorationTween;
  }
}

// 3. Flutter预设的动画过渡效果
class AnimatedWidgetsTest extends StatefulWidget {
  const AnimatedWidgetsTest({Key? key}) : super(key: key);

  @override
  State createState() => _AnimatedWidgetsTestState();
}

class _AnimatedWidgetsTestState extends State<AnimatedWidgetsTest> {
  double _padding = 10;
  var _align = Alignment.topRight;
  double _height = 100;
  double _left = 0;
  Color _color = Colors.red;
  TextStyle _style = const TextStyle(color: Colors.black);
  Color _decorationColor = Colors.blue;
  double _opacity = 1;

  @override
  Widget build(BuildContext context) {
    var duration = const Duration(milliseconds: 400);
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          ElevatedButton(
            onPressed: () {
              setState(() {
                _padding = 20;
              });
            },
            child: AnimatedPadding(
              duration: duration,
              padding: EdgeInsets.all(_padding),
              child: const Text("AnimatedPadding"),
            ),
          ),
          SizedBox(
            height: 50,
            child: Stack(
              children: <Widget>[
                AnimatedPositioned(
                  duration: duration,
                  left: _left,
                  child: ElevatedButton(
                    onPressed: () {
                      setState(() {
                        _left = 100;
                      });
                    },
                    child: const Text("AnimatedPositioned"),
                  ),
                )
              ],
            ),
          ),
          Container(
            height: 100,
            color: Colors.grey,
            child: AnimatedAlign(
              duration: duration,
              alignment: _align,
              child: ElevatedButton(
                onPressed: () {
                  setState(() {
                    _align = Alignment.center;
                  });
                },
                child: const Text("AnimatedAlign"),
              ),
            ),
          ),
          AnimatedContainer(
            duration: duration,
            height: _height,
            color: _color,
            child: TextButton(
              onPressed: () {
                setState(() {
                  _height = 150;
                  _color = Colors.blue;
                });
              },
              child: const Text(
                "AnimatedContainer",
                style: TextStyle(color: Colors.white),
              ),
            ),
          ),
          AnimatedDefaultTextStyle(
            style: _style,
            duration: Duration(seconds: 2),
            child: GestureDetector(
              child: const Text("hello world"),
              onTap: () {
                setState(() {
                  _style = const TextStyle(
                    color: Colors.blue,
                    decorationStyle: TextDecorationStyle.solid,
                    decorationColor: Colors.blue,
                    // 字体大小变化好像没有动画哦
                    fontSize: 26,
                  );
                });
              },
            ),
          ),
          AnimatedOpacity(
            opacity: _opacity,
            duration: duration,
            child: TextButton(
              style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(Colors.blue)),
              onPressed: () {
                setState(() {
                  _opacity = 0.2;
                });
              },
              child: const Text(
                "AnimatedOpacity",
                style: TextStyle(color: Colors.white),
              ),
            ),
          ),
          AnimatedDecoratedBox1(
            duration: Duration(
                milliseconds: _decorationColor == Colors.red ? 400 : 2000),
            decoration: BoxDecoration(color: _decorationColor),
            child: Builder(builder: (context) {
              return TextButton(
                onPressed: () {
                  setState(() {
                    _decorationColor = _decorationColor == Colors.blue
                        ? Colors.red
                        : Colors.blue;
                  });
                },
                child: const Text(
                  "AnimatedDecoratedBox toggle",
                  style: TextStyle(color: Colors.white),
                ),
              );
            }),
          )
        ]
            .map((e) => Padding(
                padding: const EdgeInsets.symmetric(vertical: 16), child: e))
            .toList(),
      ),
    );
  }
}

void main(List<String> args) {
  runApp(MaterialApp(
    home: Scaffold(body: Demo()),
  ));
}
