import 'package:flutter/material.dart';

class Demo extends StatefulWidget {
  const Demo({super.key});

  @override
  State<Demo> createState() => _DemoState();
}

class _DemoState extends State<Demo> {
  int count = 0;
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("对上一章的代码进行封装"),
        SizedBox(height: 30),
        AnimationedCounter(count: count),
        ElevatedButton(
          onPressed: () {
            setState(() {
              count++;
            });
          },
          child: Text("add"),
        ),
        AnimationedCounter(count: count, duration: 300),
      ],
    );
  }
}

class AnimationedCounter extends StatelessWidget {
  AnimationedCounter({super.key, required this.count, this.duration = 1000});

  final int count;
  final int duration;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 300,
      height: 120,
      color: Colors.blue,
      child: TweenAnimationBuilder(
        duration: Duration(milliseconds: duration),
        tween: Tween<double>(end: count as double),
        builder: (BuildContext context, value, Widget? child) {
          final whole = value ~/ 1;
          final decimal = value - whole;
          print("value $whole $decimal");
          return Stack(
            children: [
              Positioned(
                top: -100 * decimal, // 0 -> 100
                child: Opacity(
                  opacity: 1.0 - decimal,
                  child: Text("$whole", style: TextStyle(fontSize: 100)),
                ),
              ),
              Positioned(
                top: 100 - 100 * decimal, // 0 -> 100
                child: Opacity(
                  opacity: decimal,
                  child: Text("${whole + 1}", style: TextStyle(fontSize: 100)),
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}

void main(List<String> args) {
  runApp(MaterialApp(
    home: Scaffold(body: Demo()),
  ));
}
