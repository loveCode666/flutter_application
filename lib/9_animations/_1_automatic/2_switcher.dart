import 'package:flutter/material.dart';

class Demo extends StatefulWidget {
  const Demo({super.key});

  @override
  State<Demo> createState() => _DemoState();
}

class _DemoState extends State<Demo> {
  Widget _text = const Text("hi");
  int i = 0;

  Widget _text2 = const Text("hi", style: TextStyle(fontSize: 24));
  int j = 0;
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("1. 解决上一小节组件切换动画问题, AnimatedSwitcher"),
        TextButton(
          onPressed: () {
            i++;
            setState(() {
              // _text = i % 2 == 0 ? Text("hi") : Text("hello");
              // _text = i % 2 == 0
              //     ? Text("hi", key: UniqueKey())
              //     : Text("hello", key: UniqueKey());
              _text = i % 2 == 0
                  ? Text("hi", key: ValueKey("hi"))
                  : Text("hello", key: ValueKey("hello"));
            });
          },
          child: Text("change text"),
        ),
        Container(
          width: 100,
          height: 100,
          color: Colors.blue,
          alignment: Alignment.center,
          child: AnimatedSwitcher(
            duration: Duration(seconds: 1),
            child: _text,
          ),
        ),
        SizedBox(height: 30),
        Text("2. AnimatedSwitcher可以在两个不同的组件之间切换"),
        TextButton(
          onPressed: () {
            i++;
            setState(() {
              _text2 = i % 2 == 0
                  ? const Text("hi", style: TextStyle(fontSize: 24))
                  : Image.asset("images/hanli.jpeg");
            });
          },
          child: Text("change text"),
        ),
        Container(
          width: 100,
          height: 100,
          color: Colors.blue,
          alignment: Alignment.center,
          child:
              AnimatedSwitcher(duration: Duration(seconds: 2), child: _text2),
        ),
        Text("换个特效，重写transitionBuilder"),
        Container(
          width: 100,
          height: 100,
          color: Colors.blue,
          alignment: Alignment.center,
          child: AnimatedSwitcher(
              transitionBuilder: (child, animation) {
                // 这是默认动画
                // return FadeTransition(opacity: animation, child: child);
                // return ScaleTransition(scale: animation, child: child);
                return RotationTransition(turns: animation, child: child);
              },
              duration: Duration(seconds: 2),
              child: _text2),
        ),
        Text("同时多个特效也是可以的，可以嵌套"),
        Container(
          width: 100,
          height: 100,
          color: Colors.blue,
          alignment: Alignment.center,
          child: AnimatedSwitcher(
              transitionBuilder: (child, animation) {
                // 这是默认动画
                return RotationTransition(
                  turns: animation,
                  child: FadeTransition(opacity: animation, child: child),
                );
              },
              duration: Duration(seconds: 2),
              child: _text2),
        ),
        Text("据说AnimatedCrossFade是个更优版本，比AnimatedSwitcher"),
      ],
    );
  }
}

void main(List<String> args) {
  runApp(MaterialApp(
    home: Scaffold(body: Demo()),
  ));
}
