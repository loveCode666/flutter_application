import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

class Demo extends StatefulWidget {
  const Demo({super.key});

  @override
  State<Demo> createState() => _DemoState();
}

class _DemoState extends State<Demo> {
  double _width = 50;
  double _width2 = 50;
  double _tweenEndwidth = 50;

  @override
  void initState() {
    // setState(() {
    //   _width2 = 300;
    // });
    SchedulerBinding.instance.addPostFrameCallback((_) {
      // 这里的代码会在首次渲染后执行
      setState(() {
        _width2 = 300;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("万能的补间动画"),
        SizedBox(height: 30),
        Text("1. 前面的AnimatedContainer能简单改变任何大小, 但是"),
        Text("1.1 begin, end的定义和动画分离了，还需要借助state"),
        TextButton(
          onPressed: () => setState(() {
            _width = _width == 300 ? 50 : 300;
          }),
          child: Text("change width"),
        ),
        AnimatedContainer(
          duration: Duration(seconds: 1),
          width: _width,
          height: 100,
          color: Colors.lightBlue,
          alignment: Alignment.center,
          child: Text("curve"),
        ),
        Text("1.2 开场自动动画，得借助initSate & addPostFrameCallback, 有点复杂了"),
        AnimatedContainer(
          duration: Duration(seconds: 1),
          width: _width2,
          height: 100,
          color: Colors.lightBlue,
          alignment: Alignment.center,
          child: Text("curve"),
        ),
        SizedBox(height: 10),
        Text("2.1 TweenAnimationBuilder能对属性进行补间，达到AnimatedContainer一样的效果"),
        Text("一样可以通过setState动态控制，"),
        TextButton(
          onPressed: () => setState(() {
            _tweenEndwidth = _tweenEndwidth == 300 ? 50 : 300;
          }),
          child: Text("change tween end width"),
        ),
        TweenAnimationBuilder(
          tween: Tween(end: _tweenEndwidth),
          duration: Duration(seconds: 1),
          builder: (BuildContext context, double value, Widget? child) {
            return Container(
              width: value,
              height: 100,
              color: Colors.lightBlue,
              alignment: Alignment.center,
              child: Text("tween"),
            );
          },
        ),
        Text("2.2 更强应用在开场动画，无需额外的state了，已经封装了setState"),
        TweenAnimationBuilder(
          tween: Tween(begin: 50.0, end: 300.0),
          duration: Duration(seconds: 1),
          builder: (BuildContext context, double value, Widget? child) {
            return Container(
              width: value,
              height: 100,
              color: Colors.lightBlue,
              alignment: Alignment.center,
              child: Text("tween"),
            );
          },
        ),
        SizedBox(height: 30),
        Text("TweenAnimationBuilder应用场景可能是："),
        Text("1. 开场自动动画，不用addPostFrameCallback的介入了"),
        Text("2. 需要根据value来特定改变组件属性"),
      ],
    );
  }
}

void main(List<String> args) {
  runApp(MaterialApp(
    home: Scaffold(body: Demo()),
  ));
}
