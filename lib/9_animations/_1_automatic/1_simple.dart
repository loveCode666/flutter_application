import 'package:flutter/material.dart';

class Demo extends StatefulWidget {
  const Demo({super.key});

  @override
  State<Demo> createState() => _DemoState();
}

class _DemoState extends State<Demo> {
  double _width = 50;
  Color _color = Colors.blue;
  String _text = "hi";
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("全自动动画(隐式动画)：Animationed..."),
        SizedBox(height: 10),
        Text("AnimatedContainer就是比Container多了一个duration参数，用来设置动画的持续时间。"),
        Container(
          width: 100,
          height: 100,
          color: Colors.blue,
          alignment: Alignment.center,
          child: Text("hi"),
        ),
        SizedBox(height: 10),
        Row(
          children: [
            TextButton(
              onPressed: () => setState(() {
                _width = _width == 100 ? 50 : 100;
              }),
              child: Text("change width"),
            ),
            TextButton(
              onPressed: () => setState(() {
                _color = _color == Colors.blue ? Colors.red : Colors.blue;
              }),
              child: Text("change color"),
            ),
            TextButton(
              onPressed: () => setState(() {
                _text = _text == "hi" ? "hello" : "hi";
              }),
              child: Text("change text"),
            ),
          ],
        ),
        AnimatedContainer(
          duration: Duration(seconds: 1),
          width: _width,
          height: 100,
          color: _color,
          alignment: Alignment.center,
          // 盛水效果
          // decoration: BoxDecoration(
          //   gradient: LinearGradient(
          //       begin: Alignment.topCenter,
          //       end: Alignment.bottomCenter,
          //       colors: [Colors.white, Colors.blue],
          //       stops: [0.6, 0.7]),
          //   borderRadius: BorderRadius.circular(150),
          //   border: Border.all(color: Colors.black12, width: 1),
          // ),
          child: Text(_text, key: ValueKey(_text)),
        ),
        Text(
            "change text变化没有效果，因为AnimatedContainer的动画效果只改变Container。即便加key也没啥用"),
      ],
    );
  }
}

void main(List<String> args) {
  runApp(MaterialApp(
    home: Scaffold(body: Demo()),
  ));
}
