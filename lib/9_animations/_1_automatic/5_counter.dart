import 'package:flutter/material.dart';

class Demo extends StatefulWidget {
  const Demo({super.key});

  @override
  State<Demo> createState() => _DemoState();
}

class _DemoState extends State<Demo> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("计数器练习"),
        SizedBox(height: 30),
        Container(
          width: 300,
          height: 120,
          color: Colors.blue,
          // child: StackNums(decimal: 0.5),
          child: TweenAnimationBuilder(
            duration: Duration(seconds: 1),
            tween: Tween<double>(begin: 7, end: 8),
            builder: (BuildContext context, value, Widget? child) {
              final whole = value ~/ 1;
              final decimal = value - whole;
              print("value $whole $decimal");
              // 为什么会报错呢？Cannot hit test a render box that has never been laid out.
              return stackNums(whole: whole, decimal: decimal);
              // return stackNumsWithOpcity(whole: whole, decimal: decimal);

              // return Column(children: [
              //   stackNums(whole: whole, decimal: decimal),
              //   // stackNumsWithOpcity(whole: whole, decimal: decimal),
              // ]);
            },
          ),
        )
      ],
    );
  }

  Widget stackNums({int whole = 7, double decimal = 0}) {
    return Stack(children: [
      Positioned(
        top: -100 * decimal, // 0 -> 100
        child: Text("$whole", style: TextStyle(fontSize: 100)),
      ),
      Positioned(
        top: 100 - 100 * decimal, // 0 -> 100
        child: Text("${whole + 1}", style: TextStyle(fontSize: 100)),
      ),
    ]);
  }

  Widget stackNumsWithOpcity({int whole = 7, double decimal = 0}) {
    return Stack(children: [
      Positioned(
        top: -100 * decimal, // 0 -> 100
        child: Opacity(
          opacity: 1.0 - decimal,
          child: Text("$whole", style: TextStyle(fontSize: 100)),
        ),
      ),
      Positioned(
        top: 100 - 100 * decimal, // 0 -> 100
        child: Opacity(
          opacity: decimal,
          child: Text("${whole + 1}", style: TextStyle(fontSize: 100)),
        ),
      ),
    ]);
  }
}

void main(List<String> args) {
  runApp(MaterialApp(
    home: Scaffold(body: Demo()),
  ));
}
