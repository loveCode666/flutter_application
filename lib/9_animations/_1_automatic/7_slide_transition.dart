import 'package:flutter/material.dart';

// 任意方向滑动
class SlideTransitionX extends AnimatedWidget {
  SlideTransitionX({
    required Animation<double> animation,
    this.transformHitTests = true,
    this.direction,
    this.child,
  }) : super(listenable: animation) {
    print("direction $direction");
    Tween<Offset> tween;
    switch (direction) {
      case AxisDirection.up:
        tween = Tween<Offset>(begin: Offset(0, -1), end: Offset(0, 0));
        break;
      case AxisDirection.right:
        tween = Tween<Offset>(begin: Offset(-1, 0), end: Offset(0, 0));
        break;
      case AxisDirection.down:
        tween = Tween<Offset>(begin: Offset(0, 1), end: Offset(0, 0));
        break;
      case AxisDirection.left:
      default:
        tween = Tween<Offset>(begin: Offset(1, 0), end: Offset(0, 0));
        break;
    }
    position = tween.animate(animation);
  }

  late final Animation<Offset> position;
  // Animation<Offset> get position => listenable as Animation<Offset>;

  final AxisDirection? direction;

  final bool transformHitTests;

  /// The widget below this widget in the tree.
  ///
  /// {@macro flutter.widgets.ProxyWidget.child}
  final Widget? child;

  @override
  Widget build(BuildContext context) {
    Offset offset = position.value;
    // print("offset: $offset; position: ${position.status}");
    if (position.status == AnimationStatus.reverse) {
      // 以rtl为例：改offset为：(begin: Offset(-1, 0), end: Offset(0, 0)),
      // 然后默认会被外层的AnimatedSwitcher reverse执行为 => (begin: Offset(0, 0), end: Offset(-1, 0))
      switch (direction) {
        case AxisDirection.up:
        case AxisDirection.down:
          offset = Offset(offset.dx, -offset.dy);
          break;
        case AxisDirection.right:
        case AxisDirection.left:
          offset = Offset(-offset.dx, offset.dy);
          break;
        default:
      }
    }

    return FractionalTranslation(
      translation: offset,
      transformHitTests: transformHitTests,
      child: child,
    );
  }
}
