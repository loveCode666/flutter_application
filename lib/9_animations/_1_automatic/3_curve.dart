import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher_string.dart';

class Demo extends StatefulWidget {
  const Demo({super.key});
  @override
  State<Demo> createState() => _DemoState();
}

class _DemoState extends State<Demo> {
  double _width = 50;
  Color _color = Colors.blue;
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("所有的动画除了duration, 还有curve属性：变幻的过程，默认可能是线性"),
        Row(
          children: [
            TextButton(
              onPressed: () => setState(() {
                _width = _width == 300 ? 50 : 300;
              }),
              child: Text("change width"),
            ),
            TextButton(
              onPressed: () => setState(() {
                _color = _color == Colors.blue ? Colors.red : Colors.blue;
              }),
              child: Text("change color"),
            )
          ],
        ),
        AnimatedContainer(
          duration: Duration(seconds: 1),
          // 默认Curves.linear
          // curve: Curves.bounceIn,
          curve: Curves.bounceInOut,
          // curve: Curves.fastOutSlowIn,
          width: _width,
          height: 100,
          color: _color,
          alignment: Alignment.center,
          child: Text("curve"),
        ),
        TextButton(
          onPressed: () => launchUrlString(
              "https://api.flutter.dev/flutter/animation/Curves-class.html"),
          child: Text("官方文档curve曲线介绍"),
        ),
      ],
    );
  }
}

void main(List<String> args) {
  runApp(MaterialApp(
    home: Scaffold(body: Demo()),
  ));
}
