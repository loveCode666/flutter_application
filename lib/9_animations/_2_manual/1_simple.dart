import 'package:flutter/material.dart';

class Demo extends StatefulWidget {
  const Demo({super.key});
  @override
  State<Demo> createState() => _DemoState();
}

class _DemoState extends State<Demo>
    // with SingleTickerProviderStateMixin {
// 多个AnimationController
    with
        TickerProviderStateMixin {
  late AnimationController _controller;
  late AnimationController _controller2;

  @override
  void initState() {
    super.initState();
    // 初始化 AnimationController
    _controller = AnimationController(
      vsync: this, // 使用 SingleTickerProviderStateMixin 提供的 vsync
      duration: Duration(seconds: 2), // 动画持续时间
    );
    _controller2 = AnimationController(
      vsync: this,
      duration: Duration(seconds: 1), // 动画持续时间
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("通过AnimationController控制动画"),
        TextButton(
          onPressed: () {
            _controller.forward(); // 只旋转一次，360度
            // _controller.repeat(); // 一直转
          },
          child: Text("旋转方块"),
        ),
        Padding(
          padding: EdgeInsets.only(top: 30, left: 30),
          child: RotationTransition(
            turns: _controller,
            child: Container(
              width: 100,
              height: 100,
              color: Colors.blue,
              child: Text("haha"),
            ),
          ),
        ),
        SizedBox(height: 30),
        Text("自制刷新动画"),
        Row(
          children: [
            TextButton(
                onPressed: () => _controller2.forward(), child: Text("旋转")),
            TextButton(
                onPressed: () => _controller2.repeat(), child: Text("一直旋转")),
            TextButton(onPressed: () => _controller2.stop(), child: Text("停止")),
            TextButton(
                onPressed: () => _controller2.reset(), child: Text("还原")),
          ],
        ),
        RotationTransition(
          turns: _controller2,
          child: Icon(Icons.refresh, size: 50),
        ),
        SizedBox(height: 30),
        Text("flutter自带加载中组件"),
        CircularProgressIndicator(),
      ],
    );
  }
}

void main(List<String> args) {
  runApp(MaterialApp(home: Scaffold(body: Demo())));
}
