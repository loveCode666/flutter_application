import 'package:flutter/material.dart';

class Demo extends StatefulWidget {
  const Demo({super.key});
  @override
  State<Demo> createState() => _DemoState();
}

class _DemoState extends State<Demo>
    // with SingleTickerProviderStateMixin {
// 多个AnimationControll er
    with
        TickerProviderStateMixin {
  late AnimationController _controller;
  late AnimationController _controller2;
  late AnimationController _controller3;

  double endRotation = 1;

  @override
  void initState() {
    super.initState();
    // 初始化 AnimationController
    _controller = AnimationController(
      vsync: this, // 使用 SingleTickerProviderStateMixin 提供的 vsync
      duration: Duration(seconds: 2),
      lowerBound: 0,
      upperBound: 1,
    );
    _controller2 = AnimationController(
      vsync: this,
      duration: Duration(seconds: 1), // 动画持续时间
    );
    _controller3 = AnimationController(
      vsync: this,
      duration: Duration(seconds: 5), // 动画持续时间
    )..repeat();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("渐变动画"),
          Text("1. AnimationController构造时申明lowerBound, upperBound"),
          TextButton(
            // 只旋转一次，360度
            onPressed: () => _controller.forward(),
            child: Text("旋转方块一圈: lowerBound: 0, upperBound: 1,"),
          ),
          Padding(
            padding: EdgeInsets.only(top: 30, left: 30),
            child: RotationTransition(
              turns: _controller,
              child: Container(
                width: 100,
                height: 100,
                color: Colors.blue,
                child: Text("haha"),
              ),
            ),
          ),
          SizedBox(height: 30),
          Text("2. 通过Tween也能控制"),
          TextButton(
            onPressed: () => _controller2.forward(),
            child: Text("通过Tween传递给xxTransition的参数, 精准随意控制"),
          ),
          Padding(
            padding: EdgeInsets.only(top: 30, left: 30),
            child: RotationTransition(
              // turns: _controller,
              turns: _controller2.drive(Tween(begin: 0, end: endRotation)),
              child: Container(
                width: 100,
                height: 100,
                color: Colors.blue,
                child: Text("haha"),
              ),
            ),
          ),
          Text("Tween动态控制状态，而AnimationController是初始化时定死的变化"),
          TextButton(
            onPressed: () {
              setState(() {
                endRotation = endRotation == 1 ? 5 : 1;
              });
              _controller2.repeat();
            },
            child: Text("改变Tween的结尾数字"),
          ),
          SizedBox(height: 30),
          Text("3. Tween例子"),
          SlideTransition(
            position: Tween(begin: Offset(0, 0), end: Offset(0.8, 0))
                .chain(CurveTween(curve: Interval(0.5, 1.0)))
                .animate(_controller3),
            child: Container(
              width: 100,
              height: 100,
              color: Colors.blue,
              child: Text("haha"),
            ),
          ),
        ],
      ),
    );
  }
}

void main(List<String> args) {
  runApp(MaterialApp(home: Scaffold(body: Demo())));
}
