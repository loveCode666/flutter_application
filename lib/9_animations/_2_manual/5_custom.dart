import 'package:flutter/material.dart';

class Demo extends StatefulWidget {
  const Demo({super.key});
  @override
  State<Demo> createState() => _DemoState();
}

class _DemoState extends State<Demo> with SingleTickerProviderStateMixin {
  late AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      vsync: this, // 使用 SingleTickerProviderStateMixin 提供的 vsync
      duration: Duration(seconds: 2),
    )..repeat(reverse: true);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
            "这一章讲了很多flutter显示动画控件：SlideTransition, FadeTransition, AlignTransition等等 他们都可以传入controller手动控制"),
        SlideTransition(
          position: Tween(begin: Offset(0, 0), end: Offset(2, 0))
              .animate(_controller),
          child: Container(
            width: 100,
            height: 100,
            color: Colors.blue,
            child: Text("haha"),
          ),
        ),
        Text("但是假设记不住这么多名字，或者想随心所欲自定义各种属性, 那就得借助AnimatedBuilder"),
        AnimatedBuilder(
          animation: _controller,
          builder: (BuildContext context, child) {
            return Padding(
              padding: EdgeInsets.only(
                // 方法一：直接通过controller.value获取value
                // left: 200 * _controller.value,
                // 方法而：Tween.evaluate控制区间
                // left: Tween(begin: 50.0, end: 100.0).evaluate(_controller),
                // 方法三：需要chain加入更多动画，那还是得 Tween().animate(_controller)
                left: Tween(begin: 0.0, end: 100.0)
                    .chain(Tween(begin: 2, end: 1))
                    // .chain(CurveTween(curve: Curves.bounceInOut))
                    .animate(_controller)
                    .value,
              ),
              child: child,
            );
          },
          child: Container(
            width: 100,
            height: 100,
            color: Colors.blue,
            child: Text("haha"),
          ),
        )
      ],
    );
  }
}

void main(List<String> args) {
  runApp(MaterialApp(home: Scaffold(body: Demo())));
}
