import 'package:flutter/material.dart';

class Demo extends StatefulWidget {
  const Demo({super.key});
  @override
  State<Demo> createState() => _DemoState();
}

class _DemoState extends State<Demo> with TickerProviderStateMixin {
  late AnimationController _controller;
  late AnimationController _controller2;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      vsync: this, // 使用 SingleTickerProviderStateMixin 提供的 vsync
      duration: Duration(seconds: 20),
    );

    _controller2 = AnimationController(
      vsync: this, // 使用 SingleTickerProviderStateMixin 提供的 vsync
      lowerBound: 0.2,
      duration: Duration(seconds: 4),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("478呼吸法：4秒吸气，7秒憋住，8秒呼气。"),
          SizedBox(height: 30),
          Text("方法一：交错动画："),
          Row(
            children: [
              TextButton(
                  onPressed: () => _controller.repeat(), child: Text("Start")),
              TextButton(
                  onPressed: () => _controller.reset(), child: Text("Reset")),
            ],
          ),
          AnimatedBuilder(
            animation: _controller,
            builder: (context, child) {
              // 圆圈的值：吸气/呼气
              double inValue = Tween(begin: 0.2, end: 1.0)
                  .chain(CurveTween(curve: Interval(0, 4 / 20)))
                  .evaluate(_controller);

              double outValue = Tween(begin: 1.0, end: 0.2)
                  .chain(CurveTween(curve: Interval(11 / 20, 19 / 20)))
                  .evaluate(_controller);

              int second = (_controller.value * 20).floor();
              if (second < 4) {
                second = 4 - second;
              } else if (4 <= second && second < 11) {
                second = 11 - second;
              } else if (11 <= second) {
                second = 19 - second;
              }
              return Container(
                width: 300,
                height: 300,
                decoration: BoxDecoration(
                  gradient: RadialGradient(
                    colors: [Colors.blue.shade600, Colors.blue.shade100],
                    stops: _controller.value < 4 / 20
                        ? [inValue, inValue + 0.1]
                        : [outValue, outValue + 0.1],
                  ),
                  shape: BoxShape.circle,
                ),
                child: Center(
                    child: Text("$second", style: TextStyle(fontSize: 26))),
              );
            },
          ),
          SizedBox(height: 30),
          Text("方法二：动态修改controller时长，动作"),
          Row(
            children: [
              TextButton(
                  onPressed: () async {
                    _controller2.duration = Duration(seconds: 4);
                    _controller2.forward();
                    await Future.delayed(Duration(seconds: 4));

                    _controller2.duration = Duration(seconds: 7);
                    _controller2.reverse();
                    await Future.delayed(Duration(seconds: 7));

                    _controller2.duration = Duration(seconds: 8);
                    // 强行滑动到尾巴处
                    _controller2.value = _controller2.upperBound;
                    _controller2.reverse();
                  },
                  child: Text("Start")),
              TextButton(
                  onPressed: () => _controller2.reset(), child: Text("Reset")),
            ],
          ),
          AnimatedBuilder(
            animation: _controller2,
            builder: (context, child) {
              int duration = _controller2.duration?.inSeconds ?? 0;
              // 百分比
              double percent = (_controller2.value - 0.2) / 0.8;
              // 当前时间
              int count = (duration * percent).floor();

              int second = count;
              if (_controller2.status == AnimationStatus.forward) {
                second = duration - count;
              } else if (_controller2.status == AnimationStatus.dismissed) {
                second = 4;
              } else if (_controller2.status == AnimationStatus.reverse) {
                second += 1;
              }
              print("secord $second");

              return Container(
                width: 300,
                height: 300,
                decoration: BoxDecoration(
                  gradient: RadialGradient(
                      colors: [Colors.blue.shade600, Colors.blue.shade100],
                      // 憋气那7秒
                      stops: _controller2.duration?.inSeconds == 7
                          ? [1.0, 1.0]
                          : [_controller2.value, _controller2.value + 0.1]),
                  shape: BoxShape.circle,
                ),
                child: Center(
                    child: Text("$second", style: TextStyle(fontSize: 26))),
              );
            },
          ),
        ],
      ),
    );
  }
}

void main(List<String> args) {
  runApp(MaterialApp(home: Scaffold(body: Demo())));
}
