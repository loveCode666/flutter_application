import 'package:flutter/material.dart';

import '../../source/_2_page_route.dart';
import '2_2_fade_route.dart';

class Demo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("1. 点击进入新页面"),
        ElevatedButton(
            onPressed: () {
              Navigator.push(
                context,
                MyMaterialPageRoute(builder: (context) => DetailPage()),
              );
            },
            child: Text("Material风格: MaterialPageRoute")),
        SizedBox(height: 10),
        Text("2. 在iOS上会左右滑动切换，在Android上会上下滑动切换。现在，我们如果在Android上也想使用左右切换风格，"),
        ElevatedButton(
          onPressed: () {
            Navigator.push(
              context,
              MyCupertinoPageRoute(builder: (context) => DetailPage()),
            );
          },
          child: Text("Cupertino风格: CupertinoPageRoute"),
        ),
        SizedBox(height: 10),
        Text("3. 自定义动画, 借助PageRouteBuilder"),
        ElevatedButton(
          onPressed: () {
            Navigator.push(
              context,
              // CupertinoPageRoute(builder: (context) => DetailPage()),
              MyPageRouteBuilder(
                transitionDuration: Duration(milliseconds: 2000), //动画时间为500毫秒
                pageBuilder: (BuildContext context, Animation animation,
                    Animation secondaryAnimation) {
                  return FadeTransition(
                    //使用渐隐渐入过渡,
                    opacity: animation as Animation<double>,
                    child: DetailPage(),
                  );
                },
              ),
            );
          },
          child: Text("渐隐渐入动画: PageRouteBuilder"),
        ),
        SizedBox(height: 10),
        Text("4. 自定义动画, class xxx extends PageRoute;"),
        Text(
            "无论是MaterialPageRoute、CupertinoPageRoute，还是PageRouteBuilder，它们都继承自PageRoute类"),
        ElevatedButton(
          onPressed: () {
            Navigator.push(
              context,
              FadeRoute(
                  transitionDuration: Duration(milliseconds: 2000),
                  builder: (context) => DetailPage()),
            );
          },
          child: Text("渐隐渐入动画: FadeRoute extends PageRoute"),
        ),
        Text("太复杂了，还得自定义一个class，还是尽量用PageRouteBuilder自定义动画"),
        SizedBox(height: 10),
        Text("5. 学完了下一章的Hero动画，来试试路由之间的跳转飞行动画"),
        ElevatedButton(
          onPressed: () {
            Navigator.push(
              context,
              MyPageRouteBuilder(
                // transitionDuration: Duration(milliseconds: 1000), //动画时间为500毫秒
                pageBuilder: (BuildContext context, Animation animation,
                    Animation secondaryAnimation) {
                  return FadeTransition(
                    opacity: animation as Animation<double>,
                    child: DetailPage(),
                  );
                },
              ),
            );
          },
          child: Hero(tag: "hanli", child: Text("Hero动画")),
        ),
      ],
    );
  }
}

class DetailPage extends StatelessWidget {
  const DetailPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("New route"),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text("This is new route"),
          Row(),
          SizedBox(height: 30),
          InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child: Hero(tag: "hanli", child: Text("飞回上一页")),
          ),
          SizedBox(height: 10),
          // ElevatedButton(
          //   onPressed: () {
          //     Navigator.pop(context);
          //   },
          //   child: Hero(tag: "hanli", child: Text("Hero动画")),
          // ),
        ],
      ),
    );
  }
}

void main(List<String> args) {
  runApp(MaterialApp(
    home: Scaffold(body: Demo()),
  ));
}
