import 'package:flutter/material.dart';

Widget showd3DModel() {
  return Container(
    color: Colors.blue,
    height: 300,
    child: Center(child: Text("3D Model")),
  ); // 返回一个Container
}

Widget showVideo() {
  return Container(
    color: Colors.green,
    // 在PositionTransition面前, 宽高都会被rect的插值给替换掉
    height: 300,
    // width: 300,
    child: Center(
      child: Text("Playing Video"),
    ),
  ); //
}

class WorkModel extends StatefulWidget {
  const WorkModel({super.key});

  @override
  State<WorkModel> createState() => _WorkModelState();
}

class _WorkModelState extends State<WorkModel>
    with SingleTickerProviderStateMixin {
  bool isPlayVideo = false;
  late AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 400),
    );
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width; // 获取屏幕宽度
    return Stack(
      children: [
        showd3DModel(),
        PositionedTransition(
          rect: RelativeRectTween(
            begin: RelativeRect.fromLTRB(width, 10, 10, 300),
            end: RelativeRect.fill,
          ).animate(_controller),
          child: showVideo(),
        ),
        Positioned(
          right: 10,
          top: 10,
          child: GestureDetector(
            onTap: () {
              isPlayVideo ? _controller.reverse() : _controller.forward();
              setState(() {
                isPlayVideo = !isPlayVideo;
              });
            },
            child:
                Icon(isPlayVideo ? Icons.fullscreen_exit : Icons.play_circle),
          ),
        )
      ],
    );
  }
}

class HeroModel extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        showd3DModel(),
        Positioned(
          right: 60,
          top: 10,
          child: GestureDetector(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) {
                  return VideoPage();
                }),
              );
            },
            child: Hero(tag: 'shujun', child: Icon(Icons.play_circle)),
          ),
        )
      ],
    );
  }
}

class VideoPage extends StatelessWidget {
  const VideoPage({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Stack(
          children: [
            Hero(tag: "shujun", child: showVideo()),
            Positioned(
              right: 60,
              top: 10,
              child: GestureDetector(
                onTap: () => Navigator.pop(context),
                child: Icon(Icons.fullscreen_exit),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class Demo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("安全卫士视频收起动画"),
        SizedBox(height: 10),
        Text("1. 工作中的实现方式"),
        WorkModel(),
        SizedBox(height: 10),
        Text("2. Hero是不是也可行呢？"),
        Text("Hero 动画适用于同一 tag 的两个组件在不同的页面或路由之间进行过渡。它不适用于在同一页面内的不同位置之间进行动画过渡"),
        HeroModel(),
        SizedBox(height: 30),
        Text("结论：Hero虽然代码上简洁了，但是需要两个页面，而且效果不对，是按钮在飞翔，不是视频在缩小，可能是我设置的不对..."),
        Text("但是它确实很适合做头像放大查看效果"),
      ],
    );
  }
}

void main(List<String> args) {
  runApp(MaterialApp(
    home: Scaffold(body: Demo()),
  ));
}
