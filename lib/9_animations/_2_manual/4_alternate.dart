import 'package:flutter/material.dart';

class Demo extends StatefulWidget {
  const Demo({super.key});
  @override
  State<Demo> createState() => _DemoState();
}

class _DemoState extends State<Demo> with SingleTickerProviderStateMixin {
  late AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      vsync: this, // 使用 SingleTickerProviderStateMixin 提供的 vsync
      duration: Duration(seconds: 5),
    )..repeat(reverse: true);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text("tween加interval，可以很好的实现交错动画"),
        for (int i = 0; i < 5; i++)
          slideBox(
              Interval(0.2 * i, 0.2 * i + 0.2), Colors.blue[100 + 200 * i]),
      ],
    );
  }

  Widget slideBox(Interval interval, Color? color) {
    return SlideTransition(
      position: Tween(begin: Offset.zero, end: Offset(0.5, 0))
          .chain(CurveTween(curve: interval))
          // .chain(CurveTween(curve: Curves.bounceInOut))
          .animate(_controller),
      child: Container(width: 300, height: 100, color: color),
    );
  }
}

void main(List<String> args) {
  runApp(MaterialApp(home: Scaffold(body: Demo())));
}
