import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';

class Demo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("1. 轻量级动画有Lottie,  rive/flare"),
        FlareActor("assets/Filip.flr",
            alignment: Alignment.center,
            fit: BoxFit.contain,
            animation: "idle"),
        SizedBox(height: 10),
        Text("2. 待续..."),
      ],
    );
  }
}

void main(List<String> args) {
  runApp(MaterialApp(
    home: Scaffold(body: Demo()),
  ));
}
