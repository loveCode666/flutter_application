import 'package:flutter/material.dart';

import '../source/_1_animation.dart';
import '../source/_1_animation_controller.dart';
import '../source/_1_tween.dart';
import '1_original.dart';

class ScaleAnimationRoute extends StatefulWidget {
  const ScaleAnimationRoute({Key? key}) : super(key: key);

  @override
  State createState() => _ScaleAnimationRouteState();
}

//需要继承TickerProvider，如果有多个AnimationController，则应该使用TickerProviderStateMixin。
class _ScaleAnimationRouteState extends State<ScaleAnimationRoute>
    with SingleTickerProviderStateMixin {
  late MyAnimation<double> animation;
  // AnimationController extends Animation<double>, controller可以看作是animation的一个实现
  late MyAnimationController controller;

  @override
  initState() {
    super.initState();
    controller = MyAnimationController(
        duration: const Duration(seconds: 3), vsync: this);

    // 1. 基本用法
    //图片宽高从0变到300
    animation = MyTween(begin: 0.0, end: 200.0).animate(controller)
      ..addListener(() {
        setState(() => {});
      });

    // 2. 整活儿
    //使用弹性曲线
    // animation = CurvedAnimation(parent: controller, curve: Curves.bounceIn);
    // animation = Tween(begin: 0.0, end: 200.0).animate(animation)
    //   ..addListener(() {
    //     setState(() => {});
    //   });

    //启动动画(正向执行)
    controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    return Image.asset("images/hanli.jpeg", width: animation.value);
  }

  @override
  dispose() {
    //路由销毁时需要释放动画资源
    controller.dispose();
    super.dispose();
  }
}

class ScaleAnimationWithoutState extends StatefulWidget {
  const ScaleAnimationWithoutState({Key? key}) : super(key: key);

  @override
  State createState() => _ScaleAnimationWithoutState();
}

class _ScaleAnimationWithoutState extends State<ScaleAnimationWithoutState>
    with SingleTickerProviderStateMixin {
  late Animation<double> animation;
  late AnimationController controller;

  @override
  initState() {
    super.initState();
    controller =
        AnimationController(duration: const Duration(seconds: 3), vsync: this);
    animation = Tween(begin: 0.0, end: 200.0).animate(controller);

    //启动动画(正向执行)
    controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedImage(animation: animation);

    // AnimatedBuilder正是将渲染逻辑分离出来
    // return AnimatedBuilder(
    //   animation: animation,
    //   child: Image.asset("images/hanli.jpeg"),
    //   builder: (BuildContext ctx, child) {
    //     return SizedBox(width: animation.value, child: child);
    //   },
    // );
  }

  @override
  dispose() {
    //路由销毁时需要释放动画资源
    controller.dispose();
    super.dispose();
  }
}

class AnimatedImage extends AnimatedWidget {
  const AnimatedImage({
    Key? key,
    required Animation<double> animation,
  }) : super(key: key, listenable: animation);

  @override
  Widget build(BuildContext context) {
    final animation = listenable as Animation<double>;
    return Image.asset("images/hanli.jpeg", width: animation.value);
  }
}

class Demo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("继续Animation"),
        Text("1. 自动动画"),
        Text("1.1 上一章节AnimatedContainer是不是感觉有些麻烦，因为变量属性需要维护在statefull widet中"),
        Text("state初始化改变需要 WidgetsBinding.instance.addPostFrameCallback"),
        AnimatedDemo(),
        Text("1.2 TweenAnimationBuilder将会是个不错的选择: "),
        TweenAnimationBuilder<double>(
          tween: Tween(begin: 0, end: 200),
          duration: Duration(seconds: 3),
          builder: (BuildContext context, double value, Widget? child) {
            return Image.asset("images/hanli.jpeg", width: value);
          },
        ),
        Text("2. 手动动画: "),
        ScaleAnimationRoute(),
        SizedBox(height: 10),
        Text("2.2 AnimatedWidget类封装了调用setState()的细节"),
        // ScaleAnimationWithoutState(),
      ],
    );
  }
}

void main(List<String> args) {
  runApp(MaterialApp(
    home: Scaffold(body: Demo()),
  ));
}
