import 'dart:async';

import 'package:flutter/material.dart';

class Original extends StatefulWidget {
  const Original({super.key});

  @override
  State<Original> createState() => _OriginalState();
}

class _OriginalState extends State<Original> {
  double _width = 0;

  @override
  void initState() {
    Timer.periodic(Duration(milliseconds: 10), (timer) {
      setState(() => _width++);
      if (_width > 200) {
        timer.cancel();
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Image.asset("images/hanli.jpeg", width: _width);
  }
}

class Original2 extends StatefulWidget {
  Original2({
    super.key,
    required this.begin,
    required this.end,
    required this.duration,
  });
  final double begin;
  final double end;
  final double duration;

  @override
  State<Original2> createState() => _Origina2lState();
}

class _Origina2lState extends State<Original2> {
  double _width = 0;

  @override
  void initState() {
    _width = widget.begin;
    const interval = 10;
    double step = (widget.end - widget.begin) / (widget.duration / interval);
    Timer.periodic(Duration(milliseconds: 10), (timer) {
      setState(() => _width += step);
      if (_width > widget.end) {
        timer.cancel();
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Image.asset("images/hanli.jpeg", width: _width);
  }
}

class AnimatedDemo extends StatefulWidget {
  const AnimatedDemo({super.key});

  @override
  State<AnimatedDemo> createState() => _AnimatedDemoState();
}

class _AnimatedDemoState extends State<AnimatedDemo> {
  double _width = 0;

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _width = 200;
    });
    return AnimatedContainer(
      duration: Duration(seconds: 3),
      width: _width,
      child: Image.asset("images/hanli.jpeg"),
    );
  }
}

class Demo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("动画初体验：做一个图片宽高自动变化的效果，渐入"),
        Text("1. 原始动画"),
        Text("1.1 ：setState, Timer"),
        Original(),
        SizedBox(height: 10),
        Text("1.2. 增加点难度：0->200, 3s内完成"),
        Original2(
          begin: 0,
          end: 200,
          duration: 3000,
        ),
        SizedBox(height: 30),
        Text("2. Animation"),
        AnimatedDemo(),
        Text(
            "AnimatedXXX需要一个duration & 变量的属性。它应该是在监控这个属性，一旦变化，则在duration内计算出值，不断setState，")
      ],
    );
  }
}

void main(List<String> args) {
  runApp(MaterialApp(
    home: Scaffold(body: Demo()),
  ));
}
