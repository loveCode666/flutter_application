1. 如果是简单全自动，使用AnimatedXX, 类似于：AnimatedContainer，AnimatedSwitcher
   这类动画只能播放一遍，
   TweenAnimationBuilder补间动画，解放了一些复杂state的设置
   a. Tween作为一个渐变，
   TweenAnimationBuilder(
   tween: Tween(begin: 50.0, end: 300.0),
   b. 也是可以在手动动画中的，例如：
   RotationTransition(
   // turns: _controller,
   turns: _controller2.drive(Tween(begin: 0, end: endRotation)),

2. 如果是循环播放，随意控制中断，需要手动：
   a. flutter定义了不少XXTransition: SlideTransition，RotationTransition, FadeTransition等, 
   b. 如果还不满足要求，可以自定义，例如AnimatedBuilder, 

3. 如果需要卡通手绘，不单单是控件属性改变没做到的动画
3.1 直接操作底层 CustomPainter
3.2 动画框架 Rive/Flare/Lottie等...

王叔不秃教程太棒了：https://www.bilibili.com/video/BV1dt4y117J9/?spm_id_from=333.788
比《flutter实战》要好

