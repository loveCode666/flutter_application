# flutter_application

my flutter notes.

## 参考

[flutter实战*第二版](https://book.flutterchina.club/)
[online documentation](https://docs.flutter.dev/)

## 渲染机制
web渲染：html,js,css, 解析成dom树&css树，合成渲染树，绘制到屏幕上

手机UI渲染：
1. 原生渲染：通过系统API，渲染到屏幕上。(类似c++ ?)
能调用手机端的蓝牙，相机，gps等；手机端更注重资源优化，图片资源等打包往往得指定目录，
2. 跨平台渲染
2.1 嵌入html5
2.2 react native等 中间桥接转换
2.3 QT, flutter渲染：自绘ui.
